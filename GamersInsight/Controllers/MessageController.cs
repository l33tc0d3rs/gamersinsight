﻿using GamersInsight.DTOs.RequestModels.Message;
using GamersInsight.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GamersInsight.Controllers
{
    public class MessageController : Controller
    {
        private readonly IMessageService _messageService;
        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [HttpGet]
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> GetMessagesGridView()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Message([FromBody]MessageRequestModel requestModel)
        {
            var request = new SendMessageExtendedRequestModel
            {
                SenderId = long.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)),
                Content = requestModel.MessageContent,
                ReceiverId = requestModel.ReceiverId
            };

            var result = await _messageService.SendMessageAsync(request);

            if (result.IsSuccess)
            {
                return Json(result);    
            }
            return BadRequest(result.Message);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Messages([FromBody]ReadMessagesPaginatedRequestModel requestModel)
        {
            var request = new ReadMessagesPaginatedExtendedRequestModel
            {
                CurrentUserId = long.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)),
                OtherParticipantId = requestModel.OtherParticipantId,
                Skip = requestModel.Skip,
                Take = requestModel.Take,
                ShouldGetLastPage = requestModel.ShouldGetLastPage
            };

            var result = await _messageService.ReadMessageAsync(request);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result.Message);
        }
    }
}
