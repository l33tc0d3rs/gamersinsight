﻿using GamersInsight.DTOs.RequestModels.Conversation;
using GamersInsight.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GamersInsight.Controllers
{
    public class ConversationController : Controller
    {
        private readonly IConversationService _conversationService;
        public ConversationController(IConversationService messageService)
        {
            _conversationService = messageService;
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> GetConversationsGridView()
        {
            return PartialView("_ConversationsGridTable");
        }      

        [HttpPost]
        public async Task<IActionResult> Conversations(GetPaginatedConversationsRequestModel requestModel)
        {
            var request = new GetConversationsExtendedRequestModel
            {
                CurrentUserId = long.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)),
                Skip = requestModel.Skip,
                Take = requestModel.Take
            };

            var result = await _conversationService.ConversationsAsync(request);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result.Message);
        }
    }
}
