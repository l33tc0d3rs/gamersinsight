﻿using Microsoft.AspNetCore.Mvc;

namespace GamersInsight.Controllers
{
    public class ProblemHandlingController : Controller
    {       
        public IActionResult UnauthorizedResult()
        {            
            return View();
        }

        public IActionResult NotFoundResult()
        {
            return View();  
        }

        public IActionResult ServerErrorResult()
        {
            return View();
        }
    }
}
