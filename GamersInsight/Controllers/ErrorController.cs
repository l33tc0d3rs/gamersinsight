﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GamersInsight.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        [HttpGet]
        public IActionResult UnauthorizedResult()
        {            
            return View();
        }

        public IActionResult NotFoundResult()
        {
            return View();
        }

        public IActionResult ServerErrorResult()
        {
            return View();
        }
    }
}
