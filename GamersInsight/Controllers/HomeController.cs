﻿using GamersInsight.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GamersInsight.Controllers
{
    public class HomeController : Controller
    {       
        private readonly IHomeService _homeService;

        public HomeController(IHomeService homeService)
        {
            _homeService = homeService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var result = await _homeService.GetInitialPageStatisticsCachedAsync("Home");
            return View(result);
        }

        public IActionResult Privacy()
        {
            return View();
        }
       

        [HttpGet]
        public IActionResult About()
        {
            return View();
        }
    }
}
