﻿using GamersInsight.DTOs.RequestModels.Threads;
using GamersInsight.DTOs.ResponseModels.Threads;
using GamersInsight.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GamersInsight.Controllers
{
    public class ThreadController : Controller
    {
        private readonly IThreadService _threadService;

        public ThreadController(IThreadService threadService)
        {
            _threadService = threadService;
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult GetThreadsGridView(GetThreadGridByCategoryIdRequestModel requestModel)
        {
            var model = new GetThreadsGridViewByIdResponseModel
            {
                CategoryId = requestModel.Id
            };

            return View(model);
        }
      
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Threads([FromBody] GetPaginatedThreadsRequestModel requestModel)
        {
            var result = await _threadService.GetThreadsAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result.Message);
        }
    }
}