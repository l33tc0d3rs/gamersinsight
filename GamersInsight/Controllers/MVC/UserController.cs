﻿using GamersInsight.DTOs.RequestModels;
using GamersInsight.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GamersInsight.Web.Controllers
{
    public class UserController : Controller
    {

        private readonly IUserService _userServices;
        private readonly IMailService _mailServices;

        public UserController(IUserService userServices, IMailService mailServices)
        {
            _userServices = userServices;
            _mailServices = mailServices;
        }

        [HttpGet]
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Settings()
        {
            var request = new GetUserSettingsRequestModel
            {
                CurrentUserId = long.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier))
            };

            var result = await _userServices.GetUserSettingsAsync(request);

            if (result.IsSuccess)
            {
                return View(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpGet]
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> GetUserSettingsGridView()
        {
            var request = new GetUserSettingsRequestModel
            {
                CurrentUserId = long.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier))
            };

            var result = await _userServices.GetUserShortInfoAsync(request);

            if (result.IsSuccess)
            {
                return PartialView("_UserSettingsUpdatable", result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Verify([FromQuery] string token)
        {
            var email = await _userServices.ConfirmMail(token);
            if (email != null)
            {
                return RedirectToAction("index", "home");
            }
            return RedirectToAction("error", "home");
        }

        [HttpGet]
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult GetManagementGridView()
        {
            return PartialView("_ManagementGridTable");
        }
    }
}
