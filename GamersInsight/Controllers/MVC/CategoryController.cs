﻿using GamersInsight.DTOs.RequestModels;
using GamersInsight.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GamersInsight.Controllers
{
    [Authorize] // TODO -- REMOVE FORUM BUTTON FOR ANONYMUS USERS 
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]        
        public async Task<IActionResult> GetCategoriesGridView()
        {           
            return View();
        }


        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Categories([FromBody] GetPagintedCategoriesRequestModel requestModel)
        {

            var request = new GetExtendedCategoriesModel
            {
                ShouldGetLastPage = requestModel.ShouldGetLastPage,
                Skip = requestModel.Skip,
                Take = requestModel.Take,
                IsNsfw = true
            };

            var result = await _categoryService.GetCategoriesAsync(request);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result.Message);
        }
    }
}