﻿using GamersInsight.DTOs.RequestModels;
using GamersInsight.DTOs.RequestModels.Post;
using GamersInsight.DTOs.ResponseModels.Post;
using GamersInsight.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GamersInsight.Controllers
{
    public class PostController : Controller
    {
        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> GetPostsGridView([FromQuery] GetPostGridViewRequestModel requestModel)
        {          

            var response = new GetPostGridViewResponseModel
            {
                ThreadId = requestModel.ThreadId
            };

            return View(response);
        }   

        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Post([FromQuery] long id)
        {
            return View();
        }

        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Posts([FromBody] GetPagintedPostsRequestModel requestModel)
        {
            var request = new GetPagintedPostsRequestExtendedModel
            {
                ThreadId = requestModel.ThreadId,
                ShouldGetLastPage = requestModel.ShouldGetLastPage,
                Skip = requestModel.Skip,
                Take = requestModel.Take,
                ContentSearchTerm = requestModel.ContentSearchTerm,
                OrderByDateAscending = requestModel.OrderByDateAscending,
                CommentsTakeAmount = requestModel.CommentsTakeAmount,
                UserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier))
            };

            var result = await _postService.GetPostsAsync(request);

            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }
    }
}