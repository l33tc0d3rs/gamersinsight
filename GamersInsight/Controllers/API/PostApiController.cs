﻿using GamersInsight.DTOs.RequestModels;
using GamersInsight.DTOs.RequestModels.Post;
using GamersInsight.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GamersInsight.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PostApiController : Controller
    {
        private readonly IPostService _postService;

        public PostApiController(IPostService postService)
        {
            _postService = postService;
        }

        [HttpPost]
        public async Task<IActionResult> ToggleLike([FromBody] GetPostToggleLikeRequestModel requestModel)
        {
            var request = new GetPostToggleLikeRequestExtendedModel
            {
                UserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)),
                PostId = requestModel.PostId,
                ShouldLike = requestModel.ShouldLike
            };

            var result = await _postService.TogglePostLikeAsync(request);

            if (result.IsSuccess)
            {
                return Json(result);
            }

            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreatePostRequestModel requestModel)
        {
            var request = new CreatePostExtendedRequestModel
            {
                Title = requestModel.Title,
                Content = requestModel.Content,
                UserId = long.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)),
                ThreadId = requestModel.ThreadId
            };

            var result = await _postService.CreateAsync(request);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> PostsPaginated([FromBody] GetPagintedPostsRequestModel requestModel)
        {
            var request = new GetPagintedPostsRequestExtendedModel
            {
                ThreadId = requestModel.ThreadId,
                ShouldGetLastPage = requestModel.ShouldGetLastPage,
                Skip = requestModel.Skip,
                Take = requestModel.Take,
                ContentSearchTerm = requestModel.ContentSearchTerm,
                OrderByDateAscending = requestModel.OrderByDateAscending,
                CommentsTakeAmount = requestModel.CommentsTakeAmount,
                UserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier))
            };

            var result = await _postService.GetPostsAsync(request);

            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> GetPostComments([FromBody] GetPostCommentsRequestModel requestModel)
        {          
            var result = await _postService.GetPostCommentsAsync(requestModel);

            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }


        [HttpPut]
        public async Task<IActionResult> Post([FromBody] UpdatePostRequestModel requestModel)
        {
            var result = await _postService.UpdatePostAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }
                
        [HttpDelete]
        public async Task<IActionResult> PostDelete([FromBody]DeletePostRequestModel requestModel)
        {
            var result = await _postService.DeletePostAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }

        [HttpGet]
        public async Task<IActionResult> Post([FromQuery] GetPostByIdRequestModel requestModel)
        {
            var result = await _postService.GetPostByIdAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreatePostComment([FromBody] CreatePostCommentRequestModel requestModel)
        {
            var request = new CreatePostCommentExtendedRequestModel
            {
                PostId = requestModel.PostId,
                Content = requestModel.Content,
                UserId = long.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier))
            };

            var result = await _postService.CreatePostCommentAsync(request);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }
    }
}