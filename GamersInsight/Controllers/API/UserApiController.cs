﻿using GamersInsight.DTOs.RequestModels;
using GamersInsight.DTOs.RequestModels.User;
using GamersInsight.DTOs.ResponseModels.User;
using GamersInsight.Models.RequestModels;
using GamersInsight.Services.Interfaces;
using GamersInsight.Services.Utilities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GamersInsight.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserApiController : Controller
    {

        private readonly IUserService _userServices;
        private readonly IMailService _mailServices;

        public UserApiController(IUserService userServices, IMailService mailServices)
        {
            _userServices = userServices;
            _mailServices = mailServices;
        }

        [HttpPut]
        [Authorize]
        public async Task<IActionResult> UpdateUser([FromBody] UpdateUserRequestModel requestModel)
        {
            var request = new UpdateUserExtendedRequestModel
            {
                CurrentUserId = long.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)),
                CurrentPassword = requestModel.CurrentPassword,
                Email = requestModel.Email,
                NewPassword = requestModel.NewPassword,
                RepeatPassword = requestModel.RepeatPassword,
                DisplayName = requestModel.DisplayName
            };

            var result = await _userServices.UpdateUserAsync(request);

            if (result.IsSuccess)
            {
                return Json(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> UpdateAvatar([FromForm] UpdateAvatarRequestModel requestModel)
        {

            var request = new UpdateAvatarExtendedRequestModel
            {
                CurrentUserId = long.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)),
                Image = requestModel.Image
            };

            var result = await _userServices.UpdateAvatarAsync(request);

            if (result.IsSuccess)
            {
                return Json(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] RegisterUserRequestModel requestModel)
        {
            var request = new RegisterUserRequestExtendedModel
            {
                DateOfBirth = requestModel.DateOfBirth,
                DisplayName = requestModel.DisplayName,
                Email = requestModel.Email,
                Password = requestModel.Password,
                Username = requestModel.Username,
                BaseUrl = $"{Request.Scheme}://{Request.Host}{Request.PathBase}"
            };

            var result = await _userServices.RegisterUser(request);

            if (result.IsSuccess)
            {
                return Json(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] GetLoggedUserInfoRequestModel requestModel)
        {
            var result = await _userServices.GetLoggedUserInfoAsync(requestModel);

            if (result.IsSuccess)
            {
                var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, result.Id.ToString()));
                identity.AddClaim(new Claim(ClaimTypes.Name, requestModel.Username));
                identity.AddClaim(new Claim(ClaimTypes.Role, result.RoleName));
                identity.AddClaim(new Claim(ClaimTypes.DateOfBirth, result.IsNsfwEnabled.ToString()));


                var principal = new ClaimsPrincipal(identity);

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                return Json(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Verify([FromQuery] string token)
        {
            var email = await _userServices.ConfirmMail(token);
            if (email != null)
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Role()
        {
            var result = new GetUserRoleResponseModel();

            if (User.HasClaim(p => p.Type == ClaimTypes.Role))
            {
                var userRole = User.Claims.First(p => p.Type == ClaimTypes.Role).Value;
                result.Role = userRole;
                result.IsSuccess = true;
                result.Message = "Successfully retreived user roles";
                return Json(result);
            }
            else if (User == null)
            {
                result.Message = "You must be authenticated!";
            }
            else
            {
                result.Message = "User does not have a role!";
            }

            return BadRequest(result);
        }


        [HttpPost]
        [Authorize(Roles = UserRoleConstants.ADMIN)]
        public async Task<IActionResult> BlockUser([FromBody] BlockUserRequestModel requestModel)
        {
            var result = await _userServices.BlockUserAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }

        [HttpPost]
        [Authorize(Roles = UserRoleConstants.ADMIN)]
        public async Task<IActionResult> UnblockUser([FromBody] UnblockUserRequestModel requestModel)
        {
            var result = await _userServices.UnblockUserAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Management([FromBody] GetManagementRequestModel requestModel)
        {
            var result = await _userServices.GetUsersAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }
    }
}
