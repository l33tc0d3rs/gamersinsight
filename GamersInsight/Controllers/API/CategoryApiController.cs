﻿using GamersInsight.DTOs.RequestModels;
using GamersInsight.Services.Interfaces;
using GamersInsight.Services.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GamersInsight.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class CategoryApiController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryApiController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }


        [HttpGet]
        public async Task<IActionResult> Categories([FromQuery] GetPagintedCategoriesRequestModel requestModel)
        {

            var request = new GetExtendedCategoriesModel
            {
                ShouldGetLastPage = requestModel.ShouldGetLastPage,
                Skip = requestModel.Skip,
                Take = requestModel.Take,
                IsNsfw = true
            };

            var result = await _categoryService.GetCategoriesAsync(request);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result.Message);
        }


        [HttpPost]
        [Authorize(Roles = UserRoleConstants.ADMIN)]
        public async Task<IActionResult> Category([FromBody] CreateCategoryRequestModel requestModel)
        {
            var result = await _categoryService.CreateAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result.Message);
        }

        [HttpPut]
        [Authorize(Roles = UserRoleConstants.ADMIN)]
        public async Task<IActionResult> Category([FromBody]UpdateCategoryRequestModel requestModel)
        {
            var result = await _categoryService.UpdateCateogryAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result.Message);
        }

        [HttpDelete]
        [Authorize(Roles = UserRoleConstants.ADMIN)]
        public async Task<IActionResult> Category([FromQuery] DeleteCategoryRequestModel requestModel)
        {
            var result = await _categoryService.DeleteCategoryAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }

        [HttpGet]
        public async Task<IActionResult> Category([FromQuery] GetCategoryByIdRequestModel requestModel)
        {
            var result = await _categoryService.GetCategoryByIdAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }
    }
}