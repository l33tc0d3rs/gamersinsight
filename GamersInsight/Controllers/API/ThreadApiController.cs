﻿using GamersInsight.DTOs.RequestModels.Threads;
using GamersInsight.Services.Interfaces;
using GamersInsight.Services.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GamersInsight.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ThreadApiController : Controller
    {
        private readonly IThreadService _threadService;

        public ThreadApiController(IThreadService threadService)
        {
            _threadService = threadService;
        } 

        [HttpGet]
        public async Task<IActionResult> Thread([FromQuery] GetThreadByIdRequestModel requestModel)
        {
            var result = await _threadService.GetThreadByIdAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }


        [HttpPost]
        public async Task<IActionResult> Threads([FromBody] GetPaginatedThreadsRequestModel requestModel)
        {
            var result = await _threadService.GetThreadsAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result.Message);
        }

        [HttpPost]
        public async Task<IActionResult> Thread([FromBody] CreateThreadRequestModel requestModel)
        {
            var request = new CreateThreadExtendedRequestModel
            {
                ThreadName = requestModel.ThreadName,
                CategoryId = requestModel.CategoryId,
                CurrentUserId = long.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)),
            };

            var result = await _threadService.CreateAsync(request);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }

        [HttpPut]
        [Authorize(Roles = UserRoleConstants.ADMIN)]
        public async Task<IActionResult> Thread(UpdateThreadRequestModel requestModel)
        {
            var result = await _threadService.UpdateThreadAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }

        [HttpGet]
        public async Task<IActionResult> All([FromQuery] GetPaginatedThreadsRequestModel requestModel)
        {
            var result = await _threadService.GetThreadsAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }

        [HttpDelete]
        [Authorize(Roles = UserRoleConstants.ADMIN)]
        public async Task<IActionResult> Thread([FromQuery] DeleteThreadRequestModel requestModel)
        {
            var result = await _threadService.DeleteThreadAsync(requestModel);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result);
        }
    }
}