﻿function handleErrorWithSwalAndRedirect(requestResponse) {

    let responseMessage;    

    if (requestResponse.responseJSON && requestResponse.responseJSON.length > 0) {
        responseMessage = requestResponse.responseJSON.message;
    }
    else {
        responseMessage = requestResponse.responseJSON;
    }

    if (requestResponse.status == 400) {
        Swal.fire(
            ERROR_MESSAGE,
            responseMessage,
            SWAL_ERROR_METHOD_TYPE
        ).then((result) => {
            window.location.replace(BASE_URL + HOME_RELATIVE_PATH);
        });
    }
    else if (requestResponse.status == 401 || requestResponse.status == 403) {      
        window.location.replace(BASE_URL + UNAUTHORIZED_ERROR_PAGE_PATH);
    }
    else if (requestResponse.status == 404) {
        window.location.replace(BASE_URL + NOT_FOUND_ERROR_PAGE_PATH);
    }
    else {
        Swal.fire(
            ERROR_MESSAGE,
            responseMessage,
            SWAL_ERROR_METHOD_TYPE
        ).then((result) => {
            window.location.replace(BASE_URL + HOME_RELATIVE_PATH);
        });
    }   
}

function handleErrorWithSwal(requestResponse) {
    let responseMessage;

    if (requestResponse.responseJSON && requestResponse.responseJSON.length > 0) {
        responseMessage = requestResponse.responseJSON.message;
    }
    else {
        responseMessage = requestResponse.responseJSON;
    }


    if (requestResponse.status == 400) {
        Swal.fire(
            ERROR_MESSAGE,
            responseMessage,
            SWAL_ERROR_METHOD_TYPE
        );
    }
    else if (requestResponse.status == 401 || requestResponse.status == 403) {
        Swal.fire(
            ERROR_MESSAGE,
            "Unauthorized",
            SWAL_ERROR_METHOD_TYPE
        );
    }   
    else if (requestResponse.status == 404) {
        Swal.fire(
            ERROR_MESSAGE,
            "Not Found",
            SWAL_ERROR_METHOD_TYPE
        );
    }
    else {
         Swal.fire(
             ERROR_MESSAGE,
             responseMessage,
             SWAL_ERROR_METHOD_TYPE
         );       
    } 
}


function getMessagesForUser(otherUserId) {
    getMessagesGridData(currentMessageGridPageNumber, false, otherUserId);
}
