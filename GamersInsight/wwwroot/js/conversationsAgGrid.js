﻿const conversationGridPageSize = 5;
const getConversationsDataUrl = "/Conversation/Conversations";

const createNewConversationButtonId = "#create-new-conversation-button";
const createNewConversationFormId = "#create-new-conversation-form";
const conversationsAgGrid = "#conversations-ag-grid";
const editConversationFormId = "#edit-conversation-form";

const newConversationButtonClass = ".new-conversation-button";
const firstPageButtonClassConversation = ".conversations-grid-fist-page-button";
const lastPageButtonClassConversation = ".conversations-grid-last-page-button";
const conversationsGridPagesContainerClass = ".conversations-grid-pages-container";
const deleteConversationClass = '.delete-conversation-button';
const editConversationClass = '.edit-conversation-button';
const conversationsEditButtonIdAttribute = 'data-id';
const updateConversationButtonId = "#update-conversation-button";
const conversationGridTextInfoNameClass = '.conversation-grid-text-info-name';
const otherParticipantIdAttributeConversation = 'data-other-participant-id';
const showConversationModal = 'show-conversation-modal';
const conversationGridPageButton = "conversation-grid-page-button";

const noThreadsYetMessageConversation = "No Threads yet!";
const noActivityDateYetMessageConversation = "No Activity yet!"

//GridProperties
var columnDefsConversation;
var conversationsGridOptions;
var eGridDivConversation;

function initGetConversationsGridData(currentConversationGridPageNumber, shouldGetLastPage) {
    initConversationGridStructure();
    getConversationsGridData(currentConversationGridPageNumber, shouldGetLastPage);
}

function initConversationGridStructure() {
    columnDefsConversation = [
        {
            headerName: "Conversation Name",
            field: "div",
            cellRenderer: function (params) {
                let htmlResult = `<div class="conversation-grid-text-info nk-forum">
                                 <div class="nk-forum-title">
                                     <h4>
                                        <a class="conversation-grid-text-info-name" ${otherParticipantIdAttributeConversation}=${params.data.otherParticipantId}>
                                            Open messages with: '${params.data.ohterParticipantDisplayName}'
                                        </a>                                        
                                     </h4>                                    
                                 </div>                                
                                
                              </div>`;
                return htmlResult;
            }
        }
    ];

    conversationsGridOptions = {
        rowHeight: 60,
        columnDefs: columnDefsConversation,
        rowData: [],
        headerHeight: 0
    };

    eGridDivConversation = document.querySelector(conversationsAgGrid);
    new agGrid.Grid(eGridDivConversation, conversationsGridOptions);
    conversationsGridOptions.api.setDomLayout('autoHeight');
    conversationsGridOptions.api.sizeColumnsToFit();
}

const pagesElementsArrConversation = [
    {
        firstPageButtonId: "#top-conversations-grid-first-page-button",
        lastPageButtonId: "#top-conversations-grid-last-page-button",
        pagesContainerId: "#top-conversations-grid-pages-container"
    },
    {
        firstPageButtonId: "#bottom-conversations-grid-first-page-button",
        lastPageButtonId: "#bottom-conversations-grid-last-page-button",
        pagesContainerId: "#bottom-conversations-grid-pages-container"
    }
];

let currentConversationGridPageNumber = 1;

$(userSettingsRightPanelContainerId).on('click', firstPageButtonClassConversation, function (event) {
    if (currentConversationGridPageNumber > 1) {
        currentConversationGridPageNumber = 1;
        getConversationsGridData(currentConversationGridPageNumber, false);
    }
});

$(userSettingsRightPanelContainerId).on('click', lastPageButtonClassConversation, function (event) {
    getConversationsGridData(currentConversationGridPageNumber, true);
});

$(userSettingsRightPanelContainerId).on('click', conversationGridPageButton, function (event) {
    let pageNumber = parseInt((($(event))[0]).target.text);
    getConversationsGridData(pageNumber, false);
});

$(userSettingsRightPanelContainerId).on('click', conversationGridTextInfoNameClass, function (event) {
    let otherParticipantId = parseInt(((($(event))[0]).currentTarget).getAttribute(otherParticipantIdAttributeConversation));
    document.getElementById("messages-ag-grid").setAttribute(messagesOtherParticipantIdAttribute, otherParticipantId);
    $(`#${showConversationModal}`).modal('show');
    debugger;
    getMessagesGridData(currentMessageGridPageNumber, false, otherParticipantId);
});

function getConversationsGridData(newPageNumber, shouldGetLastPage) {
    let skip = (newPageNumber * conversationGridPageSize) - conversationGridPageSize;

    let dataToSend = JSON.stringify({
        take: conversationGridPageSize,
        skip: skip,
        shouldGetLastPage: shouldGetLastPage
    });

    $.ajax({
        url: getConversationsDataUrl,
        type: "POST",
        data: dataToSend,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            conversationsGridOptions.api.setRowData(response.data);
            currentConversationGridPageNumber = response.currentPage;
            drawPagination(response.totalCount, response.currentPage, pagesElementsArrConversation, conversationGridPageButton);
        },
        error: function (response) {           
        }
    });
}
