﻿function getDateFormatedLongMonthShortDayYear(dateInput) {
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    const date = new Date(dateInput);
    let formattedDate = date.toLocaleDateString("gb-GB", options);
    return formattedDate;
}