﻿var CurrentUserRole = null;
const getUserRoleUrl = '/api/UserApi/Role';

$("#user-login-button").click(function (event) {
    event.preventDefault();
    var formValues = {};
    $.each($("#user-login-form").serializeArray(), function (i, field) {
        formValues[field.name] = field.value;
    });

    let dataToSend = JSON.stringify({
        username: formValues.UserName,
        password: formValues.Password
    });

    $.ajax({
        url: "/api/UserApi/Login",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {
            Swal.fire({
                title: response.message,
                text: 'Redirecting...',
                icon: 'success',
                timer: 2000,
                buttons: false,
            }).then((result) => {
                location.reload();
            });            
        },
        error: function (response) {
            Swal.fire(
                'Error!',
                response.responseJSON.message,
                'error'
            );
        }
    });
});

$("#logout-button-global").click(function (event) {    
    window.localStorage.removeItem(UserRoleLocalStorageKey);
    $.ajax({
        url: "/User/Logout",
        type: "GET",
        success: function (response) {
            Swal.fire({
                title: response.message,
                text: 'Redirecting...',
                icon: 'success',
                timer: 2000,
                buttons: false,
            }).then((result) => {
                window.location.replace(BASE_URL + HOME_RELATIVE_PATH);
            });
        },
        error: function (response) {
            Swal.fire(
                'Error!',
                response.responseJSON.message,
                'error'
            );
        }
    });
});


$("#user-register-button").click(function (event) {
    event.preventDefault();
    var formValues = {};
    $.each($("#user-register-form").serializeArray(), function (i, field) {
        formValues[field.name] = field.value;
    });

    let dataToSend = JSON.stringify({
        userName: formValues.UserName,
        displayName: formValues.DisplayName,
        password: formValues.Password,
        email: formValues.Email,
        dateOfBirth: formValues.DateOfBirth,

    });

    $.ajax({
        url: "/api/UserApi/Register",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {
            Swal.fire({
                title: response.message,
                text: 'Redirecting...',
                icon: 'success',
                timer: 2000,
                buttons: false,
            }).then((result) => {
                location.reload();
            });
        },
        error: function (response) {
            Swal.fire(
                'Error!',
                response.responseJSON.message,
                'error'
            );
        }
    });
});

function getCurrentUserRole() {
    if (CurrentUserRole && CurrentUserRole.length > 0) {
        return CurrentUserRole;
    }
    
    let result = localStorage.getItem(UserRoleLocalStorageKey);
    if (result && result.length > 0) {
        CurrentUserRole = result;
        return CurrentUserRole;
    }
    else {
        $.ajax({
            url: getUserRoleUrl,
            type: "GET",
            async: false,
            contentType: "application/json; charset=utf-8",            
            success: function (response) {
                if (response.isSuccess) {
                    window.localStorage.setItem(UserRoleLocalStorageKey, response.role);
                    CurrentUserRole = response.role;
                    return CurrentUserRole;
                }
                else {
                    //TODO -- Set to no role?
                }
            },
            error: function (response) {
            }
        });

        return CurrentUserRole;
    }
}