﻿const messageGridPageSize = 5;

const getMessagesDataUrl = "/Message/Messages";

const createNewMessageButtonId = "#create-new-message-button";
const createNewMessageFormId = "#create-new-message-form";
const messagesAgGrid = "#messages-ag-grid";
const editMessageFormId = "#edit-message-form";

const newMessageButtonClass = ".new-message-button";
const firstPageButtonClassMessages = ".messages-grid-fist-page-button";
const lastPageButtonClassMessages = ".messages-grid-last-page-button";
const messagesGridPagesContainerClass = ".messages-grid-pages-container";
const deleteMessageClass = '.delete-message-button';
const editMessageClass = '.edit-message-button';
const messagesEditButtonIdAttribute = 'data-id';
const updateMessageButtonId = "#update-message-button";
const messagesOtherParticipantIdAttribute = "data-other-participant-id"

const noThreadsYetMessage = "No Threads yet!";
const noActivityDateYetMessage = "No Activity yet!"

const pagesElementsArrMessages = [
    {
        firstPageButtonId: "#top-messages-grid-first-page-button",
        lastPageButtonId: "#top-messages-grid-last-page-button",
        pagesContainerId: "#top-messages-grid-pages-container"
    },
    {
        firstPageButtonId: "#bottom-messages-grid-first-page-button",
        lastPageButtonId: "#bottom-messages-grid-last-page-button",
        pagesContainerId: "#bottom-messages-grid-pages-container"
    }
];

let currentMessageGridPageNumber = 1;

const columnDefsMessage = [
    {
        headerName: "Sender Display Name",
        field: "div",
        cellRenderer: function (params) {
            let dateFormated = getDateFormatedLongMonthShortDayYear(params.data.timeOfCreation);
            let htmlResult = `<div class="messages-grid-text-info nk-forum">
                                 <div class="nk-forum-title">
                                     <h4 class="mb-0">
                                        <p class="mb-0 messages-grid-text-info-name">
                                            ${params.data.senderDisplayName}:
                                        </p>                                       
                                     </h4>
                                    <p class="category-grid-activity-date">
                                         ${dateFormated}
                                    </p>
                                 </div>                                
                              </div>`;
            return htmlResult;
        }
    },
    {
        headerName: "Message",
        field: "div",
        width: 700,
        cellStyle: { 'white-space': 'normal' },
        cellRenderer: function (params) {
            let htmlresult = `<div class="messages-grid-text-info nk-forum">
                                 <div class="messages-grid-text-info-description">
                                     ${params.data.message}
                                 </div>
                                 <hr>
                              </div>`;
            return htmlresult;
        }
    }
];

const gridOptionsMessages = {
    columnDefs: columnDefsMessage,
    rowData: [],
    headerHeight: 0,
    defaultColDef: {
        autoHeight: true
    }
};

const eGridDivMessages = document.querySelector(messagesAgGrid);
new agGrid.Grid(eGridDivMessages, gridOptionsMessages);
gridOptionsMessages.api.setDomLayout('autoHeight');
gridOptionsMessages.api.sizeColumnsToFit();

//$(createNewMessageButtonId).click(function (event) {
//    var formValues = {};
//    $.each($(createNewMessageFormId).serializeArray(), function (i, field) {
//        formValues[field.name] = field.value;
//    });

//    let otherParticipantId = $(messagesAgGrid).getAttribute(messagesOtherParticipantIdAttribute);

//    let dataToSend = JSON.stringify({
//        messageName: formValues.messageName,
//        messageDescription: formValues.messageDescription,
//        isNsfw: formValues.isNsfw,
//        receiverId: otherParticipantId
//    });

//    $.ajax({
//        url: "/api/Message/Create",
//        type: "POST",
//        contentType: "application/json; charset=utf-8",
//        data: dataToSend,
//        success: function (response) {           
//            getMessagesGridData(currentMessageGridPageNumber, false, otherParticipantId);
//        },
//        error: function () {

//        }
//    });
//});

$("#send-message-button").click(function (event) {
    let messageContent = $("#new-message-text-area").val();
    let otherParticipantId = parseInt(document.getElementById("messages-ag-grid").getAttribute(messagesOtherParticipantIdAttribute));

    let dataToSend = JSON.stringify({
        messageContent: messageContent,
        receiverId: otherParticipantId
    });

    $.ajax({
        url: "/Message/Message",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {
            $("#new-message-text-area").val("");
            getMessagesGridData(currentMessageGridPageNumber, false, otherParticipantId);
        },
        error: function () {
        }
    });
});


$(firstPageButtonClassMessages).click(function (event) {
    if (currentMessageGridPageNumber > 1) {
        currentMessageGridPageNumber = 1;
        let otherParticipantId = parseInt(document.getElementById("messages-ag-grid").getAttribute(messagesOtherParticipantIdAttribute));
        getMessagesGridData(currentMessageGridPageNumber, false, otherParticipantId);
    }
});

$(lastPageButtonClassMessages).click(function (event) {
    let otherParticipantId = parseInt(document.getElementById("messages-ag-grid").getAttribute(messagesOtherParticipantIdAttribute));
    getMessagesGridData(currentMessageGridPageNumber, true, otherParticipantId);
});

$(messagesGridPagesContainerClass).on('click', POSTS_GRID_PAGE_NUMBER_CLASS, function (event) {
    let pageNumber = parseInt((($(event))[0]).target.text);
    let otherParticipantId = parseInt(document.getElementById("messages-ag-grid").getAttribute(messagesOtherParticipantIdAttribute));
    getMessagesGridData(pageNumber, false, otherParticipantId);
});

function getMessagesGridData(newPageNumber, shouldGetLastPage, otherParticipantId) {
    let skip = (newPageNumber * messageGridPageSize) - messageGridPageSize;

    let dataToSend = JSON.stringify({
        take: messageGridPageSize,
        skip: skip,
        shouldGetLastPage: shouldGetLastPage,
        otherParticipantId: otherParticipantId
    });

    $.ajax({
        url: getMessagesDataUrl,
        type: "POST",
        data: dataToSend,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            debugger;
            gridOptionsMessages.api.setRowData(response.data);
            currentMessageGridPageNumber = response.currentPage;
            drawPagination(response.totalCount, response.currentPage, pagesElementsArrMessages);
        },
        error: function (response) {
            //TODO -- Bad message toastr?
        }
    });
}
