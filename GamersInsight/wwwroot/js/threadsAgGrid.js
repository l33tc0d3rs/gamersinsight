﻿const ThreadGridPageSize = 5;

const getThreadsDataUrl = "/Thread/Threads";

const createNewThreadButtonId = "#create-new-thread-button";
const createNewThreadFormId = "#create-new-thread-form";
const threadsAgGrid = "#threads-ag-grid";

const newThreadButtonClass = ".new-thread-button";
const firstPageButtonClass = ".threads-grid-fist-page-button";
const lastPageButtonClass = ".threads-grid-last-page-button";
const threadsGridPagesContainerClass = ".threads-grid-pages-container";

const noPostsYetMessage = "No Posts yet!";
const noActivityDateYetMessage = "No Posts yet!"
const threadAgGridDataCategoryIdAttribute = "data-category-id";
const currentCategoryIdForThreadGrid = parseInt($(threadsAgGrid).attr(threadAgGridDataCategoryIdAttribute));

const deleteThreadClass = '.delete-thread-button';
const editThreadClass = '.edit-thread-button';
const threadsEditButtonIdAttribute = 'data-id';
const updateThreadButtonId = "#update-thread-button";
const editThreadFormId = "#edit-thread-form";
const editThreadModalId = '#edit-thread-modal';

const pagesElementsArr = [
    {
        firstPageButtonId: "#top-threads-grid-first-page-button",
        lastPageButtonId: "#top-threads-grid-last-page-button",
        pagesContainerId: "#top-threads-grid-pages-container"
    },
    {
        firstPageButtonId: "#bottom-threads-grid-first-page-button",
        lastPageButtonId: "#bottom-threads-grid-last-page-button",
        pagesContainerId: "#bottom-threads-grid-pages-container"
    }
];

let currentThreadGridPageNumber = 1;

//INIT
getThreadsGridData(currentThreadGridPageNumber, false);

const columnDefs = [
    {        

        headerName: "Thread Name",
        field: "div",
        cellRenderer: function (params) {            
            let htmlResult = `<div class="thread-grid-text-info nk-forum">
                                 <div class="nk-forum-title">
                                     <h4>
                                        <a class="thread-grid-text-info-name" href='/post/GetPostsGridView?threadId=${params.data.id}'>
                                            ${params.data.name}
                                        </a>                                        
                                     </h4>                                    
                                 </div>                          
                              </div>`;
            return htmlResult;
        }
    },
    {
        headerName: "Actions",
        field: "button",
        cellRenderer: function (params) {
            let userRole = getCurrentUserRole();
            if (userRole === ADMIN_ROLE_NAME || userRole === ADMIN_ROLE_NAME) {
                return `<button class="edit-thread-button nk-btn nk-btn-color-main-3" row-index=${params.rowIndex} data-id=${params.data.id}>Edit</button>
                        <button class="delete-thread-button nk-btn nk-btn-color-main-5" row-index=${params.rowIndex} data-id=${params.data.id}>Delete</button>`;
            }

        }
    },
    {       
        headerName: "Post Count",
        field: "div",
        cellClass: "right-flexed-items",
        cellRenderer: function (params) {
            return `<span>${params.data.postsCount} posts</span>`
        }
    },
    {       
        headerName: "Last Posts",
        field: "div",
        cellClass: "right-flexed-items",
        cellRenderer: function (params) {   
            let title = noPostsYetMessage;
            let dateFormated = noActivityDateYetMessage;
            if (params.data.lastActivePost) {                
                title = params.data.lastActivePost.title; // TODO
                dateFormated = getDateFormatedLongMonthShortDayYear(params.data.lastActivePost.lastActivityTime);
            }
            
            let htmlResult = `<div class="thread-grid-activity-outer-container">
                                <div class="nk-forum-icon icon-thread-grid-container">
                                    <span class="ion-chatboxes icon-thread-grid"></span>
                                </div>
                                <div class="thread-grid-activity">
                                    <div class="thread-grid-activity-title">
                                        <span class="thread-grid-activity-title-link">
                                            ${title}
                                        </span>                                         
                                    </div>
                                    <div class="thread-grid-activity-date">
                                         ${dateFormated}
                                    </div>
                                </div>
                              </div>`
                             
            return htmlResult;
        }
    }
];

const gridOptions = {
    rowHeight: 115,
    columnDefs: columnDefs,
    rowData: [],
    headerHeight: 0,
    autoHeight: true
};

const eGridDiv = document.querySelector(threadsAgGrid);
new agGrid.Grid(eGridDiv, gridOptions);
gridOptions.api.sizeColumnsToFit();

$(createNewThreadButtonId).click(function (event) {
    var formValues = {};
    $.each($(createNewThreadFormId).serializeArray(), function (i, field) {
        formValues[field.name] = field.value;
    });

    let dataToSend = JSON.stringify({
        threadName: formValues.threadName,
        categoryId: currentCategoryIdForThreadGrid
    });

    $.ajax({
        url: "/api/ThreadApi/Thread",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {
            $("#create-thread-modal").modal('hide');
            getThreadsGridData(currentThreadGridPageNumber, false);
        },
        error: function () {

        }
    });
});

$(threadsAgGrid).on('click', deleteThreadClass, function (event) {
    let threadId = ($(event)[0]).currentTarget.getAttribute(threadsEditButtonIdAttribute);

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        deleteThread(threadId);
    })
});


function deleteThread(threadId) {

    $.ajax({
        url: `/api/ThreadApi/Thread?Id=${threadId}`,
        contentType: "application/json; charset=utf-8",
        type: 'DELETE',
        success: function (response) {
            if (response.isSuccess) {
                if (response.isSuccess) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
                //TODO -- Will bug when pages get less than previous current
                getThreadsGridData(currentThreadGridPageNumber, false);
            }
        },
        error: function () {
        }
    });
}


$(threadsAgGrid).on('click', editThreadClass, function (event) {
    let threadId = ($(event)[0]).currentTarget.getAttribute(threadsEditButtonIdAttribute);

    $.ajax({
        url: `/api/ThreadApi/Thread?Id=${threadId}`,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            if (response.isSuccess) {
                $(editThreadModalId).modal('show');
                document.getElementById("edit-thread-name-input").setAttribute('value', response.name);            
                document.getElementById("edit-thread-id-input").setAttribute('value', response.id);
            }
        },
        error: function () {
        }
    });
});


$(updateThreadButtonId).click(function (event) {
    var formValues = {};
    $.each($(editThreadFormId).serializeArray(), function (i, field) {
        formValues[field.name] = field.value;
    });

    formValues["id"] = $('#edit-thread-id-input').val();

    let dataToSend = JSON.stringify({
        id: formValues.id,
        threadName: formValues.threadName,
    });

    $.ajax({
        url: "/api/ThreadApi/Thread",
        type: "PUT",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {
            $(editThreadModalId).modal('hide');
            getThreadsGridData(currentThreadGridPageNumber, false);
        },
        error: function () {

        }
    });
});


$(firstPageButtonClass).click(function (event) {
    if (currentThreadGridPageNumber > 1) {
        currentThreadGridPageNumber = 1;
        getThreadsGridData(currentThreadGridPageNumber, false);
    }
});

$(lastPageButtonClass).click(function (event) {
    getThreadsGridData(currentThreadGridPageNumber, true);
});

$(threadsGridPagesContainerClass).on('click', POSTS_GRID_PAGE_NUMBER_CLASS, function (event) {
    let pageNumber = parseInt((($(event))[0]).target.text);
    getThreadsGridData(pageNumber, false);
});

function getThreadsGridData(newPageNumber, shouldGetLastPage) {
    let skip = (newPageNumber * ThreadGridPageSize) - ThreadGridPageSize;

    let dataToSend = JSON.stringify({
        take: ThreadGridPageSize,
        skip: skip,
        shouldGetLastPage: shouldGetLastPage,
        categoryId: currentCategoryIdForThreadGrid
    });

    $.ajax({
        url: getThreadsDataUrl,
        type: "POST",
        data: dataToSend,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            gridOptions.api.setRowData(response.data);
            currentThreadGridPageNumber = response.currentPage;
            drawPagination(response.totalCount, response.currentPage, pagesElementsArr, ThreadGridPageSize);
        },
        error: function (response) {
          //TODO -- Bad message toastr?
        }
    });
}
