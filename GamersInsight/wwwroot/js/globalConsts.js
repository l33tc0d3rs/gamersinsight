const POSTS_GRID_PAGE_NUMBER_CLASS = ".grid-page-button";

const UserRoleLocalStorageKey = "GamersInsight_UserRole";

const ADMIN_ROLE_NAME = 'Admin';
const MODERATOR_ROLE_NAME = 'Moderator';
const USER_ROLE_NAME = 'User';

const ERROR_MESSAGE = "Error occured!";
const SWAL_ERROR_METHOD_TYPE = "error";

const HOME_RELATIVE_PATH = "home/index";

const UNAUTHORIZED_ERROR_PAGE_PATH = "Error/UnauthorizedResult";
const NOT_FOUND_ERROR_PAGE_PATH = "Error/NotFoundResult";
const BASE_URL = (window.location).protocol + "//" + (window.location).host + "/";