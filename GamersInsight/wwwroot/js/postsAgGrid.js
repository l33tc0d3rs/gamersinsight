﻿const PostGridPageSize = 10;
const CommentsPageSize = 3;
const PostsSeperationSpaceInPixels = 50;
const CommentHeightStepInPixels = 112;

const getPostsDataUrl = "/Post/Posts";

const createNewPostButtonId = "#create-new-post-button";
const createNewPostFormId = "#create-new-post-form";
const postsAgGrid = "#posts-ag-grid";
const newPostTextAreaTinyMce = "#new-post-tinyMce";
const newPostCommentTextArea = "#new-post-comment-textarea";
const postsAgGridDataThreadIdAttribute = "data-thread-id";
const createPostModalId = '#create-post-modal';

const newPostButtonClass = ".new-post-button";
const firstPageButtonClass = ".posts-grid-fist-page-button";
const lastPageButtonClass = ".posts-grid-last-page-button";
const postsGridPagesContainerClass = ".posts-grid-pages-container";
const postsGridLikesClass = ".posts-grid-likes";

const noThreadsYetMessage = "No Threads yet!";
const noActivityDateYetMessage = "No Activity yet!"

const showMoreCommentsButtonClass = '.show-more-comments-button';
const postsRowClass = ".posts-row-class";
const rowIndexAttribute = "row-index";
const postCommentsList = ".post-comments-list";
const maxHeightAttribute = "max-height";
const dataCurrentPageAttribute = "data-current-page";
const dataPostIdAttribute = "data-post-id";
const postsRowMainContainer = "posts-rows-main-container";
const createNewCommentButtonId = "#create-new-post-comment-button";
const newPostCommentButtonClass = ".new-post-comment-button";
const createPostCommentModalId = "#create-post-comment-modal";
const createPostCommentPostIdInput = "#create-post-comment-post-id";
const deletePostClass = ".delete-post-button";
const postEditButtonIdAttribute = "data-id";
const editPostClass = ".edit-post-button";
const createNewPostTitleId = '#create-post-title-input';
const editPostId = "#edit-post-id-input"
const editPostTitle = "#edit-post-title-input";
const editPostTextAreaTinyMce = "#edit-post-tinyMce";
const updatePostButtonId = "#update-post-button";
const postDateContainsInputId = "#post-date-contains-input";
const postDateContainsFormId = "#post-date-contains-form";
const postDateSortSelect = "#post-date-sort-select";
const postAddCommentButtonIdAttribute = "data-post-id";
const fontAwesomeIconFilled = 'fas';
const fontAwesomeIconOutlined = 'far';

const pagesElementsArr = [
    {
        firstPageButtonId: "#top-posts-grid-first-page-button",
        lastPageButtonId: "#top-posts-grid-last-page-button",
        pagesContainerId: "#top-posts-grid-pages-container"
    },
    {
        firstPageButtonId: "#bottom-posts-grid-first-page-button",
        lastPageButtonId: "#bottom-posts-grid-last-page-button",
        pagesContainerId: "#bottom-posts-grid-pages-container"
    }
];

let currentPostGridPageNumber = 1;

////INIT
getPostsGridData(currentPostGridPageNumber, false);


//TODO -- Post Title
const postColumnDefs = [   
      {
        headerName: "User Info",
        field: "div",
        cellRenderer: function (params) {
            let userRole = getCurrentUserRole();
            let heartIconClass = params.data.isLikedByUser ? `${fontAwesomeIconFilled} fa-heart` : `${fontAwesomeIconOutlined} fa-heart`;
            let htmlResult = `
                <div class="${postsRowMainContainer}" ${dataPostIdAttribute}=${params.data.id}>
                	<div class="posts-rows-top-content">
                		<div class="nk-forum-topic-author post-user-info-container">
                			<div>
                				<img class="avatar-image-custom" src="${params.data.author.avatarUrl}" alt="/avatars/avatar-default.jpg"></img>
                			</div>
                			<div class="nk-forum-topic-author-name" title="${params.data.author.username}">
                				<a href="#">${params.data.author.username}</a>
                			</div>
                			<div class="nk-forum-topic-author-role">
                				${params.data.author.roleName}
                			</div>
                			<div>
                				Member since:
                			</div>
                			<div>
                				${getDateFormatedLongMonthShortDayYear(params.data.author.dateOfCreation)}
                			</div>
                        </div>                    
                        <div class="posts-content-wrapper">
                            <div><h4>${params.data.title}</h4> </div>
                            <div>${params.data.content}</div>                             
                        </div>
                	</div>
                
                	<div class="posts-border-divider"></div>
                
                	<div class="posts-rows-bottom-content">
                		<div class="post-user-info-container">
                			<div>
                				Posted On:
                			</div>
                			<div>
                				${getDateFormatedLongMonthShortDayYear(params.data.dateOfCreation)}
                			</div>
                			<div>
                				<span class="nk-forum-action-btn likes-count-container">
                					<span class="posts-grid-likes ${heartIconClass}" data-post-id="${params.data.id}" data-liked="${params.data.isLikedByUser}">
                				    </span>                
                					<span class="num">
                						${params.data.likesCount} 
                				    </span><span> Likes</span>
                                </span>
                       </div>
                        </div>
                		<div class="post-comments-container">
                			<ul class="post-comments-list" ${dataCurrentPageAttribute}="${params.data.commentsResponse.currentPage}"> `
                                + (params.data.commentsResponse.comments && params.data.commentsResponse.comments.length > 0 ?
                                    getCommentsHtml(params.data.commentsResponse.comments)
                				: ``) + `					
                			</ul>	
                		    <div class="post-comment-action-buttons">
                               `+ ((userRole === ADMIN_ROLE_NAME || params.data.isOwner) ?
                                `<button class="edit-post-button nk-btn nk-btn-color-main-3" row-index=${params.rowIndex} data-id=${params.data.id}>
                                    Edit Post
                                 </button >`
                                    : ``)
                                + ((userRole === ADMIN_ROLE_NAME || params.data.isOwner) ?
                                    `<button class="delete-post-button nk-btn nk-btn-color-main-5" row-index=${params.rowIndex} data-id=${params.data.id} >
                                    Delete Post
                                 </button >`
                                    : ``)
                                + (!params.data.commentsResponse.isLastPage ? `
                                             <button class="show-more-comments-button nk-btn nk-btn-xs link-effect-4">
                                                Show More Comments
                                             </button>`
                                : ``) +
                                `<button class="new-post-comment-button nk-btn nk-btn-xs link-effect-4" data-post-id=${params.data.id} data-toggle="modal" data-target="#create-post-comment-modal">
                					Add Comment
                				</button>             
                            </div>
                        </div>
                	</div>
                </div>
                `;
                return htmlResult;
        }
     }
];

const postsGridOptions = {
    columnDefs: postColumnDefs,
    rowData: [],
    headerHeight: 0,
    rowHeight: 470,
    rowClass: "posts-row-class",
    rowStyle: {
        "max-height": "420px",
        "background": "#0e0e0e",
    }
};

const postsGridDiv = document.querySelector(postsAgGrid);
new agGrid.Grid(postsGridDiv, postsGridOptions);
postsGridOptions.api.setDomLayout('autoHeight');
postsGridOptions.api.sizeColumnsToFit();

$(createNewPostButtonId).click(function (event) {   
    tinyMCE.triggerSave();   
    let content = $(newPostTextAreaTinyMce).val();
    let postTitle = $(createNewPostTitleId).val();
    let threadId = $(postsAgGrid).attr(postsAgGridDataThreadIdAttribute);

    let dataToSend = JSON.stringify({
        threadId: threadId,
        title: postTitle,
        content: content
    });

    $.ajax({
        url: "/api/PostApi/Post",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {
            $(createPostModalId).modal('hide');
            getPostsGridData(currentPostGridPageNumber, false);
        },
        error: function (response) {
            Swal.fire(
                'Error!',
                response.responseJSON.message,
                'error'
            );
        }
    });
});

$(postDateSortSelect).change(function (e) {
    getPostsGridData(currentPostGridPageNumber, false);
})

$(postDateContainsFormId).submit(function (e) {
    e.preventDefault();
    getPostsGridData(currentPostGridPageNumber, false);
})

$(postsAgGrid).on('click', newPostCommentButtonClass, function (event) {
    let postId = ($(event)[0]).currentTarget.getAttribute(postAddCommentButtonIdAttribute);
    $(createPostCommentModalId).modal('show');
    document.getElementById("create-post-comment-post-id").setAttribute('value', postId);
});

$(createNewCommentButtonId).click(function (e) {
    e.preventDefault();
    let content = $(newPostCommentTextArea).val();
    let postId = $(createPostCommentPostIdInput).val();

    let dataToSend = JSON.stringify({
        postId: postId,
        content: content
    });

    $.ajax({
        url: "/api/PostApi/CreatePostComment",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {
            $(createPostCommentModalId).modal('hide');
            getPostsGridData(currentPostGridPageNumber, false);
            $(newPostCommentTextArea).val("");
        },
        error: function (response) {
            Swal.fire(
                'Error!',
                response.responseJSON.message,
                'error'
            );
        }
    });
});

$(firstPageButtonClass).click(function (event) {
    if (currentPostGridPageNumber > 1) {
        currentPostGridPageNumber = 1;
        getPostsGridData(currentPostGridPageNumber, false);
    }
});

$(lastPageButtonClass).click(function (event) {
    getPostsGridData(currentPostGridPageNumber, true);
});

$(postsGridPagesContainerClass).on('click', POSTS_GRID_PAGE_NUMBER_CLASS, function (event) {
    let pageNumber = parseInt((($(event))[0]).target.text);
    getPostsGridData(pageNumber, false);
});

$(postsAgGrid).on('click', editPostClass, function (event) {
    let postId = ($(event)[0]).currentTarget.getAttribute(postEditButtonIdAttribute);

    $.ajax({
        url: `/api/PostApi/Post?Id=${postId}`,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            if (response.isSuccess) {
                $('#edit-post-modal').modal('show');
                document.getElementById("edit-post-id-input").setAttribute('value', response.id);
                document.getElementById("edit-post-title-input").setAttribute('value', response.title);
               
                //TinyMCE
                document.getElementById("edit-post-tinyMce").innerHTML = response.content;
                initTinyMceForTextArea("edit-post-tinyMce");               
            }
        },
        error: function (response) {
            Swal.fire(
                'Error!',
                response.responseJSON.message,
                'error'
            );
        }
    });
});

$(updatePostButtonId).click(function (event) {
    tinyMCE.triggerSave();
    let content = $(editPostTextAreaTinyMce).val();
    let postTitle = $(editPostTitle).val();
    let postId = $(editPostId).val();

    let dataToSend = JSON.stringify({
        id: postId,
        title: postTitle,
        content: content
    });

    $.ajax({
        url: "/api/PostApi/Post",
        type: "PUT",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {
            $('#edit-post-modal').modal('hide');
            getPostsGridData(currentPostGridPageNumber, false);
        },
        error: function (response) {
            Swal.fire(
                'Error!',
                response.responseJSON.message,
                'error'
            );
        }
    });
});

$(postsAgGrid).on('click', deletePostClass, function (event) {
    let postId = ($(event)[0]).currentTarget.getAttribute(postEditButtonIdAttribute);

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        deletePost(postId);
    })
});

function deletePost(postId) {

    let dataToSend = JSON.stringify({
        id: postId
    });

    $.ajax({
        url: `/api/PostApi/PostDelete`,
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        type: 'DELETE',
        success: function (response) {
            if (response.isSuccess) {
                Swal.fire(
                    'Deleted!',
                    response.message,
                    'success'
                ).then((result) => {
                    getPostsGridData(currentPostGridPageNumber, false);
                });
            }
        },
        error: function (response) {
            Swal.fire(
                'Error!',
                response.responseJSON.message,
                'error'
            );
        }
    });
}

$(postsAgGrid).on('click', showMoreCommentsButtonClass, function (event) {
    let wholeRow = event.currentTarget.closest(postsRowClass);
    $wholeRow = $(wholeRow);

    let postCommentsUl = ($wholeRow.find(postCommentsList))[0];
    let page = parseInt(postCommentsUl.getAttribute(dataCurrentPageAttribute));

    let postRowMainContainer = ($wholeRow.find('.' + postsRowMainContainer))[0];
    let postId = postRowMainContainer.getAttribute(dataPostIdAttribute);

    let skip = page * CommentsPageSize;

    let dataToSend = JSON.stringify({
        take: CommentsPageSize,
        skip: skip,
        postId: postId
    });

    $.ajax({
        url: "/api/PostApi/GetPostComments",
        type: "POST",
        data: dataToSend,
        contentType: "application/json; charset=utf-8",
        success: function (response) {

            let rowIndex = parseInt(wholeRow.getAttribute(rowIndexAttribute));
            postsGridOptions.api.resetRowHeights();
            let currentRowNode = postsGridOptions.api.getDisplayedRowAtIndex(rowIndex);
            let rowHeight = wholeRow.offsetHeight;

            rowHeight += CommentHeightStepInPixels;
            currentRowNode.setRowHeight(rowHeight);
            postsGridOptions.api.onRowHeightChanged();
           
            $wholeRow.css(maxHeightAttribute, rowHeight - PostsSeperationSpaceInPixels);           

            let commentsHtml = postCommentsUl.innerHTML;
            let newCommentsHtml = getCommentsHtml(response.comments);

            postCommentsUl.setAttribute(dataCurrentPageAttribute, response.currentPage);

            commentsHtml += newCommentsHtml;
            $(postCommentsUl).html(commentsHtml);
        },
        error: function (response) {
            handleErrorWithSwal(response);
        }
    });

});

$(postsAgGrid).on('click', postsGridLikesClass, function (event) {
    let currentTarget = event.currentTarget;
    $currentTarget = $(currentTarget);

    let isLikedByUser = ($currentTarget.attr("data-liked")) === 'true';

    let postId = currentTarget.getAttribute("data-post-id");

    let postUserInfoContainer = ($(currentTarget).parents('.post-user-info-container'))[0];
    let likesCounterSpan = ($(postUserInfoContainer).find('.num'))[0];

    let dataToSend = JSON.stringify({
        postId: postId,
        shouldLike: !isLikedByUser
    });    

    $.ajax({
        url: "/api/PostApi/ToggleLike",
        type: "POST",
        data: dataToSend,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            let wasLiked = response.isLikedByUser;

            $(likesCounterSpan).text(response.postRating);
            $currentTarget.attr("data-liked", wasLiked);
            let likedOrDisklikedTitle;
            if (wasLiked) {
                $currentTarget.removeClass(fontAwesomeIconOutlined);
                $currentTarget.addClass(fontAwesomeIconFilled);
                likedOrDisklikedTitle = 'liked';
            }
            else {
                $currentTarget.removeClass(fontAwesomeIconFilled);
                $currentTarget.addClass(fontAwesomeIconOutlined);
                likedOrDisklikedTitle = 'disliked';
            }   

            Swal.fire({
                icon: 'success',
                title: `Succesfully ${likedOrDisklikedTitle}`,
                showConfirmButton: false,
                timer: 1000
            });
        },
        error: function (response) {
            handleErrorWithSwalAndRedirect(response);
        }
    });
});

function getCommentsHtml(commentsArr) {
    let result = ``;
    for (const singleComment of commentsArr) {
        result += `<li class="single-comment">
                        <span class="single-comment-left-side">
                            <p class="single-comment-date">
								${singleComment.authorName}:						
							</p>
							<p class="single-comment-text">
								${singleComment.content}
							</p>
                        </span>		
                        <span>
                            <p class="single-comment-author">
							    ${getDateFormatedLongMonthShortDayYear(singleComment.dateOfCreation)}
						    </p>
                        </span>
                    </li>`;
    }

    return result;
}

function getPostsGridData(newPageNumber, shouldGetLastPage) {
    let skip = (newPageNumber * PostGridPageSize) - PostGridPageSize;
    let contentSearchTerm = $(postDateContainsInputId).val();
    let orderByDateAscendingInputValue = $(postDateSortSelect).val();
    let orderByDateAscending = orderByDateAscendingInputValue != "null" ? !!(parseInt(orderByDateAscendingInputValue)) : null;
    let dataToSend = JSON.stringify({
        take: PostGridPageSize,
        skip: skip,
        shouldGetLastPage: shouldGetLastPage,
        threadId: $(postsAgGrid).attr(postsAgGridDataThreadIdAttribute),
        commentsTakeAmount: CommentsPageSize,
        contentSearchTerm: contentSearchTerm,
        orderByDateAscending: orderByDateAscending
    });

    $.ajax({
        url: getPostsDataUrl,
        type: "POST",
        data: dataToSend,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            postsGridOptions.api.setRowData(response.data);
            currentPostGridPageNumber = response.currentPage;
            drawPagination(response.totalCount, response.currentPage, pagesElementsArr, PostGridPageSize);           
        },
        error: function (response) {
            handleErrorWithSwalAndRedirect(response);
        }
    });
}