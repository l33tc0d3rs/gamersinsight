﻿const userSettingsFormId = '#user-settings-form';

cropper.start(document.getElementById("avatar-canvas"), 1);

//INIT
getUserSettingsSideGridView(`/api/UserApi/GetUserSettingsGridView`);

function handleFileSelect() {
	// this function will be called when the file input below is changed
	var file = document.getElementById("avatar-input").files[0];  // get a reference to the selected file

	var reader = new FileReader(); // create a file reader
	// set an onload function to show the image in cropper once it has been loaded
	reader.onload = function (event) {
		var data = event.target.result; // the "data url" of the image
		cropper.showImage(data); // hand this to cropper, it will be displayed
	};

	reader.readAsDataURL(file); // this loads the file as a data url calling the function above once done

	$("#avatar-file-name-holder").text(`File name: ${file.name}`);
}

$(userSettingsFormId).submit(function (event) {
	event.preventDefault();

	var formValues = {};
	$.each($(userSettingsFormId).serializeArray(), function (i, field) {
		formValues[field.name] = field.value;
	});

	let dataToSend = JSON.stringify({
		email: formValues.email,
		displayName: formValues.displayName,
		currentPassword: formValues.currentPassword,
		newPassword: formValues.newPassword,
		repeatPassword: formValues.repeatPassword
	});

    $.ajax({
		url: "/api/UserApi/UpdateUser",
        type: "PUT",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {
            Swal.fire(
                'Success!',
                response.message,
                'success',
            ).then((result) => {
                location.reload();
            });                               
        },
        error: function (response) {
            Swal.fire(
                'Error!',
                response.responseJSON.message,
                'error'
            );
        }
    });	
});

$('#change-user-avatar-button').click(function () {
	$('#change-avatar-modal').modal('show');
});

function getUserSettingsSideGridView(url) {
	$.ajax({
		url: url,
		type: "GET",
		contentType: "application/json; charset=utf-8",
		success: function (response) {
			$("#user-settings-right-panel-container").empty();
			$("#user-settings-right-panel-container").append(response);
		},
		error: function () {
		}
	});
}

$('#get-user-settings-side').click(function () {
	let url = `/User/GetUserSettingsGridView`;
	getUserSettingsSideGridView(url);
});

$('#get-management-users-side').click(function () {
	$.ajax({
		url: `/User/GetManagementGridView`,
		type: "GET",
		contentType: "application/json; charset=utf-8",
		success: function (response) {
			$("#user-settings-right-panel-container").empty();
			$("#user-settings-right-panel-container").append(response);
			initGetManagementsGridData(1, false);
		},
		error: function () {
		}
	});	
});

$('#get-conversations-side').click(function () {
	getConversationsSideGridView();
});

function getConversationsSideGridView() {
	$.ajax({
		url: `/Conversation/GetConversationsGridView`,
		type: "GET",
		contentType: "application/json; charset=utf-8",
		success: function (response) {
			$("#user-settings-right-panel-container").empty();
			$("#user-settings-right-panel-container").append(response);
			initGetConversationsGridData(1, false);
		},
		error: function () {
		}
	});
}

$("#update-avatar-submit-button").click(function (event) {
	event.preventDefault();
	debugger;
	var selectedFile = cropper.getCroppedImageBlob();

	var fdata = new FormData();
	fdata.append("Image", selectedFile);

	$.ajax({
		url: "/api/UserApi/UpdateAvatar",
		type: "POST",
		data: fdata,
		cache: false,
		contentType: false,
		processData: false,
		success: function (result) {
			$('#change-avatar-modal').modal('hide');
			$("#user-avatar-holder-img").attr("src", result.newAvatarFullUrl);
		},
		error: function (result) {
			Swal.fire(
				'Error!',
				result.responseJSON.message,
				'error'
			);
		}
	});
})

