﻿const CategoryGridPageSize = 5;

const getCategoriesDataUrl = "/Category/Categories";

const createNewCategoryButtonId = "#create-new-category-button";
const createNewCategoryFormId = "#create-new-category-form";
const categoriesAgGrid = "#categories-ag-grid";
const editCategoryFormId = "#edit-category-form";

const newCategoryButtonClass = ".new-category-button";
const firstPageButtonClass = ".categories-grid-fist-page-button";
const lastPageButtonClass = ".categories-grid-last-page-button";
const categoriesGridPagesContainerClass = ".categories-grid-pages-container";

const deleteCategoryClass = '.delete-category-button';
const editCategoryClass = '.edit-category-button';
const categoriesEditButtonIdAttribute = 'data-id';
const updateCategoryButtonId = "#update-category-button";

const noThreadsYetMessage = "No Threads yet!";
const noActivityDateYetMessage = "No Activity yet!"

const pagesElementsArr = [
    {
        firstPageButtonId: "#top-categories-grid-first-page-button",
        lastPageButtonId: "#top-categories-grid-last-page-button",
        pagesContainerId: "#top-categories-grid-pages-container"
    },
    {
        firstPageButtonId: "#bottom-categories-grid-first-page-button",
        lastPageButtonId: "#bottom-categories-grid-last-page-button",
        pagesContainerId: "#bottom-categories-grid-pages-container"
    }
];

let currentCategoryGridPageNumber = 1;

//INIT
getCategoriesGridData(currentCategoryGridPageNumber);

const columnDefs = [
    {        
        headerName: "Category Name",
        field: "div",
        cellRenderer: function (params) {
            let htmlResult = `<div class="categiry-grid-text-info nk-forum">
                                 <div class="nk-forum-title">
                                     <h4>
                                        <a class="categiry-grid-text-info-name" href='/Thread/GetThreadsGridView?id=${params.data.id}'>
                                            ${params.data.categoryName}
                                        </a>                                        
                                     </h4>                                    
                                 </div>                                
                                 <div class="categiry-grid-text-info-description">
                                     ${params.data.description}
                                 </div>
                              </div>`;
            return htmlResult;
        }
    },
    {
        headerName: "Actions",
        field: "button",
        cellRenderer: function (params) {
            debugger;
            let userRole = getCurrentUserRole();
            if (userRole === ADMIN_ROLE_NAME) {
                return `<button class="edit-category-button nk-btn nk-btn-color-main-3" row-index=${params.rowIndex} data-id=${params.data.id}>Edit</button>
                        <button class="delete-category-button nk-btn nk-btn-color-main-5" row-index=${params.rowIndex} data-id=${params.data.id}>Delete</button>`;
            }
            
        }
    },
    {       
        headerName: "Threads Count",
        field: "div",
        cellClass: "right-flexed-items",
        cellRenderer: function (params) {
            return `<span>${params.data.threadsCount} threads</span>`
        }
    },   
    {       
        headerName: "Last Activity",
        field: "div",
        cellClass: "right-flexed-items",
        cellRenderer: function (params) {
            let threadGetUrl = "#";
            let title = noThreadsYetMessage;
            let dateFormated = noActivityDateYetMessage;
            if (params.data.lastActiveThread) {
                threadGetUrl = `/thread/thread?id=${params.data.lastActiveThread.threadId}`;
                title = params.data.lastActiveThread.threadTitle;
                dateFormated = getDateFormatedLongMonthShortDayYear(params.data.lastActiveThread.lastActivityTime);
            }
            
            let htmlResult = `<div class="category-grid-activity-outer-container">
                                <div class="nk-forum-icon icon-category-grid-container">
                                    <span class="ion-chatboxes icon-category-grid"></span>
                                </div>
                                <div class="category-grid-activity">
                                    <div class="category-grid-activity-title">
                                        <a class="category-grid-activity-title-link" href="${threadGetUrl}">
                                            ${title}
                                        </a>                                         
                                    </div>
                                    <div class="category-grid-activity-date">
                                         ${dateFormated}
                                    </div>
                                </div>
                              </div>`
                             
            return htmlResult;
        }
    }
];

const gridOptions = {
    rowHeight: 115,
    columnDefs: columnDefs,
    rowData: [],
    headerHeight: 0,
    autoHeight: true
};

const eGridDiv = document.querySelector(categoriesAgGrid);
new agGrid.Grid(eGridDiv, gridOptions);
gridOptions.api.sizeColumnsToFit();

$(createNewCategoryButtonId).click(function (event) {
    var formValues = {};
    $.each($(createNewCategoryFormId).serializeArray(), function (i, field) {
        formValues[field.name] = field.value;
    });

    let dataToSend = JSON.stringify({
        categoryName: formValues.categoryName,
        description: formValues.categoryDescription,
        isNsfw: formValues.isNsfw
    });

    $.ajax({
        url: "/api/CategoryApi/Category",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {
            $('#create-category-modal').modal('hide');
            getCategoriesGridData(currentCategoryGridPageNumber, false);
        },
        error: function () {

        }
    });
});

$(categoriesAgGrid).on('click', deleteCategoryClass, function (event) {
    let categoryId = parseInt(($(event)[0]).currentTarget.getAttribute(categoriesEditButtonIdAttribute));

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        deleteCategory(categoryId);        
    })
});

function deleteCategory(categoryId) {
    $.ajax({
        url: `/api/CategoryApi/Category?id=${categoryId}`,
        contentType: "application/json; charset=utf-8",        
        type: 'DELETE',
        success: function (response) {
            if (response.isSuccess) {
                if (response.isSuccess) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
                //TODO -- Will bug when pages get less than previous current
                getCategoriesGridData(currentCategoryGridPageNumber, false);
            }           
        },
        error: function () {
        }
    });   
}

$(categoriesAgGrid).on('click', editCategoryClass, function (event) {
    let categoryId = parseInt(($(event)[0]).currentTarget.getAttribute(categoriesEditButtonIdAttribute));

    $.ajax({
        url: `/api/CategoryApi/Category?Id=${categoryId}`,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            if (response.isSuccess) {
                $('#edit-category-modal').modal('show');
                document.getElementById("edit-category-name-input").setAttribute('value', response.categoryName);
                document.getElementById("edit-category-description-input").setAttribute('value', response.description);
                document.getElementById("edit-category-isnsfw-checkbox").setAttribute('value', response.nsfw);
                document.getElementById("edit-category-id-input").setAttribute('value', response.id);
            }            
        },
        error: function () {
        }
    });   
});


$(updateCategoryButtonId).click(function (event) {
    var formValues = {};
    $.each($(editCategoryFormId).serializeArray(), function (i, field) {
        formValues[field.name] = field.value;
    });

    formValues["id"] = parseInt($('#edit-category-id-input').val());
    formValues["isNsfw"] = Boolean($('#edit-category-isnsfw-checkbox').val());

    let dataToSend = JSON.stringify({
        id: formValues.id,
        categoryName: formValues.categoryName,
        description: formValues.categoryDescription,
        isNsfw: formValues.isNsfw
    });

    $.ajax({
        url: "/api/CategoryApi/Category",
        type: "PUT",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {
            $('#edit-category-modal').modal('hide');
            getCategoriesGridData(currentCategoryGridPageNumber, false);
        },
        error: function (response) {

        }
    });
});

$(firstPageButtonClass).click(function (event) {
    if (currentCategoryGridPageNumber > 1) {
        currentCategoryGridPageNumber = 1;
        getCategoriesGridData(currentCategoryGridPageNumber, false);
    }
});

$(lastPageButtonClass).click(function (event) {
    getCategoriesGridData(currentCategoryGridPageNumber, true);
});

$(categoriesGridPagesContainerClass).on('click', POSTS_GRID_PAGE_NUMBER_CLASS, function (event) {
    let pageNumber = parseInt((($(event))[0]).target.text);
    getCategoriesGridData(pageNumber, false);
});

function getCategoriesGridData(newPageNumber, shouldGetLastPage) {
    let skip = (newPageNumber * CategoryGridPageSize) - CategoryGridPageSize;

    let dataToSend = JSON.stringify({
        take: CategoryGridPageSize,
        skip: skip,
        shouldGetLastPage: shouldGetLastPage
    });

    $.ajax({
        url: getCategoriesDataUrl,
        type: "POST",
        data: dataToSend,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            gridOptions.api.setRowData(response.data);
            currentCategoryGridPageNumber = response.currentPage;
            drawPagination(response.totalCount, response.currentPage, pagesElementsArr, CategoryGridPageSize);
        },
        error: function (response) {
          //TODO -- Bad message toastr?
        }
    });
}
