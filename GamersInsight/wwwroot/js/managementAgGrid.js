﻿const managementGridPageSize = 5;
const getManagementsDataUrl = "/api/UserApi/Management";

const createNewManagementButtonId = "#create-new-management-button";
const createNewManagementFormId = "#create-new-management-form";
const managementsAgGrid = "#managements-ag-grid";
const editManagementFormId = "#edit-management-form";

const newManagementButtonClass = ".new-management-button";
const firstPageButtonClassManagement = ".managements-grid-fist-page-button";
const lastPageButtonClassManagement = ".managements-grid-last-page-button";
const managementsGridPagesContainerClass = ".managements-grid-pages-container";
const deleteManagementClass = '.delete-management-button';
const editManagementClass = '.edit-management-button';
const managementsEditButtonIdAttribute = 'data-id';
const updateManagementButtonId = "#update-management-button";
const managementGridTextInfoNameClass = '.management-grid-text-info-name';
const otherParticipantIdAttributeManagement = 'data-other-participant-id';
const showManagementModal = 'show-management-modal';
const userSettingsRightPanelContainerId = "#user-settings-right-panel-container";

const noThreadsYetMessageManagement = "No Threads yet!";
const noActivityDateYetMessageManagement = "No Activity yet!";

const managementGridPageButton = "management-grid-page-button";

//GridProperties
var columnDefsManagement;
var managementsGridOptions;
var eGridDivManagement;

const pagesElementsArrManagement = [
    {
        firstPageButtonId: "#top-managements-grid-first-page-button",
        lastPageButtonId: "#top-managements-grid-last-page-button",
        pagesContainerId: "#top-managements-grid-pages-container"
    },
    {
        firstPageButtonId: "#bottom-managements-grid-first-page-button",
        lastPageButtonId: "#bottom-managements-grid-last-page-button",
        pagesContainerId: "#bottom-managements-grid-pages-container"
    }
];

let currentManagementGridPageNumber = 1;

function initGetManagementsGridData(currentManagementGridPageNumber, shouldGetLastPage) {
    initManagementGridStructure();
    getManagementsGridData(currentManagementGridPageNumber, shouldGetLastPage);
}

function initManagementGridStructure() {
    columnDefsManagement = [
        {
            headerName: "Management Name",
            field: "div",
            cellRenderer: function (params) {
                let userRole = getCurrentUserRole();
                let htmlResult = `<div class="management-grid-text-info management-grid-inner-div nk-forum" ${otherParticipantIdAttributeManagement}=${params.data.id}>
                                 <div class="nk-forum-title">
                                     <p>
                                        ${params.data.userName}
                                     </p>                                    
                                 </div>
                                <div class="">
                                     <p>
                                       ${params.data.email}
                                     </p>
                                 </div>
                                 <div class="nk-forum-title">
                                     <button class="start-conversation nk-btn link-effect-4 ready">
                                            Start Conversation
                                     </button >
                                 </div>
                                 <div class="nk-forum-title">`
                    + ((userRole === ADMIN_ROLE_NAME && params.data.isBlocked) ?
                        `<button class="unblock-user-button nk-btn nk-btn-md link-effect-1 nk-btn-color-main-2 ready">
                                            Unblock User
                                        </button > `
                        : ``)
                    + ((userRole === ADMIN_ROLE_NAME && !params.data.isBlocked) ?
                        `<button class="block-user-button nk-btn nk-btn-color-main-5">
                                            Block User
                                         </button > `
                        : ``) + `
                                 </div>
                              </div>`;
                return htmlResult;
            }
        }
    ];

    managementsGridOptions = {
        rowHeight: 115,
        columnDefs: columnDefsManagement,
        rowData: [],
        headerHeight: 0,
        //autoHeight: true
    };

    eGridDivManagement = document.querySelector(managementsAgGrid);
    new agGrid.Grid(eGridDivManagement, managementsGridOptions);
    managementsGridOptions.api.setDomLayout('autoHeight');
    managementsGridOptions.api.sizeColumnsToFit();
}

$(userSettingsRightPanelContainerId).on('click', firstPageButtonClassManagement, function (event) {
    if (currentManagementGridPageNumber > 1) {
        currentManagementGridPageNumber = 1;
        getManagementsGridData(currentManagementGridPageNumber, false);
    }
});

$(userSettingsRightPanelContainerId).on('click', lastPageButtonClassManagement, function (event) {
    getManagementsGridData(currentManagementGridPageNumber, true);
});

$(userSettingsRightPanelContainerId).on('click', ".block-user-button", function (event) {
    let managedUserId = parseInt(event.currentTarget.closest(".management-grid-inner-div").getAttribute(otherParticipantIdAttributeManagement));
    toggleUserBlock(managedUserId, true);
});

$(userSettingsRightPanelContainerId).on('click', ".unblock-user-button", function (event) {
    let managedUserId = parseInt(event.currentTarget.closest(".management-grid-inner-div").getAttribute(otherParticipantIdAttributeManagement));
    toggleUserBlock(managedUserId, false);
});

$(userSettingsRightPanelContainerId).on('click', ".start-conversation", function (event) {
    let otherParticipantId = parseInt(event.currentTarget.closest(".management-grid-inner-div").getAttribute(otherParticipantIdAttributeManagement));
    document.getElementById("start-new-conversation-modal").setAttribute(otherParticipantIdAttributeManagement, otherParticipantId);
    $(`#start-new-conversation-modal`).modal('show');
});

$("#start-new-conversation-button").click(function (event) {
    event.preventDefault();
    debugger;
    let otherParticipantId = parseInt(document.getElementById("start-new-conversation-modal").getAttribute(otherParticipantIdAttributeManagement));
    var formValues = {};
    $.each($("#start-new-conversation-form").serializeArray(), function (i, field) {
        formValues[field.name] = field.value;
    });

    let dataToSend = JSON.stringify({
        messageContent: formValues.messageContent,
        receiverId: otherParticipantId
    });

    $.ajax({
        url: "/Message/Message",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {
            $(`#start-new-conversation-modal`).modal('hide');
            getConversationsSideGridView();
        },
        error: function () {
        }
    });

});

$(userSettingsRightPanelContainerId).on('click', managementGridPageButton, function (event) {
    let pageNumber = parseInt((($(event))[0]).target.text);
    getManagementsGridData(pageNumber, false);
});


function toggleUserBlock(managedUserId, shouldBlock) {
    debugger;

    let dataToSend = JSON.stringify({
        managedUserId: managedUserId
    });

    let actionUrl;

    if (shouldBlock) {
        actionUrl = "/User/BlockUser"
    }
    else {
        actionUrl = "/User/UnblockUser"
    }

    $.ajax({
        url: actionUrl,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {

            Swal.fire(
                'Success!',
                response.message,
                'success',
            ).then((result) => {
                getManagementsGridData(currentManagementGridPageNumber, false);
            });
           
        },
        error: function (response) {
            Swal.fire(
                'Error!',
                response.responseJSON.message,
                'error'
            );
        }
    });
}


function getManagementsGridData(newPageNumber, shouldGetLastPage) {
    let skip = (newPageNumber * managementGridPageSize) - managementGridPageSize;

    let dataToSend = JSON.stringify({
        take: managementGridPageSize,
        skip: skip,
        shouldGetLastPage: shouldGetLastPage
    });

    $.ajax({
        url: getManagementsDataUrl,
        type: "POST",
        data: dataToSend,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            managementsGridOptions.api.setRowData(response.data);
            currentManagementGridPageNumber = response.currentPage;
            drawPagination(response.totalCount, response.currentPage, pagesElementsArrManagement, managementGridPageSize, managementGridPageButton);
        },
        error: function (response) {
            //TODO -- Bad message toastr?
        }
    });
}
