﻿function drawPagination(totalElements, currentPage, pagesElementsArr, gridPageSize, userSettingsPaginationClass = null) {
    const disabledClass = "disabled"; //TODO - Constants
    let numberOfPages = 0;
    if (totalElements === 0) {
        numberOfPages = 1
    }
    else if (totalElements % gridPageSize === 0) {
        numberOfPages = Math.floor(totalElements / gridPageSize)
    }
    else {
        numberOfPages = Math.floor(totalElements / gridPageSize) + 1;
    }

    pagesElementsArr.forEach(function (pageElement) {
        drawPageButtonsHtml(pageElement.firstPageButtonId, pageElement.lastPageButtonId, pageElement.pagesContainerId, currentPage, numberOfPages, totalElements, disabledClass, userSettingsPaginationClass);
    });
}

function drawPageButtonsHtml(fistPageButton, lastPageButton, pagesContainerId, currentPage, numberOfPages, totalElements, disabledClass, userSettingsPaginationClass) {
    let $fistPageButton = $(fistPageButton);
    let $lastPageButton = $(lastPageButton);

    let html = "";

    //Note: This is first page
    if (numberOfPages === 1 && totalElements === 0) {
        html += buildSinglePageHtml(1, true, userSettingsPaginationClass);
        $lastPageButton.addClass(disabledClass);
        $fistPageButton.addClass(disabledClass);
    }
    else if (currentPage === 1 && totalElements >= 1) {
        html += buildSinglePageHtml(currentPage, true, userSettingsPaginationClass);
        $fistPageButton.addClass(disabledClass);
        if (numberOfPages > 1) {
            html += buildSinglePageHtml(currentPage + 1, false, userSettingsPaginationClass);
            $lastPageButton.removeClass(disabledClass);
        }
        else if (numberOfPages == 1) {
            $fistPageButton.addClass(disabledClass);
            $lastPageButton.addClass(disabledClass);
        }
    }
    //Note: This is last page with more than one pages
    else if (currentPage == numberOfPages) {
        html += buildSinglePageHtml(currentPage - 1, false, userSettingsPaginationClass);
        html += buildSinglePageHtml(currentPage, true, userSettingsPaginationClass);
        $lastPageButton.addClass(disabledClass);
        $fistPageButton.removeClass(disabledClass);
    }
    //Note: This is neiher last nor first page with more than one pages
    else {
        $fistPageButton.removeClass(disabledClass);
        $lastPageButton.removeClass(disabledClass);

        html += buildSinglePageHtml(currentPage - 1, false, userSettingsPaginationClass);
        html += buildSinglePageHtml(currentPage, true, userSettingsPaginationClass);
        html += buildSinglePageHtml(currentPage + 1, false, userSettingsPaginationClass);
    }

    $(pagesContainerId).html(html);
}

function buildSinglePageHtml(pageNumber, isCurrent, userSettingsPaginationClass) {
    let result;
    let gridPageButtonClass = userSettingsPaginationClass == null ? 'grid-page-button' : userSettingsPaginationClass
    if (isCurrent) {
        result = `<a href="#" class="nk-pagination-current-white ${gridPageButtonClass}">${pageNumber}</a>`;
    }
    else {
        result = `<a href="#" class=" ${gridPageButtonClass}">${pageNumber}</a>`;
    }

    return result;
}