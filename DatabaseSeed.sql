INSERT INTO Roles (RoleName, Alies)
VALUES ('User', 'Commoner'), ('Moderator', 'Knight'), ('Admin', 'King'), ('NotVerified', 'To be verified');

INSERT INTO Categories (CategoryName, Description, NSFW)
VALUES ('RPG', 'For the role players', 0),
('Shooters', 'How good is your aim', 0),
('Racing', 'How fast can you go', 0),
('MMO', 'How many is too many', 0), 
('Mobile', 'Why does this exist even', 0),
('Strategy', 'Micro vs Macro', 0),
('Sandbox', 'Show your creativity', 0),
('Simulation', 'Brand new universe', 0),
('Adventure', 'For the explorers', 0),
('Horror', 'Bloody awsome', 1),
('Sims', 'Becase we all know how you play this', 1);

INSERT INTO Users (UserName, Password, Email, DisplayName, Avatar, DateOfCreation, DateOfBirth, RoleId, IsActivated, IsBlocked, Rating)
VALUES ('Test', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'TestMail@test-com', 'TestUser', 'wwwroot/images/test-jpg', '2020-10-10', '2000-01-05', 1, 1, 0, 0),
('Johnny', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'Johnny@test-com', 'JohnnyS', 'wwwroot/images/johnny-jpg', '2020-10-10', '2000-01-05', 1, 1, 0, 0),
('Ronny', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'Ronny@test-com', 'R0n3', 'wwwroot/images/rony-jpg', '2020-10-10', '2000-03-07', 1, 1, 0, 0),
('Tony', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'Tony@test-com', 'T0ngun', 'wwwroot/images/tony-jpg', '2020-10-10', '2000-04-05', 1, 1, 0, 0),
('Mony', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'Mony@test-com', 'MonneyM', 'wwwroot/images/mony-jpg', '2020-10-10', '2000-09-08', 1, 1, 0, 0),
('Robbert', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'Robbert@test-com', 'RobRobster', 'wwwroot/images/Rob-jpg', '2020-10-10', '2000-09-05', 1, 1, 0, 0),
('Jim', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'Jim@test-com', 'JimJim', 'wwwroot/images/jim-jpg', '2020-10-10', '2000-03-05', 1, 1, 0, 0),
('Jack', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'Jack@test-com', 'JackD', 'wwwroot/images/jack-jpg', '2020-10-10', '2000-04-09', 1, 1, 0, 0),
('James', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'James@test-com', 'J4m3s', 'wwwroot/images/james-jpg', '2020-10-10', '2000-02-03', 1, 1, 0, 0),
('Jameson', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'Jameson@test-com', 'Jameson1', 'wwwroot/images/jameson-jpg', '2020-10-10', '2000-06-06', 1, 1, 0, 0),
('George', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'George@test-com', 'Joro', 'wwwroot/images/george-jpg', '2020-10-10', '2000-07-07', 1, 1, 0, 10),
('DummyOne', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'DummyOne@test-com', 'DUMDUM', 'wwwroot/images/dummy-jpg', '2020-10-10', '2000-06-05', 1, 1, 0, 10),
('AllMighty', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'AllMighty@test-com', 'MightyOne', 'wwwroot/images/one-jpg', '2020-10-10', '2000-05-05', 1, 1, 0, 100),
('TheKing', '$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq', 'TheKing@test-com', 'KING!', 'wwwroot/images/King-jpg', '2020-10-10', '1989-07-22', 3, 1, 0, 10000);

INSERT INTO Threads (Id, ThreadName, CategoryId, CreatorId, DateOfCreation)
VALUES (Convert(uniqueidentifier,N'9a5ed572-3bc5-4918-a03b-82934a2017ec'), 'New Games', 1, 2, '2021-01-02'),
(Convert(uniqueidentifier,N'3554bc35-af7f-4d9e-b17f-426720c111f7'), 'New Games', 2, 2, '2021-01-02'),
(Convert(uniqueidentifier,N'de6fda2a-b6ed-4d43-9211-8212230a6177'), 'New Games', 3, 2, '2021-01-02'),
(Convert(uniqueidentifier,N'55f8d69a-6772-402b-b9e2-706f73d9f2cf'), 'New Games', 4, 2, '2021-01-02'),
(Convert(uniqueidentifier,N'3ef77fbd-23b1-4cda-a450-7f972ad3d749'), 'New Games', 5, 2, '2021-01-02'),
(Convert(uniqueidentifier,N'1884ac1f-6aab-4aab-bd5c-d4c5aada0120'), 'New Games', 6, 2, '2021-01-02'), 
(Convert(uniqueidentifier,N'5b3db90b-e94f-42b7-b558-07537c9a73c3'), 'New Games', 7, 2, '2021-01-02'), 
(Convert(uniqueidentifier,N'03004644-409e-4cac-8c8b-6591d721dbdd'), 'New Games', 8, 2, '2021-01-02'),
(Convert(uniqueidentifier,N'15c7164c-595e-4043-949a-0ffbf8aa5323'), 'New Games', 9, 2, '2021-02-03'), 
(Convert(uniqueidentifier,N'c6fbe616-3159-4be9-94e2-4264659385cb'), 'New Games', 10, 2, '2021-10-10'), 
(Convert(uniqueidentifier,N'08b73b93-a0f4-4115-9c96-5434400d9caf'), 'New Games', 11, 2, '2021-10-10'),
(Convert(uniqueidentifier,N'079dbf17-7ccc-4510-bd1f-de7a259dec0f'), 'TheMeta', 1, 2, '2021-10-10'), 
(Convert(uniqueidentifier,N'8980c92b-4df9-4772-92cf-feba03567f0d'), 'Pro tips', 2, 2, '2021-10-10'), 
(Convert(uniqueidentifier,N'7de3f5f1-8ee0-4997-a7be-80ed956ebb49'), 'Drifting', 3, 2, '2021-03-01'),
(Convert(uniqueidentifier,N'311e640d-5fa5-485c-ac4e-1258b0704e36'), 'imrpovise', 4, 2, '2021-10-10'), 
(Convert(uniqueidentifier,N'02053ac0-c64b-4aef-bced-3e4c76c5fd47'), 'Anything', 5, 2, '2021-10-10'), 
(Convert(uniqueidentifier,N'1c762c44-d9a5-40a8-9799-6d4ab36aa176'), 'Improve APM', 6, 2, '2021-10-10'), 
(Convert(uniqueidentifier,N'52813ea4-c2e7-4ff2-83ee-e85d1b07953d'), 'Creativity', 7, 14, '2021-06-11'), 
(Convert(uniqueidentifier,N'ced7467c-af3c-4c09-9ea2-d213d1a672c9'), '3DVR', 8, 2, '2021-06-18'), 
(Convert(uniqueidentifier,N'3ca50c3e-a422-4c1f-9e11-3666a58810c2'), 'Any Ideas?', 9, 2, '2021-10-10'),
(Convert(uniqueidentifier,N'261ed7c6-8609-4a1c-adaf-0dca1aaf9bb5'), 'Not Sure', 1, 14, '2021-10-10'), 
(Convert(uniqueidentifier,N'a0da73b7-aede-4791-b6e7-c0781e5dce2c'), 'Joy Of Gaming', 1, 11, '2021-06-07'), 
(Convert(uniqueidentifier,N'417ba647-b265-49a1-a39a-80d998ababb7'), 'How good?', 1, 8, '2021-02-01'),
(Convert(uniqueidentifier,N'7e3d2963-4da1-41a7-a190-2470011225a4'), 'How To', 1, 14, '2021-03-03'), 
(Convert(uniqueidentifier,N'454d6dc8-d49a-4e2b-b132-d7cba74d3e76'), 'Controllers', 1, 12, '2021-11-11'), 
(Convert(uniqueidentifier,N'85ae3a3d-12ff-4d35-a56f-aa2f63ff3b86'), 'Trolls', 1, 7, '2021-05-10'),
(Convert(uniqueidentifier,N'346f03db-ece4-4fd6-b190-d4f0bf88ad02'), 'No Life', 1, 12, '2021-04-11'), 
(Convert(uniqueidentifier,N'd3e82346-d6e3-402a-8563-cfa0aecc0b6e'), 'Playtrough', 1, 12, '2021-08-10');


INSERT INTO Posts (Id,Title, Content, ThreadId, CreatorId, DateofCreation, Rating)
VALUES (Convert(uniqueidentifier,N'c79894aa-997d-4a97-9ffc-2290d791b0c4'), 'First Post', 'Filling the empty space.','9a5ed572-3bc5-4918-a03b-82934a2017ec', 2, '2021-01-02', 0),
(Convert(uniqueidentifier,N'1e4ee427-3299-44a6-8394-e09129196c1d'), 'When Will', 'How is this okay?', '9a5ed572-3bc5-4918-a03b-82934a2017ec', 2, '2021-01-02', 0),
(Convert(uniqueidentifier,N'57f18194-fea0-460b-bcbf-4fb4021c0f06'), 'This Ever', 'Excepteur sint occaecat 	.', '9a5ed572-3bc5-4918-a03b-82934a2017ec', 3, '2021-01-03', 0),
(Convert(uniqueidentifier,N'496362c9-f551-4653-be87-37d38225be00'), 'Be A real', 'sunt in culpa qui officia deserunt.', '079dbf17-7ccc-4510-bd1f-de7a259dec0f', 2, '2021-01-04', 0),
(Convert(uniqueidentifier,N'f1f0d72a-332c-46cf-b248-e9cd2eb4229a'), 'Post about stuff', 'Enim nunc faucibus .', '3554bc35-af7f-4d9e-b17f-426720c111f7', 3, '2021-01-05', 0),
(Convert(uniqueidentifier,N'f80ae531-be6a-4243-8090-9fe85432bc90'), 'Gaming?', 'pellentesque sit amet porttitor.', '3554bc35-af7f-4d9e-b17f-426720c111f7', 4, '2021-01-06', 0),
(Convert(uniqueidentifier,N'972948f6-3647-4aa1-a383-98508b225e1f'), 'Get A', 'Mattis rhoncus.', '3554bc35-af7f-4d9e-b17f-426720c111f7', 3, '2021-01-06', 0),
(Convert(uniqueidentifier,N'eb2abe7b-2d60-42d6-a3e0-8ee7243ef888'), 'Real Life', 'Enim nunc faucibus.', '3554bc35-af7f-4d9e-b17f-426720c111f7', 5, '2021-01-06', 0),
(Convert(uniqueidentifier,N'2f9e681b-0199-4952-ba43-611fd6218d10'), 'How much is too much?', 'pellentesque sit amet.', '3554bc35-af7f-4d9e-b17f-426720c111f7', 6, '2021-01-07', 0),
(Convert(uniqueidentifier,N'fc99180c-d5cb-448d-9f83-8bd70037c144'), 'Hackers!', 'Mattis rhoncus.', '3554bc35-af7f-4d9e-b17f-426720c111f7', 4, '2021-01-07', 0),
(Convert(uniqueidentifier,N'ab268ffd-6c2c-4a31-8946-758a9d6873ff'), 'Beat this', 'Arcu odio ut sem .', '3554bc35-af7f-4d9e-b17f-426720c111f7', 5, '2021-01-08', 0),
(Convert(uniqueidentifier,N'd2d5211b-1d46-4cfa-8135-04fbbbe364dd'), 'Out of ideas', 'nulla pharetra diam.', '3554bc35-af7f-4d9e-b17f-426720c111f7', 7, '2021-01-09', 0),
(Convert(uniqueidentifier,N'72516b69-0bc0-49f0-a5da-d708ff31e688'), 'For this', 'Ac auctor augue.', '3554bc35-af7f-4d9e-b17f-426720c111f7', 2, '2021-01-09', 0),
(Convert(uniqueidentifier,N'562fa09a-28ed-48d3-b9e8-ea1f9c57f419'), 'How to', 'mauris augue neque.', '3554bc35-af7f-4d9e-b17f-426720c111f7', 4, '2021-01-09', 0),
(Convert(uniqueidentifier,N'b91cb614-9aee-4aab-9f15-40085f3fae6f'), 'Fill this', 'Sagittis nisl rhoncus.', '3554bc35-af7f-4d9e-b17f-426720c111f7', 3, '2021-01-09', 0),
(Convert(uniqueidentifier,N'0174b66c-4e68-433d-956c-744e1619388f'), 'With data', 'mattis rhoncus urna.', '3554bc35-af7f-4d9e-b17f-426720c111f7', 2, '2021-01-09', 0),
(Convert(uniqueidentifier,N'4bab217e-7c47-4a51-8090-95982149a5a5'), 'I have tried', 'Aliquam vestibulum morbi.', '8980c92b-4df9-4772-92cf-feba03567f0d', 3, '2021-10-10', 0),
(Convert(uniqueidentifier,N'1651df7a-837f-461a-bb5d-3865529062df'), 'So hard', 'blandit cursus risus.', '8980c92b-4df9-4772-92cf-feba03567f0d', 3, '2021-10-10', 0),
(Convert(uniqueidentifier,N'63ca1fd2-68d6-4f7e-969c-38f085a7c4c2'), 'And got so far', 'Eget felis eget nunc', '8980c92b-4df9-4772-92cf-feba03567f0d', 4, '2021-10-10', 0),
(Convert(uniqueidentifier,N'1492051d-ffea-4058-89ed-25b8ab9956e6'), 'But in the end', 'lobortis mattis aliquam', '8980c92b-4df9-4772-92cf-feba03567f0d', 4, '2021-10-10', 0),
(Convert(uniqueidentifier,N'65e2d7af-e737-4ea7-846d-b8c09b25c3c8'), 'It doesnt', 'Tempus egestas sed', '8980c92b-4df9-4772-92cf-feba03567f0d', 5, '2021-10-11', 0),
(Convert(uniqueidentifier,N'6959b9a8-bd53-48d6-8fea-ad81f329f6c8'), 'Really matter', 'sed risus pretium quam', '8980c92b-4df9-4772-92cf-feba03567f0d', 3, '2021-10-11', 0),
(Convert(uniqueidentifier,N'097caa9c-df2d-4697-90b9-382d8c779134'), '39 thieves', 'Diam quam nulla porttitor', '8980c92b-4df9-4772-92cf-feba03567f0d', 12, '2021-10-11', 0),
(Convert(uniqueidentifier,N'53693038-ca04-4bf4-9642-916821984964'), 'Is quicker', 'porttitor massa id neque aliquam', '7de3f5f1-8ee0-4997-a7be-80ed956ebb49', 11, '2021-03-01', 0),
(Convert(uniqueidentifier,N'e991d7b8-f04c-48cd-8247-674ca2092499'), 'Than 40 winks', 'Faucibus et molestie ac.', '7de3f5f1-8ee0-4997-a7be-80ed956ebb49', 5, '2021-03-01', 0),
(Convert(uniqueidentifier,N'ed687305-9f69-43f6-8c5a-38d29be321bd'), 'No regrets', 'Elementum pulvinar etiam non', '7de3f5f1-8ee0-4997-a7be-80ed956ebb49', 6, '2021-03-01', 0),
(Convert(uniqueidentifier,N'56187570-b67d-4f2d-b30b-e0b76a59f6b9'), 'Every dog', 'Ac turpis egestas integer.', '7de3f5f1-8ee0-4997-a7be-80ed956ebb49', 7, '2021-03-02', 0),
(Convert(uniqueidentifier,N'0354696d-82c0-4246-8004-286f9553e5f2'), 'Has its day', 'eget aliquet nibh praesent tri.', '7de3f5f1-8ee0-4997-a7be-80ed956ebb49', 8, '2021-03-02', 0),
(Convert(uniqueidentifier,N'2ea19db5-2430-46a7-bbfb-6da04669d691'), 'And today', ' Fermentum leo vel orci porta.', '7de3f5f1-8ee0-4997-a7be-80ed956ebb49', 7, '2021-03-02', 0),
(Convert(uniqueidentifier,N'c5051f81-7f69-40d4-b119-54a0b3d63c68'), 'It is mine', ' Bibendum arcu vitae.', '7de3f5f1-8ee0-4997-a7be-80ed956ebb49', 6, '2021-03-03', 0),
(Convert(uniqueidentifier,N'cdbc921d-1445-4ddd-83d7-caf63ddbf562'), 'Been there', 'elementum curabitur vitae nunc.', '7de3f5f1-8ee0-4997-a7be-80ed956ebb49', 5, '2021-03-03', 0),
(Convert(uniqueidentifier,N'e8bd90cf-c222-429c-ba14-07527688b1f5'), 'Done that', 'Sit amet commodo nulla.', '7de3f5f1-8ee0-4997-a7be-80ed956ebb49', 3, '2021-03-03', 0),
(Convert(uniqueidentifier,N'f28f4aa0-ada1-48fd-93f8-f6ef07054595'), 'Impressive', 'facilisi nullam vehicula ipsum .', '02053ac0-c64b-4aef-bced-3e4c76c5fd47', 2, '2021-10-10', 0),
(Convert(uniqueidentifier,N'87743a44-3241-4dfc-b660-7a2ae4a5e28b'), 'How far', 'Massa massa ultricies mi quis.', '02053ac0-c64b-4aef-bced-3e4c76c5fd47', 3, '2021-10-10', 0),
(Convert(uniqueidentifier,N'b00d9be2-d8e4-4289-b04f-6d39db7072b1'), 'Should you go', 'Sit amet tellus cras adipiscing.', '02053ac0-c64b-4aef-bced-3e4c76c5fd47', 4, '2021-10-11', 0),
(Convert(uniqueidentifier,N'b5d48d41-f514-4615-8946-006454612138'), 'To win', 'Ac tortor dignissim convallis.', '02053ac0-c64b-4aef-bced-3e4c76c5fd47', 2, '2021-10-11', 0),
(Convert(uniqueidentifier,N'e8ad86d2-fbec-430a-a461-77eb520a5bb3'), 'Faster?', 'Suspendisse in est ante in nibh.', '02053ac0-c64b-4aef-bced-3e4c76c5fd47', 5, '2021-10-11', 0),
(Convert(uniqueidentifier,N'22c705ed-57b0-41bc-9cb4-3098b41e9b8e'), 'Slower?', ' In vitae turpis massa sed.', '02053ac0-c64b-4aef-bced-3e4c76c5fd47', 6, '2021-10-12', 0),
(Convert(uniqueidentifier,N'94b9e76d-a3fe-489b-b82d-d1cd6ce3726c'), 'Jump on', 'In hac habitasse sadge.', '7de3f5f1-8ee0-4997-a7be-80ed956ebb49', 7, '2021-10-12', 1),
(Convert(uniqueidentifier,N'6369e6a3-6764-4171-bffd-116e448db038'), 'Jump off', 'Nullam quam dolor, egestas a.', '02053ac0-c64b-4aef-bced-3e4c76c5fd47', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'a4561b0d-e579-448d-9f99-6966825b9b44'), 'Map changes', 'Morbi eu elit erat.', '02053ac0-c64b-4aef-bced-3e4c76c5fd47', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'e7219ae9-206d-4c91-b002-965d15baa032'), 'Music bug', 'Suspendisse blandit sodales eros.', '7de3f5f1-8ee0-4997-a7be-80ed956ebb49', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'6188fbac-2e17-442d-be5d-c23e0b0c4ed3'), 'HACKERS!', 'ut gravida tortor sodales quis.', '02053ac0-c64b-4aef-bced-3e4c76c5fd47', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'430054e6-4f05-4997-a6a1-332b7d928151'), 'Dance Dance', 'Nulla ut venenatis dolor.', '079dbf17-7ccc-4510-bd1f-de7a259dec0f', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'd03ad245-cb12-46f4-9b1b-ba9193ac548c'), 'LAN party?', 'Nulla in bibendum nunc.', '079dbf17-7ccc-4510-bd1f-de7a259dec0f', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'ebde3ae9-a163-453d-a3d3-7f3ad1c8ef84'), 'Good old days', 'sit amet luctus tortor.', '079dbf17-7ccc-4510-bd1f-de7a259dec0f', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'e5612ddd-8944-4194-b768-a12131889579'), 'JazzHands', 'Etiam rhoncus ullamcorper aliquam.', '079dbf17-7ccc-4510-bd1f-de7a259dec0f', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'cf624d85-807e-4bd0-b27a-e0b5a7d84752'), 'LockNload', 'Duis porta imperdiet sem.', '261ed7c6-8609-4a1c-adaf-0dca1aaf9bb5', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'0b3630ff-4aaa-49cf-b2b5-4c6f58f3dc02'), 'Faster run?', 'Praesent sed quam in lacus.', '261ed7c6-8609-4a1c-adaf-0dca1aaf9bb5', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'08d0d621-08e0-488f-ac28-edab31fc906b'), 'Exploits abusers', 'egestas fermentum ut vel.', '261ed7c6-8609-4a1c-adaf-0dca1aaf9bb5', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'380d5547-980f-4e06-96d3-3d4926bca8d9'), 'Fishing Simulator', 'Curabitur venenatis, erat.', '261ed7c6-8609-4a1c-adaf-0dca1aaf9bb5', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'baea9b51-ba78-492b-ab8b-a94228d44b7c'), 'How to win vs hacker', 'lorem risus eleifend nisl.', 'a0da73b7-aede-4791-b6e7-c0781e5dce2c', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'b16a841a-a989-446d-b3af-30d5871e39c1'), 'Disconects', 'vel pulvinar odio mi at tellus.', 'a0da73b7-aede-4791-b6e7-c0781e5dce2c', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'f3eddf6c-82f3-48d8-afef-3d15557b56c9'), 'kole poluchi li?', 'Donec fringilla odio in.', 'a0da73b7-aede-4791-b6e7-c0781e5dce2c', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'ee32e4f7-a092-4dc2-b825-e8d0c8c53f56'), 'Magic the gathering', 'rutrum, non sagittis.', '7e3d2963-4da1-41a7-a190-2470011225a4', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'1c29aa91-968d-4f6e-9c8c-deb1a60c288d'), 'Board Games?', 'Curabitur nec est pulvinar.', '7e3d2963-4da1-41a7-a190-2470011225a4', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'a83c0121-18f9-4460-8937-cc7febe11eb6'), 'Newbies unite', 'luctus massa a, fermentum.', '7e3d2963-4da1-41a7-a190-2470011225a4', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'9629f26d-9d2d-471b-9383-0fb7ec67a15d'), 'L2P', 'In vestibulum, ipsum nec.', '454d6dc8-d49a-4e2b-b132-d7cba74d3e76', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'97778c9e-dd47-4cd3-aa16-118120c1af4b'), 'Video settings', 'cursus convallis, mi quam.', '454d6dc8-d49a-4e2b-b132-d7cba74d3e76', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'8479e6ac-8859-48e0-847c-232c19a4e5cb'), 'Any offers?', 'auctor leo, et scelerisque.', '454d6dc8-d49a-4e2b-b132-d7cba74d3e76', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'b08f1907-3152-4567-82b4-575744356428'), 'Launch incoming', 'nisi turpis sed erat.', '85ae3a3d-12ff-4d35-a56f-aa2f63ff3b86', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'1a3ee670-15e4-4d15-9b82-76b40c3e28ce'), 'New game seems good', 'Nullam dignissim est dui.', '85ae3a3d-12ff-4d35-a56f-aa2f63ff3b86', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'cb5ee479-82b3-42ee-b03b-b62a743604ef'), 'Lets talk', 'ut bibendum nunc efficitur.', '346f03db-ece4-4fd6-b190-d4f0bf88ad02', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'85107dae-a543-4d1e-a35e-be6c1e4f509f'), 'How to fix this?', 'Pellentesque posuere mauris.', '346f03db-ece4-4fd6-b190-d4f0bf88ad02', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'07a78081-84bb-494a-8f70-8b27db894628'), 'Pepehands', 'enim, luctus imperdiet.', '346f03db-ece4-4fd6-b190-d4f0bf88ad02', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'7b42cd54-fbaa-4844-9581-f8f4a4ea9240'), 'Almost there', 'Quisque viverra eu sapien.', 'd3e82346-d6e3-402a-8563-cfa0aecc0b6e', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'3394e0a1-3c79-4a0b-a8b6-9c5407e73bef'), 'Few more to go', 'Quisque rutrum quis arcu.', 'd3e82346-d6e3-402a-8563-cfa0aecc0b6e', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'891dbba8-cd2d-427d-b592-afbd74374f1c'), 'Achievement unlocked', 'Eu maximus. Morbi massa.', 'd3e82346-d6e3-402a-8563-cfa0aecc0b6e', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'5d457fb8-8de4-4a09-b5db-35ffc800b796'), 'WHEN?', 'Nullam et sollicitudin magna.', '417ba647-b265-49a1-a39a-80d998ababb7', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'ae8eec96-0150-4eee-a8f0-9185fb589380'), 'Now or never', 'Praesent in ante pulvinar', '417ba647-b265-49a1-a39a-80d998ababb7', 2, '2021-10-12', 0),
(Convert(uniqueidentifier,N'20b67c2f-8cfb-4792-a3c7-b540769823b6'), 'Finished it!', 'Aliquam sed nibh ut nunc.', '417ba647-b265-49a1-a39a-80d998ababb7', 2, '2021-10-12', 1);

/* Function */
CREATE OR ALTER FUNCTION inter_GetUserMessagesFunc (
    @CurrentUserId BIGINT,
	@OtherParticipantId BIGINT
)

RETURNS TABLE
AS
RETURN

SELECT [SenderId], 
[ReceiverId],
(SELECT top 1 (U.DisplayName) FROM dbo.Users U where U.Id = m.SenderId)
AS [SenderDisplayName],
(SELECT top 1 (u2.DisplayName) FROM dbo.Users U2 where U2.Id = m.ReceiverId)
AS [ReceiverDisplayName],
[m].[MessageContent] AS [Message],
[m].[TimeOfCreation]
FROM [Messages] AS [m]
WHERE (([m].[ReceiverId] = @CurrentUserId) AND ([m].[SenderId] = @OtherParticipantId))
 OR (([m].[ReceiverId] = @OtherParticipantId) AND ([m].[SenderId] = @CurrentUserId))
 
 
 /* PROCEDURE */
CREATE OR ALTER PROCEDURE api_GetUserMessagesCount
@CurrentUserId BIGINT,
@OtherParticipantId BIGINT,
@TotalCount int output
	
AS
BEGIN
SET NOCOUNT ON

SELECT @TotalCount = (SELECT Count(*) AS TotalCount FROM dbo.inter_GetUserMessagesFunc(@CurrentUserId, @OtherParticipantId))

END


/* PROCEDURE */

USE [GamersInsight_DB]
GO
/****** Object:  StoredProcedure [dbo].[api_GetUserMessagesPaginated]    Script Date: 28.11.2021 г. 23:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER   PROCEDURE [dbo].[api_GetUserMessagesPaginated]
@CurrentUserId BIGINT,
@OtherParticipantId BIGINT,
@Skip INT,
@Take INT

AS
BEGIN
SET NOCOUNT ON

SELECT [SenderId], [ReceiverId], [SenderDisplayName], [ReceiverDisplayName], [Message], [TimeOfCreation]
FROM dbo.inter_GetUserMessagesFunc(@CurrentUserId, @OtherParticipantId) GUMF
ORDER BY GUMF.[TimeOfCreation]
OFFSET @Skip ROWS
FETCH NEXT @Take ROWS ONLY
 
END