﻿namespace GamersInsight.DTOs.RequestModels
{
    public class GetCategoryByIdRequestModel
    {
        public long Id { get; set; }
    }
}
