﻿namespace GamersInsight.DTOs.RequestModels.User
{
    public class UnblockUserRequestModel
    {
        public long ManagedUserId { get; set; }
    }
}
