﻿namespace GamersInsight.DTOs.RequestModels
{
    public class UpdateAvatarExtendedRequestModel : UpdateAvatarRequestModel
    {        
        public long CurrentUserId { get; set; }
    }
}
