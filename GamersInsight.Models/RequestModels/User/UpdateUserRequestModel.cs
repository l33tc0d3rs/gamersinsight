﻿namespace GamersInsight.DTOs.RequestModels
{
    public class UpdateUserRequestModel
    {
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string RepeatPassword { get; set; }
    }
}
