﻿namespace GamersInsight.DTOs.RequestModels
{
    public class GetBlockedUserStatusRequestModel : GetPaginatedRequestModel
    {
        public long Id { get; set; }
    }
}
