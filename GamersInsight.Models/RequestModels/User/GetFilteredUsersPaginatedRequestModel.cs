﻿namespace GamersInsight.DTOs.RequestModels
{
    public class GetFilteredUsersPaginatedRequestModel : GetPaginatedRequestModel
    {
        public string FilterInput { get; set; }
    }
}
