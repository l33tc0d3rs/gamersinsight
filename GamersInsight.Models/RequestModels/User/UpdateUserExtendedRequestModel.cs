﻿namespace GamersInsight.DTOs.RequestModels
{
    public class UpdateUserExtendedRequestModel : UpdateUserRequestModel
    {
        public long CurrentUserId { get; set; }
    }
}
