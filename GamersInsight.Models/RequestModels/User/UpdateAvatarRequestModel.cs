﻿using Microsoft.AspNetCore.Http;

namespace GamersInsight.DTOs.RequestModels
{
    public class UpdateAvatarRequestModel
    {        
        public IFormFile Image { get; set; }
    }
}
