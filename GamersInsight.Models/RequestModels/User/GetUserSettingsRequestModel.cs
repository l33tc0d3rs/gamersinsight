﻿namespace GamersInsight.DTOs.RequestModels
{
    public class GetUserSettingsRequestModel
    {
        public long CurrentUserId { get; set; }
    }
}
