﻿namespace GamersInsight.DTOs.RequestModels.Conversation
{
    public class GetConversationsExtendedRequestModel : GetPaginatedRequestModel
    {
        public long CurrentUserId { get; set; }
    }
}
