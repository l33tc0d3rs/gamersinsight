﻿namespace GamersInsight.DTOs.RequestModels.Conversation
{
    public class StartConversationExtendedRequestModel : StartConversationRequestModel
    {
        public long SenderId { get; set; }
    }
}
