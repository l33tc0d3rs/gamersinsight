﻿namespace GamersInsight.DTOs.RequestModels.Conversation
{
    public class GetPaginatedConversationsRequestModel : GetPaginatedRequestModel
    {
        public long CurrentUserId { get; set; }
    }
}
