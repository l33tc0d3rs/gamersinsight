﻿namespace GamersInsight.DTOs.RequestModels.Conversation
{
    public class StartConversationRequestModel
    {
        public long ReceiverId { get; set; }

        public string MessageContent { get; set; }
    }
}
