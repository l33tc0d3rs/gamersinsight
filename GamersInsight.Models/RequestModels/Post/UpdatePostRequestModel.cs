﻿using System;

namespace GamersInsight.DTOs.RequestModels.Post
{
    public class UpdatePostRequestModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
