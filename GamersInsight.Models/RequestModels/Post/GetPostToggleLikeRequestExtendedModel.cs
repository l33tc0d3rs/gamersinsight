﻿namespace GamersInsight.DTOs.RequestModels.Post
{
    public class GetPostToggleLikeRequestExtendedModel : GetPostToggleLikeRequestModel
    {
        public long UserId { get; set; }        
    }
}
