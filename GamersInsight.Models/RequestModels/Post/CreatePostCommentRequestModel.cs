﻿using System;

namespace GamersInsight.DTOs.RequestModels
{
    public class CreatePostCommentRequestModel
    {
        public Guid PostId { get; set; }
        public string Content { get; set; }
    }
}
