﻿using System;

namespace GamersInsight.DTOs.RequestModels.Post
{
    public class GetPostToggleLikeRequestModel
    {
        public Guid PostId { get; set; }
        public bool ShouldLike { get; set; }
    }
}
