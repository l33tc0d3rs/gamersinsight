﻿namespace GamersInsight.DTOs.RequestModels.Post
{
    public class CreatePostCommentExtendedRequestModel : CreatePostCommentRequestModel
    {
        public long UserId { get; set; }
    }
}
