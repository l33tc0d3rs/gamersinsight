﻿namespace GamersInsight.DTOs.RequestModels.Post
{
    public class CreatePostExtendedRequestModel : CreatePostRequestModel
    {
        public long UserId { get; set; }
    }
}
