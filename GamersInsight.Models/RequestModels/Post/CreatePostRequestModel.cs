﻿using System;

namespace GamersInsight.DTOs.RequestModels.Post
{
    public class CreatePostRequestModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public Guid ThreadId { get; set; }
    }
}
