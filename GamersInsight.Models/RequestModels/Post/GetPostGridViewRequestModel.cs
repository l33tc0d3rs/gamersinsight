﻿using System;

namespace GamersInsight.DTOs.RequestModels.Post
{
    public class GetPostGridViewRequestModel
    {
        public Guid ThreadId { get; set; }
    }
}
