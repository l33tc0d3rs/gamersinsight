﻿namespace GamersInsight.DTOs.RequestModels.Post
{
    public class GetPagintedPostsRequestExtendedModel : GetPagintedPostsRequestModel
    {
        public long UserId { get; set; }
    }
}
