﻿using System;

namespace GamersInsight.DTOs.RequestModels.Post
{
    public class GetPagintedPostsRequestModel : GetPaginatedRequestModel
    {
        public Guid ThreadId { get; set; }
        public bool? OrderByDateAscending { get; set; }
        public string ContentSearchTerm { get; set; }
        public int CommentsTakeAmount { get; set; } = 3; //TODO -- Const
    }
}
