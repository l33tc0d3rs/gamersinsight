﻿using System;

namespace GamersInsight.DTOs.RequestModels.Post
{
    public class GetPostByIdRequestModel
    {
        public Guid Id { get; set; }
    }
}
