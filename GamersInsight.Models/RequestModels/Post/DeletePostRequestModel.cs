﻿using System;

namespace GamersInsight.DTOs.RequestModels.Post
{
    public class DeletePostRequestModel
    {
        public Guid Id { get; set; }
    }
}
