﻿using System;

namespace GamersInsight.DTOs.RequestModels.Post
{
    public class GetPostCommentsRequestModel
    {
        public Guid PostId { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; } = 3; //TODO -- Const
    }
}
