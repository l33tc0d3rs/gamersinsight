﻿namespace GamersInsight.DTOs.RequestModels
{
    public class GetExtendedCategoriesModel : GetPaginatedRequestModel
    {
        public bool IsNsfw { get; set; }
    }
}
