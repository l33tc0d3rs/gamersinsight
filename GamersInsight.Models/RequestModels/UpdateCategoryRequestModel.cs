﻿namespace GamersInsight.DTOs.RequestModels
{
    public class UpdateCategoryRequestModel
    {
        public long Id { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public bool IsNsfw { get; set; }
    }
}
