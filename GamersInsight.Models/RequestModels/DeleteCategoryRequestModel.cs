﻿namespace GamersInsight.DTOs.RequestModels
{
    public class DeleteCategoryRequestModel
    {
        public long Id { get; set; }
    }
}
