﻿namespace GamersInsight.DTOs.RequestModels
{
    public class CreateCategoryRequestModel
    {
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public bool NSFW { get; set; }
    }
}
