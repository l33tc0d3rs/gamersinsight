﻿namespace GamersInsight.DTOs.RequestModels
{
    public class GetPaginatedRequestModel
    {
        public bool ShouldGetLastPage { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; } = 10; //TODO : ADD TO CONSTANTS -> DEFAULT_PAGINATION_SIZE;
    }
}
