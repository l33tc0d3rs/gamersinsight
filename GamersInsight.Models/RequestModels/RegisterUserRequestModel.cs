﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GamersInsight.Models.RequestModels
{
    public class RegisterUserRequestModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string DisplayName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
    }
}