﻿namespace GamersInsight.DTOs.RequestModels.Threads
{
    public class CreateThreadExtendedRequestModel
    {
        public string ThreadName { get; set; }

        public long CategoryId { get; set; }

        public long CurrentUserId { get; set; }
    }
}
