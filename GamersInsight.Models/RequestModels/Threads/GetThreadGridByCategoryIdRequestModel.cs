﻿namespace GamersInsight.DTOs.RequestModels.Threads
{
    public class GetThreadGridByCategoryIdRequestModel
    {
        public long Id { get; set; }
    }
}
