﻿namespace GamersInsight.DTOs.RequestModels.Threads
{
    public class CreateExtendedThreadRequestModel :GetPaginatedRequestModel
    {
        public long UserId { get; set; }
    }
}
