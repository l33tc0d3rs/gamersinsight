﻿using System;

namespace GamersInsight.DTOs.RequestModels.Threads
{
    public class DeleteThreadRequestModel
    {
        public Guid Id { get; set; }
    }
}
