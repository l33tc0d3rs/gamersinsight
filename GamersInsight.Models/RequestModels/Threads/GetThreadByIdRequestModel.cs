﻿using System;

namespace GamersInsight.DTOs.RequestModels.Threads
{
    public class GetThreadByIdRequestModel
    {
        public Guid Id { get; set; }
    }
}
