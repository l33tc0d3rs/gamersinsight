﻿using System;

namespace GamersInsight.DTOs.RequestModels.Threads
{
    public class UpdateThreadRequestModel
    {
        public Guid Id { get; set; }
        public string ThreadName { get; set; }

    }
}
