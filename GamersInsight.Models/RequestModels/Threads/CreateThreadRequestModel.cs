﻿namespace GamersInsight.DTOs.RequestModels.Threads
{
    public class CreateThreadRequestModel
    {
        public string ThreadName { get; set; }
        public long CategoryId { get; set; }
    }
}
