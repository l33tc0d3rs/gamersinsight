﻿namespace GamersInsight.DTOs.RequestModels.Threads
{
    public class GetPaginatedThreadsRequestModel : GetPaginatedRequestModel
    {
        public long CategoryId { get; set; }
    }
}
