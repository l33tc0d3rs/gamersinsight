﻿namespace GamersInsight.DTOs.RequestModels.Message
{
    public class ReadMessagesPaginatedRequestModel : GetPaginatedRequestModel
    {       
        public long OtherParticipantId { get; set; }
    }
}
