﻿namespace GamersInsight.DTOs.RequestModels.Message
{
    public class SendMessageExtendedRequestModel
    {
        public long SenderId { get; set; }
        public long ReceiverId { get; set; }
        public string Content { get; set; }
    }
}
