﻿namespace GamersInsight.DTOs.RequestModels.Message
{
    public class ReadMessageRequestModel
    {
        public long SenderId { get; set; }
        public long ReceiverId { get; set; }
    }
}
