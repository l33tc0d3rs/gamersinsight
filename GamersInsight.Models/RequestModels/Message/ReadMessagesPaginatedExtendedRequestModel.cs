﻿namespace GamersInsight.DTOs.RequestModels.Message
{
    public class ReadMessagesPaginatedExtendedRequestModel : GetPaginatedRequestModel
    {
        public long CurrentUserId { get; set; }
        public long OtherParticipantId { get; set; }
    }
}
