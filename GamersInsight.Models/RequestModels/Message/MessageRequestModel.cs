﻿namespace GamersInsight.DTOs.RequestModels.Message
{
    public class MessageRequestModel
    {
        public long ReceiverId { get; set; }
        public string MessageContent { get; set; }
    }
}
