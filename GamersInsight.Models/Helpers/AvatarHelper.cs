﻿using System.IO;

namespace GamersInsight.DTOs.Helpers
{
    public static class AvatarHelper
    {
        private const string IMAGE_FOLDER = "avatars\\";

        public static string GetFullAvarUrl(string contentRoot, string newImageName)
        {
            return Path.Combine(contentRoot, IMAGE_FOLDER) + newImageName;
        }

        public static string GetWebRootRelativeAvatarUrl(string newImageName)
        {
            return Path.Combine("\\", IMAGE_FOLDER, newImageName);
        }

        public static string GetWebRootRelativeFolderUrl()
        {
            return Path.Combine("\\", IMAGE_FOLDER);
        }
    }
}
