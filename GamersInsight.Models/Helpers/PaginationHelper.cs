﻿using GamersInsight.DTOs.ResponseModels;
using System;

namespace GamersInsight.DTOs.Helpers
{
    public static class PaginationHelper
    {
        public static PaginationHelperResultModel CalculatePages(int totalCount, int skip, int take)
        {
            if (skip < 0)
            {
                skip = 0;
            }
            var result = new PaginationHelperResultModel
            {
                CurrentPage = GetCurrentPage(totalCount, skip, take)
            };

            decimal pagesCalculation = totalCount / take;
            if (totalCount % take == 0 && totalCount != 0)
            {
                result.NumberOfPages = (int)(Math.Floor(pagesCalculation));
            }
            else
            {
                result.NumberOfPages = (int)(Math.Floor(pagesCalculation) + 1);
            }

            if (result.NumberOfPages == result.CurrentPage)
            {
                result.IsLastPage = true;
            }

            return result;
        }

        public static int FindLastPageSkip(int totalPages, int take)
        {
            return (totalPages - 1) * take;
        }

        public static int GetCurrentPage(int totalCount, int skip, int take)
        {
            int result;
            if (totalCount < take)
            {
                result = 1;
            }
            else
            {
                result = skip / take;
                if (totalCount - skip > 0)
                {
                    result++;
                }
            }

            return result;
        }
    }
}
