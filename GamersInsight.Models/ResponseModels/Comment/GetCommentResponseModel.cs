﻿using System;

namespace GamersInsight.DTOs.ResponseModels.Comment
{
    public class GetCommentResponseModel
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public string AuthorName { get; set; }
        public DateTime DateOfCreation { get; set; }
    }
}