﻿using System.Collections.Generic;

namespace GamersInsight.DTOs.ResponseModels.Comment
{
    public class CommentsResponseModel : GeneralResponseModel
    {
        public bool IsLastPage { get; set; }
        public int CurrentPage { get; set; }
        public IEnumerable<GetCommentResponseModel> Comments { get; set; }
    }
}