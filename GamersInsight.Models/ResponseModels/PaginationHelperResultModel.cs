﻿namespace GamersInsight.DTOs.ResponseModels
{ 
    public class PaginationHelperResultModel
    {
        public int CurrentPage { get; set; }
        public bool IsLastPage { get; set; }
        public int NumberOfPages { get; set; }
    }
}
