﻿namespace GamersInsight.DTOs.ResponseModels
{
    public class GeneralResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; } = "Something bad happened.";
    }
}
