﻿using GamersInsight.DTOs.ResponseModels;

namespace GamersInsight.Models.ResponseModels
{
    public class RegisterUserResponseModel : GeneralResponseModel
    {
        public string Username { get; set; }
        public string UserRoleName { get; set; }

    }
}
