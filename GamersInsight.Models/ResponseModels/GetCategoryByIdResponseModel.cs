﻿namespace GamersInsight.DTOs.ResponseModels
{
    public class GetCategoryByIdResponseModel : GeneralResponseModel
    {
        public long Id { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public bool NSFW { get; set; } // TODO: change to IsNsfw 
    }
}