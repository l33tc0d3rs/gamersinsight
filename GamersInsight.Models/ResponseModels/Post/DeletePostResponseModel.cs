﻿using System;

namespace GamersInsight.DTOs.ResponseModels.Post
{
    public class DeletePostResponseModel : GeneralResponseModel
    {
        public Guid Id { get; set; }
    }
}
