﻿using System.Collections.Generic;

namespace GamersInsight.DTOs.ResponseModels.Post
{
    public class GetPostsResponseModel : GeneralPaginatedResponseModel
    {
        public IEnumerable<GetPostResponseModel> Data { get; set; }
    }
}
