﻿namespace GamersInsight.DTOs.ResponseModels.Post
{
    public class TogglePostLikeResponseModel : GeneralResponseModel
    {
        public bool IsLikedByUser { get; set; }

        public long PostRating { get; set; }
    }
}
