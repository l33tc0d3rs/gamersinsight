﻿using System;

namespace GamersInsight.DTOs.ResponseModels.Post
{
    public class GetPostGridViewResponseModel
    {
        public Guid ThreadId { get; set; }
    }
}
