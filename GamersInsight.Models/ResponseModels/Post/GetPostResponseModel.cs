﻿using GamersInsight.DTOs.ResponseModels.Author;
using GamersInsight.DTOs.ResponseModels.Comment;
using System;

namespace GamersInsight.DTOs.ResponseModels.Post
{
    public class GetPostResponseModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public long LikesCount { get; set; }
        public bool IsLikedByUser { get; set; }
        public bool IsOwner { get; set; }
        public DateTime DateOfCreation { get; set; }
        public GetAuthorResponseModel Author { get; set; }
        public CommentsResponseModel CommentsResponse { get; set; }
    }
}