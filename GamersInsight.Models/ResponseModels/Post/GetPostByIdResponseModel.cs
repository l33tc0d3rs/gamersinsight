﻿using System;

namespace GamersInsight.DTOs.ResponseModels.Post
{
    public class GetPostByIdResponseModel : GeneralResponseModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        //public string CategoryName { get; set; }
        //public long LikesCount { get; set; }
        //public IEnumerable<GetCommentResponseModel> Data { get; set; }
    }
}