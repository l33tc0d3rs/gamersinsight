﻿using System;

namespace GamersInsight.DTOs.ResponseModels
{
    public class LastActiveThreadResponseModel
    {
        public Guid ThreadId { get; set; }
        public string ThreadTitle { get; set; }
        public DateTime LastActivityTime { get; set; }
    }
}