﻿using System;

namespace GamersInsight.DTOs.ResponseModels.Author
{
    public class GetAuthorResponseModel
    {
        public string Username { get; set; }
        public string AvatarUrl { get; set; }
        public string RoleName { get; set; }
        public DateTime DateOfCreation { get; set; }
    }
}
