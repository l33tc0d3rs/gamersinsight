﻿namespace GamersInsight.DTOs.ResponseModels
{
    public class GeneralPaginatedResponseModel : GeneralResponseModel
    {
        public int TotalCount { get; set; }
        public int CurrentPage { get; set; }
        public bool IsLastPage { get; set; }
    }
}
