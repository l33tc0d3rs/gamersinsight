﻿using System;

namespace GamersInsight.DTOs.ResponseModels.Message
{
    public class MessageResponseModel
    {
        public long SenderId { get; set; }
        public long ReceiverId { get; set; }
        public string Content { get; set; }
        public DateTime TimeOfCreation { get; set; }
    }
}
