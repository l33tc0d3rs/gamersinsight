﻿using System.Collections.Generic;

namespace GamersInsight.DTOs.ResponseModels.Message
{
    public class ReadMessagesResponseModel : GeneralPaginatedResponseModel
    {
        public IEnumerable<Data.MessageResponseModel> Data { get; set; }
    }
}
