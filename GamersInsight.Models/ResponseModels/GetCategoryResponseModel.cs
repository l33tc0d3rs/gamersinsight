﻿namespace GamersInsight.DTOs.ResponseModels
{
    public class GetCategoryResponseModel
    {
        public long Id { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public bool IsNsfw { get; set; }
        public int ThreadsCount { get; set; }
        public LastActiveThreadResponseModel LastActiveThread { get; set; }
    }
}