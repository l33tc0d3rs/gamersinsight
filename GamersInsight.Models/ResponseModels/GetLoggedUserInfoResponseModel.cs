﻿using System;

namespace GamersInsight.Models.ResponseModels
{
    public class GetLoggedUserInfoResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string RoleName { get; set; }
        public string DisplayName { get; set; }
        public bool IsNsfwEnabled { get; set; }
        public long Id { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}
