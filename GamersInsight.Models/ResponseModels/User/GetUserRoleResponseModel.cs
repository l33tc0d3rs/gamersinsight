﻿namespace GamersInsight.DTOs.ResponseModels.User
{
    public class GetUserRoleResponseModel : GeneralResponseModel
    {
        public string Role { get; set; }
    }
}
