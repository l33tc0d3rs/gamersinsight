﻿namespace GamersInsight.DTOs.ResponseModels.User
{
    public class GetUserShortInfoResponseModel : GeneralResponseModel
    {
        public string Email { get; set; }    
        public string DisplayName { get; set; }
    }
}
