﻿using System;

namespace GamersInsight.DTOs.ResponseModels.User
{
    public class UserManagementResponseModel
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public bool IsBlocked { get; set; }
        public DateTime RegistrationDate { get; set; }
    }
}
