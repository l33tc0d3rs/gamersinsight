﻿using System.Collections.Generic;
namespace GamersInsight.DTOs.ResponseModels.User
{
    public class GetPaginatedUserResponseModel : GeneralPaginatedResponseModel
    {
        public IEnumerable<GetFilteredUserResponseModel> Data { get; set; }
    }
}
