﻿namespace GamersInsight.DTOs.ResponseModels.User
{
    public class LocalAvatarUploadResponseModel
    {
        public string NewAvatarFullUrl { get; set; }

        public string NewAvatarFileName { get; set; }
    }
}
