﻿namespace GamersInsight.DTOs.ResponseModels.User
{
    public class GetBlockedUserResponseModel
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string RoleName { get; set; }
        public bool IsBlocked { get; set; }
    }
}
