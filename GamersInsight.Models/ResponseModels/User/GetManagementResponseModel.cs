﻿using System.Collections.Generic;

namespace GamersInsight.DTOs.ResponseModels.User
{
    public class GetManagementResponseModel : GeneralPaginatedResponseModel
    {     
        public IEnumerable<UserManagementResponseModel> Data { get; set; }
    }
}
