﻿namespace GamersInsight.DTOs.ResponseModels.User
{
    public class UpdateAvatarResponseModel : GeneralResponseModel
    {
        public string NewAvatarFullUrl { get; set; }
    }
}
