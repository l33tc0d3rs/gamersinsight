﻿namespace GamersInsight.DTOs.ResponseModels.User
{
    public class GetUserSettingsResponseModel : GeneralResponseModel
    {
        public string FullAvatarUrl { get; set; }
        public string UserName { get; set; }

        public string DisplayName { get; set; }
    }
}
