﻿namespace GamersInsight.DTOs.ResponseModels
{
    public class DeleteCategoryResponseModel : GeneralResponseModel
    {
        public long Id { get; set; }
    }
}
