﻿using System;

namespace GamersInsight.Models.ResponseModels
{
    public class GetUserInfoResponseModel
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool IsNsfwEnabled { get; set; }
    }
}
