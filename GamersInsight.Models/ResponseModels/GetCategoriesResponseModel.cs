﻿using System.Collections.Generic;

namespace GamersInsight.DTOs.ResponseModels
{
    public class GetCategoriesResponseModel : GeneralPaginatedResponseModel
    {
        public IEnumerable<GetCategoryResponseModel> Data { get; set; }
    }
}
