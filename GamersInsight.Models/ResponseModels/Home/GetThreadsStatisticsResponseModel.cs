﻿using GamersInsight.DTOs.ResponseModels.Threads;
using System.Collections.Generic;

namespace GamersInsight.DTOs.ResponseModels.Home
{
    public class GetThreadsStatisticsResponseModel : GeneralResponseModel
    {
        public int TotalUsers { get; set; }
        public int TotalThreads { get; set; }
        public int TotalPosts { get; set; }
        public IEnumerable<LatestThreadResponseModel> Data { get; set; }
    }
}
