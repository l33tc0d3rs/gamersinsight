﻿using System.Collections.Generic;

namespace GamersInsight.DTOs.ResponseModels.Conversation
{
    public class PaginatedConversationResponseModel : GeneralPaginatedResponseModel
    {
        public IEnumerable<GetConversationResponseModel> Data { get; set; }
    }
}
