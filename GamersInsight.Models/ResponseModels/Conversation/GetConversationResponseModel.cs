﻿using System;

namespace GamersInsight.DTOs.ResponseModels.Conversation
{
    public class GetConversationResponseModel
    {
        public long OtherParticipantId { get; set; }
        public string OhterParticipantDisplayName { get; set; }
        public DateTime TimeOfCreation { get; set; }
    }
}
