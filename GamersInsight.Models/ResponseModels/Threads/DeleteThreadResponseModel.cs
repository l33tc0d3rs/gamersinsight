﻿namespace GamersInsight.DTOs.ResponseModels.Threads
{
    public class DeleteThreadResponseModel : GeneralResponseModel
    {
        public long Id { get; set; }
    }
}
