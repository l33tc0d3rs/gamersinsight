﻿using System.Collections.Generic;

namespace GamersInsight.DTOs.ResponseModels.Threads
{
    public class GetThreadsResponseModel : GeneralPaginatedResponseModel
    {
        public IEnumerable<GetThreadResponseModel> Data { get; set; }
    }
}
