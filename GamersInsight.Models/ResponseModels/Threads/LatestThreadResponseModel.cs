﻿using System;

namespace GamersInsight.DTOs.ResponseModels.Threads
{
    public class LatestThreadResponseModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public LastActivePostResponseModel LastActivePost { get; set; }
    }
}
