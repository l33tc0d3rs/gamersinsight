﻿using System;

namespace GamersInsight.DTOs.ResponseModels.Threads
{
    public class GetThreadByIdResponseModel : GeneralResponseModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    
    }
}