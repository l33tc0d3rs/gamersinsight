﻿using System;

namespace GamersInsight.DTOs.ResponseModels.Threads
{
    public class LastActivePostResponseModel
    {
        public Guid PostId { get; set; }
        public string Title { get; set; }
        public DateTime LastActivityTime { get; set; }
    }
}