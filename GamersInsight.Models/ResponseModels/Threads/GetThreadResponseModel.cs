﻿using System;

namespace GamersInsight.DTOs.ResponseModels.Threads
{
    public class GetThreadResponseModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public int PostsCount { get; set; }
        public DateTime DateOfCreation { get; set; }
        public LastActivePostResponseModel LastActivePost { get; set; }
    }
}