# [GamersInsight Forum - ASP.NET Core Web App ](https://gamersinsightweb.azurewebsites.net/)

  
  

[![Foo](https://i.imgur.com/vVmzOIM.png)](https://gamersinsightweb.azurewebsites.net/)

  
[![Foo](https://i.imgur.com/85e7fax.png)](https://gamersinsightweb.azurewebsites.net/)
  

[![Foo](https://i.imgur.com/jLZBVpV.png)](https://gamersinsightweb.azurewebsites.net/)

  

## Made by: <br />

  

Team Members: <br  />

  

&nbsp;  &bull; Krasen Velkov - GitLab Profile: @KrasenValkov<br  />

  

&nbsp;  &bull; Anton Staykov - GitLab Profile: @AntonStaykov<br  />

  
  

## Azure Board:

  

[AzureBoard: GamersInsight Project](https://dev.azure.com/L33tC0d3rs/gamersinsight/_boards/board/t/gamersinsight%20Team/Issues) <br  />

  

## Technologies used: <br />

  

<b>

  

&bull; .NET Core 3.1<br  />

  

&bull; EntityFrameworkCore 3.1 <br  />

  

&bull; SQL Server Developer Edition<br  />

  

&bull; VisualStudio 2019 Community<br  />

  

&bull; MS Test 1.3.2 <br  />

  

&bull; Moq 4.10.1 <br  />

  

&bull; JavaScript / jQuery <br  />

  

&bull; AG-Grid <br  />

  

&bull; HTML/CSS <br  />

  

&bull; Razor engine <br  />

  

&bull; Git/GitLab

  

</b>  <br  />  <br  />

  

## Project Overview:

  

Presentation Layer: consists of ASP.NET Core 3,1 Web Project with fully responsive beautiful design

Fully AJAX built using AG-Grid, JSON API calls from Back-End and dynamically updating DOM.<br  />

  

Business Layer: Services and business features processing class libraries. All queries project optimal Response Models and are checked for best possible query translations from ORM to the DataBase provided in order to ensure maximal performance.<br  />

  

Data Layer: Holding the configuration for the data-base and additional mappings of table-relationships.<br  />

  

App features RESTful Client-Side Cookie Authentication.<br  />

  

Thorough unit testing for the services and business features covering more than %.<br  />

  

Server side Pagination WITH java-script based AG-Grid manipulation for amazing customer experience.<br  />

  

The Website allows the client to login and manage all of his resources.<br  />

  

Most of the requests are Ajax based to enable beautiful handling of errors and model-state for various forms and situations.<br  />