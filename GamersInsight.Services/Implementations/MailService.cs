﻿using GamersInsight.DTOs.Helpers;
using GamersInsight.DTOs.RequestModels;
using GamersInsight.Services.Interfaces;
using GamersInsight.Services.Utilities;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Text;
using System.Threading.Tasks;

namespace GamersInsight.Services.Implementations
{
    public class MailService : IMailService
    {
        private readonly MailSettings _mailSettings;

        public MailService(IOptions<MailSettings> mailSettings)
        {
            _mailSettings = mailSettings.Value;
        }

        public async Task SendEmailAsync(MailRequestModel mailRequest, string BaseUrl)
        {
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
            email.To.Add(MailboxAddress.Parse(mailRequest.ToEmail));
            email.Subject = mailRequest.Subject;
            var builder = new BodyBuilder();

            string token = Convert.ToBase64String(Encoding.Unicode.GetBytes(mailRequest.ToEmail));

            builder.HtmlBody = $"Please confirm your email trough this link: {BaseUrl}{Constants.DOMAIN_CONSTANT_HASH}{token}";
            email.Body = builder.ToMessageBody();

            using var smtp = new MailKit.Net.Smtp.SmtpClient();
            smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
            smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
            await smtp.SendAsync(email);
            smtp.Disconnect(true);
        }

    }
}
