﻿using GamersInsight.Data;
using GamersInsight.DTOs.Helpers;
using GamersInsight.DTOs.RequestModels;
using GamersInsight.DTOs.RequestModels.User;
using GamersInsight.DTOs.ResponseModels.User;
using GamersInsight.Models.RequestModels;
using GamersInsight.Models.ResponseModels;
using GamersInsight.Services.Interfaces;
using GamersInsight.Services.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GamersInsight.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly GamersInsightContext _context;
        private readonly IWebHostEnvironment _env;
        private readonly IMailService _mailService;

        public UserService(GamersInsightContext context, IWebHostEnvironment env, IMailService mailService)
        {
            _context = context;
            _env = env;
            _mailService = mailService;
        }

        public async Task<UpdateUserResponseModel> UpdateUserAsync(UpdateUserExtendedRequestModel requestModel)
        {
            var result = new UpdateUserResponseModel();

            if (requestModel.NewPassword != requestModel.RepeatPassword)
            {
                result.Message = Constants.PASSWORD_NO_MATCH;
                return result;
            }

            var user = await _context.Users.Where(p => p.IsActivated && !p.IsBlocked && p.Id == requestModel.CurrentUserId).SingleOrDefaultAsync();
            if (user == null)
            {
                result.Message = Constants.NO_SUCH_USER;
                return result;
            }

            var passwordMatch = BCrypt.Net.BCrypt.Verify(requestModel.CurrentPassword, user.Password);
            if (!passwordMatch)
            {
                result.Message = Constants.PASSWORD_IS_INCORRECT;
                return result;
            }

            user.DisplayName = requestModel.DisplayName;
            user.Email = requestModel.Email;
            user.Password = BCrypt.Net.BCrypt.HashPassword(requestModel.NewPassword);

            result.IsSuccess = true;
            result.Message = "User updated successfully!";

            await _context.SaveChangesAsync();

            return result;
        }


        public async Task<UpdateAvatarResponseModel> UpdateAvatarAsync(UpdateAvatarExtendedRequestModel requestModel)
        {
            var result = new UpdateAvatarResponseModel();

            var user = await _context.Users.Where(p => p.IsActivated && !p.IsBlocked && p.Id == requestModel.CurrentUserId).SingleOrDefaultAsync();
            if (user == null)
            {
                result.Message = Constants.NO_SUCH_USER;
                return result;
            }

            var localImageUploadResponse = await SaveImageLocallyAsync(requestModel.Image);
            user.Avatar = localImageUploadResponse.NewAvatarFileName;

            var contentRoot = _env.WebRootPath;
            result.NewAvatarFullUrl = localImageUploadResponse.NewAvatarFullUrl;

            await _context.SaveChangesAsync();

            result.IsSuccess = true;
            result.Message = "User avatar successfully!";

            return result;
        }

        public async Task<string> ConfirmMail(string token)
        {
            var tokenByte = Convert.FromBase64String(token);
            var tokenToEmail = System.Text.Encoding.Unicode.GetString(tokenByte);
            var user = await _context.Users
                                     .FirstOrDefaultAsync(x => x.Email == tokenToEmail);

            var NotVerifiedRoleId = await _context.Roles
                                                  .Where(p => p.RoleName == UserRoleConstants.NOTVERIFIED)
                                                  .Select(p => p.Id)
                                                  .SingleOrDefaultAsync();
            var UserRoleId = await _context.Roles
                                           .Where(p => p.RoleName == UserRoleConstants.USER)
                                           .Select(p => p.Id)
                                           .SingleOrDefaultAsync();

            if (user != null && user.RoleId == NotVerifiedRoleId)
            {
                user.IsActivated = true;
                user.RoleId = UserRoleId;
                await _context.SaveChangesAsync();
                return user.Email;
            }

            return null;
        }

        public async Task<GetUserSettingsResponseModel> GetUserSettingsAsync(GetUserSettingsRequestModel requestModel)
        {
            var result = new GetUserSettingsResponseModel();

            result = await _context.Users.Where(p => p.IsActivated && !p.IsBlocked && p.Id == requestModel.CurrentUserId)
                                .Select(p => new GetUserSettingsResponseModel
                                {
                                    DisplayName = p.DisplayName,
                                    UserName = p.UserName,
                                    FullAvatarUrl = AvatarHelper.GetWebRootRelativeAvatarUrl(p.Avatar)
                                })
                                .SingleOrDefaultAsync();

            if (result == null)
            {
                var error = new GetUserSettingsResponseModel();
                error.Message = Constants.NO_SUCH_USER;
                return error;
            }

            result.IsSuccess = true;
            result.Message = Constants.USER_SETTING_RETRIVED;

            return result;
        }

        public async Task<GetUserShortInfoResponseModel> GetUserShortInfoAsync(GetUserSettingsRequestModel requestModel)
        {
            var result = new GetUserShortInfoResponseModel();

            result = await _context.Users.Where(p => p.IsActivated && !p.IsBlocked && p.Id == requestModel.CurrentUserId)
                                .Select(p => new GetUserShortInfoResponseModel
                                {
                                    Email = p.Email,
                                    DisplayName = p.DisplayName
                                })
                                .SingleOrDefaultAsync();

            if (result == null)
            {
                var error = new GetUserShortInfoResponseModel();
                error.Message = Constants.NO_SUCH_USER;
                return error;
            }

            result.IsSuccess = true;
            result.Message = Constants.USER_SETTING_RETRIVED;

            return result;
        }

        public async Task<GetManagementResponseModel> GetUsersAsync(GetManagementRequestModel requestModel)
        {
            var usersQueryable = _context.Users
                                         .Where(p => p.IsActivated)
                                         .Select(p => new UserManagementResponseModel
                                         {
                                             Id = p.Id,
                                             Email = p.Email,
                                             UserName = p.UserName,
                                             IsBlocked = p.IsBlocked,
                                             RegistrationDate = p.DateOfCreation
                                         });

            var result = new GetManagementResponseModel
            {
                TotalCount = await usersQueryable.CountAsync(),
                IsSuccess = false,
                Message = Constants.POST_NO_DATA
            };

            if (result.TotalCount - requestModel.Skip > 0)
            {
                var paginationResult = PaginationHelper.CalculatePages(result.TotalCount, requestModel.Skip, requestModel.Take);
                result.CurrentPage = paginationResult.CurrentPage;
                result.IsLastPage = paginationResult.IsLastPage;

                var skip = requestModel.Skip;
                if (requestModel.ShouldGetLastPage)
                {
                    skip = PaginationHelper.FindLastPageSkip(paginationResult.NumberOfPages, requestModel.Take);
                    result.CurrentPage = paginationResult.NumberOfPages;
                }

                result.Data = await usersQueryable
                                   .Skip(skip)
                                   .Take(requestModel.Take)
                                   .ToListAsync();

                result.IsSuccess = true;
            }

            return result;
        }
        [Authorize(Roles = UserRoleConstants.ADMIN)]
        public async Task<BlockUserResponseModel> BlockUserAsync(BlockUserRequestModel requestModel)
        {
            var result = new BlockUserResponseModel();

            var userToManage = await _context.Users.Where(p => p.IsActivated && p.Id == requestModel.ManagedUserId).SingleOrDefaultAsync();
            if (userToManage == null)
            {
                result.Message = Constants.NO_SUCH_USER;
            }
            else
            {
                userToManage.IsBlocked = true;
                await _context.SaveChangesAsync();
                result.IsSuccess = true;
                result.Message = Constants.USER_BLOCKED;
            }

            return result;
        }
        [Authorize(Roles = UserRoleConstants.ADMIN)]
        public async Task<UnblockUserResponseModel> UnblockUserAsync(UnblockUserRequestModel requestModel)
        {
            var result = new UnblockUserResponseModel();

            var userToManage = await _context.Users.Where(p => p.IsActivated && p.Id == requestModel.ManagedUserId).SingleOrDefaultAsync();
            if (userToManage == null)
            {
                result.Message = Constants.NO_SUCH_USER;
            }
            else
            {
                userToManage.IsBlocked = false;

                await _context.SaveChangesAsync();

                result.IsSuccess = true;
                result.Message = Constants.USER_UNBLOCKED;
            }

            return result;
        }


        public async Task<RegisterUserResponseModel> RegisterUser(RegisterUserRequestExtendedModel requestModel)
        {
            var result = new RegisterUserResponseModel();
            // validations for Username, DisplayName, Email and Password.

            string emailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|" + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)" + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";
            var regex = new Regex(emailPattern, RegexOptions.IgnoreCase);
            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasMinimum8Chars = new Regex(@".{8,}");

            var isPasswordValid = hasNumber.IsMatch(requestModel.Password) && hasUpperChar.IsMatch(requestModel.Password) && hasLowerChar.IsMatch(requestModel.Password) && hasMinimum8Chars.IsMatch(requestModel.Password);


            if (requestModel.Username.Length < 2 || requestModel.Username.Length > 20)
            {
                result.Message = Constants.USER_USERNAME_ERROR;
                return result;
            }

            if (requestModel.DisplayName.Length < 2 || requestModel.DisplayName.Length > 20)
            {
                result.Message = Constants.USER_DISPLAYNAME_ERROR;
                return result;
            }

            if (!regex.IsMatch(requestModel.Email))
            {
                result.Message = Constants.USER_EMAIL_ERROR;
                return result;
            }

            if (!isPasswordValid || requestModel.Password.Length < 8)
            {
                result.Message = Constants.USER_PASSWORD;
                return result;
            }


            var userExists = await _context.Users.AnyAsync(p => p.UserName == requestModel.Username);
            var emailExists = await _context.Users.AnyAsync(p => p.Email == requestModel.Email);

            string hashedPassword = BCrypt.Net.BCrypt.HashPassword(requestModel.Password);

            if (userExists)
            {
                result.IsSuccess = false;
                result.Message = Constants.USER_ALREADY_EXISTS;
                return result;
            }
            if (emailExists)
            {
                result.IsSuccess = false;
                result.Message = Constants.EMAIL_ALREADY_EXISTS;//TODO
                return result;
            }

            var userRoleId = await _context.Roles
                                               .Where(p => p.RoleName == UserRoleConstants.NOTVERIFIED)
                                               .Select(p => p.Id)
                                               .SingleOrDefaultAsync();
            var user = new User
            {
                UserName = requestModel.Username,
                Password = hashedPassword,
                DisplayName = requestModel.DisplayName,
                Email = requestModel.Email,
                DateOfBirth = requestModel.DateOfBirth,
                DateOfCreation = System.DateTime.Now,
                Avatar = "", //TODO Default avatar
                RoleId = userRoleId
            };

            var mail = new MailRequestModel()   
            {
                ToEmail = requestModel.Email,
                Subject = "Gamers Insight Verification",
            };

            await _mailService.SendEmailAsync(mail, requestModel.BaseUrl);


            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();

            result.IsSuccess = true;
            result.Message = Constants.USER_REGISTERED;

            return result;
        }       

        public async Task<GetLoggedUserInfoResponseModel> GetLoggedUserInfoAsync(GetLoggedUserInfoRequestModel requestModel)
        {
            var result = new GetLoggedUserInfoResponseModel();

            var userInfo = await (from u in _context.Users
                                  join r in _context.Roles on u.RoleId equals r.Id
                                  where u.UserName.ToLower() == requestModel.Username.ToLower()
                                  select new GetUserInfoResponseModel
                                  {
                                      Username = u.UserName,
                                      DisplayName = u.DisplayName,
                                      Password = u.Password,
                                      Role = r.RoleName,
                                      Id = u.Id,
                                      DateOfBirth = u.DateOfBirth,
                                  }).SingleOrDefaultAsync();

            if (userInfo != null)
            {
                bool verified = BCrypt.Net.BCrypt.Verify(requestModel.Password, userInfo.Password);

                if (!verified)
                {
                    result.IsSuccess = false;
                    result.Message = Constants.PASSWORD_IS_INCORRECT;
                }
                else
                {
                    result.IsSuccess = true;
                    result.RoleName = userInfo.Role;
                    result.Id = userInfo.Id;
                    result.Message = Constants.USER_LOGGED;
                    result.DisplayName = userInfo.DisplayName;

                    var today = DateTime.Today;
                    TimeSpan ageSpan = today - userInfo.DateOfBirth;
                    double age = ageSpan.TotalDays / 365.25;


                    if (age > 18)
                    {
                        result.IsNsfwEnabled = true;
                    }
                }
            }
            else
            {
                result.IsSuccess = false;
                result.Message = Constants.NO_SUCH_USER;
            }

            return result;
        }

        public async Task<GetPaginatedUserResponseModel> GetUserByFilter(GetFilteredUsersPaginatedRequestModel requestModel)
        {
            var usersAsQueriable = (from u in _context.Users
                                    join r in _context.Roles on u.RoleId equals r.Id
                                    select new GetFilteredUserResponseModel
                                    {
                                        DisplayName = u.DisplayName,
                                        UserName = u.UserName,
                                        Id = u.Id,
                                        Email = u.Email,
                                        RoleName = r.RoleName,
                                        IsBlocked = u.IsBlocked
                                    })
                                    .Where(data => data.UserName
                                    .Contains(requestModel.FilterInput) ||
                                    data.DisplayName
                                    .Contains(requestModel.FilterInput) ||
                                    data.Email
                                    .Contains(requestModel.FilterInput));

            var result = new GetPaginatedUserResponseModel()
            {
                TotalCount = await usersAsQueriable.CountAsync(),
                Message = Constants.NO_SUCH_USER
            };

            if (result.TotalCount > 0)
            {
                var paginationResult = PaginationHelper.CalculatePages(result.TotalCount, requestModel.Skip, requestModel.Take);
                result.CurrentPage = paginationResult.CurrentPage;
                result.IsLastPage = paginationResult.IsLastPage;

                var skip = requestModel.Skip;
                if (requestModel.ShouldGetLastPage)
                {
                    skip = PaginationHelper.FindLastPageSkip(paginationResult.NumberOfPages, requestModel.Take);
                    result.CurrentPage = paginationResult.NumberOfPages;
                }

                result.Data = await usersAsQueriable
                                 .Skip(skip)
                                 .Take(requestModel.Take)
                                 .ToListAsync();

                result.IsSuccess = true;
            }

            return result;
        }

        private async Task<LocalAvatarUploadResponseModel> SaveImageLocallyAsync(IFormFile file)
        {
            var contentRoot = _env.WebRootPath;
            var newImageName = Guid.NewGuid() + ".jpg";

            var inputFileName = AvatarHelper.GetFullAvarUrl(contentRoot, newImageName);
            await file.CopyToAsync(new FileStream(inputFileName, FileMode.Create));

            var result = new LocalAvatarUploadResponseModel
            {
                NewAvatarFullUrl = AvatarHelper.GetWebRootRelativeAvatarUrl(newImageName),
                NewAvatarFileName = newImageName
            };

            return result;
        }
    }
}
