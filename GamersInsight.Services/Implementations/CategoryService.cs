﻿using GamersInsight.Data;
using GamersInsight.DTOs.RequestModels;
using GamersInsight.DTOs.Helpers;
using GamersInsight.DTOs.ResponseModels;
using GamersInsight.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using GamersInsight.Services.Utilities;
using Microsoft.AspNetCore.Authorization;

namespace GamersInsight.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly GamersInsightContext _context;

        public CategoryService(GamersInsightContext context)
        {
            _context = context;
        }

        [Authorize(Roles = UserRoleConstants.ADMIN)]
        public async Task<CreateCategoryResponseModel> CreateAsync(CreateCategoryRequestModel requestModel)
        {
            var result = new CreateCategoryResponseModel();

            if (string.IsNullOrEmpty(requestModel.CategoryName) || string.IsNullOrEmpty(requestModel.Description))
            {
                result.IsSuccess = false;
                result.Message = Constants.CATEGORY_NAME_DESCRIPTION_EMPTY;

                return result;
            }

            var IsCategoryExistent = await _context.Categories.AnyAsync(p => p.CategoryName == requestModel.CategoryName);

            if (IsCategoryExistent)
            {
                result.IsSuccess = false;
                result.Message = Constants.CATEGORY_EXIST;
                return result;
            }

            var category = new Category
            {
                CategoryName = requestModel.CategoryName,
                Description = requestModel.Description,
                NSFW = requestModel.NSFW
            };

            _context.Categories.Add(category);

            await _context.SaveChangesAsync();

            result.IsSuccess = true;
            result.Message = $"{Constants.CATEGORY_CREATED}";

            return result;
        }

        public async Task<GetCategoriesResponseModel> GetCategoriesAsync(GetExtendedCategoriesModel requestModel)
        {

            var categoriesQueryable = _context.Categories.AsQueryable();

            if (requestModel.IsNsfw == false)
            {
                categoriesQueryable.Where(p => p.NSFW == requestModel.IsNsfw);
            }
            var result = new GetCategoriesResponseModel
            {
                TotalCount = await categoriesQueryable.CountAsync(),
                Message = Constants.CATEGORY_NO_DATA
            };

            if (result.TotalCount - requestModel.Skip > 0)
            {
                var paginationResult = PaginationHelper.CalculatePages(result.TotalCount, requestModel.Skip, requestModel.Take);
                result.CurrentPage = paginationResult.CurrentPage;
                result.IsLastPage = paginationResult.IsLastPage;

                var skip = requestModel.Skip;
                if (requestModel.ShouldGetLastPage)
                {
                    skip = PaginationHelper.FindLastPageSkip(paginationResult.NumberOfPages, requestModel.Take);
                    result.CurrentPage = paginationResult.NumberOfPages;
                }

                result.Data = await categoriesQueryable
                                          .Skip(skip)
                                          .Take(requestModel.Take)
                                          .Select(p => new GetCategoryResponseModel
                                          {
                                              Id = p.Id,
                                              CategoryName = p.CategoryName,
                                              Description = p.Description,
                                              IsNsfw = p.NSFW,
                                              ThreadsCount = _context.Threads.Where(t => t.CategoryId == p.Id).Count(),
                                              LastActiveThread = (from p2 in _context.Posts
                                                                  join t2 in _context.Threads on p2.ThreadId equals t2.Id
                                                                  join c in _context.Categories on t2.CategoryId equals c.Id
                                                                  orderby p2.DateOfCreation descending
                                                                  where c.Id == p.Id
                                                                  select new LastActiveThreadResponseModel
                                                                  {
                                                                      ThreadId = t2.Id,
                                                                      ThreadTitle = t2.ThreadName,
                                                                      LastActivityTime = p2.DateOfCreation

                                                                  }).FirstOrDefault()
                                          })
                                          .ToListAsync();
                result.IsSuccess = true;
                result.Message = $"{Constants.CATEGORY_TOTAL_COUNT}" + result.TotalCount;
            }

            return result;
        }

        [Authorize(Roles = UserRoleConstants.ADMIN)]
        public async Task<DeleteCategoryResponseModel> DeleteCategoryAsync(DeleteCategoryRequestModel requestModel)
        {
            var result = new DeleteCategoryResponseModel();

            var category = await _context.Categories.FirstOrDefaultAsync(p => p.Id == requestModel.Id);

            if (category != null)
            {
                _context.Categories.Remove(category);
                await _context.SaveChangesAsync();
                result.IsSuccess = true;
                result.Message = $"{Constants.CATEGORY_DELETED}";
            }
            else
            {
                result.IsSuccess = false;
                result.Message = $"{Constants.CATEGORY_NOT_FOUND}";
            }

            return result;
        }

        public async Task<UpdateCategoryResponseModel> UpdateCateogryAsync(UpdateCategoryRequestModel requestModel)
        {
            var result = new UpdateCategoryResponseModel();


            var category = await _context.Categories.FirstOrDefaultAsync(p => p.Id == requestModel.Id);

            if (category != null)
            {
                if (!string.IsNullOrEmpty(requestModel.CategoryName) && requestModel.CategoryName != category.CategoryName)
                {
                    category.CategoryName = requestModel.CategoryName;
                }
                if (!string.IsNullOrEmpty(requestModel.Description) && requestModel.Description != category.Description)
                {
                    category.Description = requestModel.Description;
                }
                if (requestModel.IsNsfw != category.NSFW)
                {
                    category.NSFW = requestModel.IsNsfw;
                }
                await _context.SaveChangesAsync();
                result.IsSuccess = true;
                result.Message = $"{Constants.CATEGORY_UPDATED}";
            }
            else
            {
                result.IsSuccess = false;
                result.Message = $"{Constants.CATEGORY_NOT_FOUND}";
            }

            return result;
        }

        public async Task<GetCategoryByIdResponseModel> GetCategoryByIdAsync(GetCategoryByIdRequestModel requestModel)
        {
            var result = new GetCategoryByIdResponseModel();
            var categoryResult = await _context.Categories
                                         .Where(p => p.Id == requestModel.Id)
                                         .Select(p => new GetCategoryByIdResponseModel
                                         {
                                             Id = p.Id,
                                             CategoryName = p.CategoryName,
                                             Description = p.Description,
                                             NSFW = p.NSFW
                                         }).FirstOrDefaultAsync();

            if (categoryResult != null)
            {
                categoryResult.IsSuccess = true;
                return categoryResult;
            }
            else
            {
                result.IsSuccess = false;
                result.Message = $"{Constants.CATEGORY_NOT_FOUND}";
                return result;
            }
        }
    }
}
