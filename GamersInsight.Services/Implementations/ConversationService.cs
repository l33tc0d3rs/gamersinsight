﻿using GamersInsight.Data;
using GamersInsight.DTOs.Helpers;
using GamersInsight.DTOs.RequestModels.Conversation;
using GamersInsight.DTOs.ResponseModels.Conversation;
using GamersInsight.Services.Interfaces;
using GamersInsight.Services.Utilities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace GamersInsight.Services
{
    public class ConversationService : IConversationService
    {
        private readonly GamersInsightContext _context;

        public ConversationService(GamersInsightContext context)
        {
            _context = context;
        }       

        public async Task<PaginatedConversationResponseModel> ConversationsAsync(GetConversationsExtendedRequestModel requestModel)
        {
            var messagesAsQueriable = (from m in _context.Messages
                                       join s in _context.Users on m.SenderId equals s.Id
                                       join r in _context.Users on m.ReceiverId equals r.Id
                                       where m.ReceiverId == requestModel.CurrentUserId ||
                                             m.SenderId == requestModel.CurrentUserId
                                       orderby m.TimeOfCreation descending
                                       select new GetConversationResponseModel
                                       {
                                           OtherParticipantId = m.SenderId == requestModel.CurrentUserId ? m.ReceiverId : m.SenderId,
                                           OhterParticipantDisplayName = m.SenderId == requestModel.CurrentUserId ? r.DisplayName : s.DisplayName
                                       });

            var result = new PaginatedConversationResponseModel
            {
                TotalCount = await messagesAsQueriable.Distinct().CountAsync(),
                IsSuccess = false,
                Message = Constants.POST_NO_DATA
            };

            if (result.TotalCount > 0)
            {
                var paginationResult = PaginationHelper.CalculatePages(result.TotalCount, requestModel.Skip, requestModel.Take);
                result.CurrentPage = paginationResult.CurrentPage;
                result.IsLastPage = paginationResult.IsLastPage;

                var skip = requestModel.Skip;
                if (requestModel.ShouldGetLastPage)
                {
                    skip = PaginationHelper.FindLastPageSkip(paginationResult.NumberOfPages, requestModel.Take);
                    result.CurrentPage = paginationResult.NumberOfPages;
                }

                result.Data = await messagesAsQueriable

                               .Skip(skip)
                               .Take(requestModel.Take)
                               .Distinct()
                               .ToListAsync();

                result.IsSuccess = true;
            }
            return result;
        }
    }
}
