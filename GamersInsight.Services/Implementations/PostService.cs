﻿using GamersInsight.Services.Utilities;
using GamersInsight.Data;
using GamersInsight.DTOs.Helpers;
using GamersInsight.DTOs.RequestModels.Post;
using GamersInsight.DTOs.ResponseModels.Author;
using GamersInsight.DTOs.ResponseModels.Comment;
using GamersInsight.DTOs.ResponseModels.Post;
using GamersInsight.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GamersInsight.Services
{
    public class PostService : IPostService
    {
        private readonly GamersInsightContext _context;

        public PostService(GamersInsightContext context)
        {
            _context = context;
        }

        public async Task<CreatePostResponseModel> CreateAsync(CreatePostExtendedRequestModel requestModel)
        {
            var result = new CreatePostResponseModel();

            if (string.IsNullOrEmpty(requestModel.Content))
            {
                result.IsSuccess = false;
                result.Message = Constants.POST_NO_CONTENT;

                return result;
            }

            var isThreadExistent = await _context.Threads.AnyAsync(p => p.Id == requestModel.ThreadId);

            if (!isThreadExistent)
            {
                result.Message = Constants.THREAD_NOT_FOUND;
                return result;
            }

            var isUserExistent = await _context.Users.AnyAsync(p => p.Id == requestModel.UserId);

            if (!isUserExistent)
            {
                result.Message = Constants.NO_SUCH_USER;
                return result;
            }

            var post = new Post
            {
                Title = requestModel.Title,
                Content = requestModel.Content,
                DateOfCreation = DateTime.Now,
                CreatorId = requestModel.UserId,
                ThreadId = requestModel.ThreadId
            };

            _context.Posts.Add(post);

            await _context.SaveChangesAsync();

            result.IsSuccess = true;
            result.Message = Constants.POST_CREATED;

            return result;
        }

        public async Task<GetPostsResponseModel> GetPostsAsync(GetPagintedPostsRequestExtendedModel requestModel)
        {
            var postsQueryable = _context.Posts.Where(p => p.ThreadId == requestModel.ThreadId).AsQueryable();

            if (!string.IsNullOrWhiteSpace(requestModel.ContentSearchTerm))
            {
                postsQueryable = postsQueryable.Where(p => p.Content.Contains(requestModel.ContentSearchTerm));
            }

            var result = new GetPostsResponseModel
            {
                TotalCount = await postsQueryable.CountAsync(),
                IsSuccess = true,
                Message = Constants.POST_NO_DATA
            };
            var isThreadExistent = await _context.Threads.AnyAsync(p => p.Id == requestModel.ThreadId);

            if (!isThreadExistent)
            {
                result.IsSuccess = false;
                result.Message = Constants.THREAD_NOT_FOUND;
                return result;
            }

            if (result.TotalCount - requestModel.Skip > 0)
            {
                var paginationResult = PaginationHelper.CalculatePages(result.TotalCount, requestModel.Skip, requestModel.Take);
                result.CurrentPage = paginationResult.CurrentPage;
                result.IsLastPage = paginationResult.IsLastPage;

                var skip = requestModel.Skip;
                if (skip < 0)
                {
                    skip = 0;
                }
                if (requestModel.ShouldGetLastPage)
                {
                    skip = PaginationHelper.FindLastPageSkip(paginationResult.NumberOfPages, requestModel.Take);
                    result.CurrentPage = paginationResult.NumberOfPages;
                }

                if (requestModel.OrderByDateAscending.HasValue && !requestModel.OrderByDateAscending.Value)
                {
                    postsQueryable = postsQueryable.OrderByDescending(p => p.DateOfCreation);
                }
                else
                {
                    postsQueryable = postsQueryable.OrderBy(p => p.DateOfCreation);
                }

                var avatarUrlBase = AvatarHelper.GetWebRootRelativeFolderUrl();

                result.Data = await postsQueryable
                               .Skip(skip)
                               .Take(requestModel.Take)
                               .Select(p => new GetPostResponseModel
                               {
                                   Id = p.Id,
                                   Title = p.Title,
                                   Content = p.Content,
                                   DateOfCreation = p.DateOfCreation,
                                   LikesCount = p.Rating,
                                   IsLikedByUser = _context.UserPostLikes.Where(l => l.UserId == requestModel.UserId && l.PostId == p.Id).Any(),
                                   IsOwner = p.CreatorId == requestModel.UserId,
                                   Author = (from u in _context.Users
                                             join r in _context.Roles on u.RoleId equals r.Id
                                             where u.Id == p.CreatorId
                                             select new GetAuthorResponseModel
                                             {
                                                 Username = u.UserName,
                                                 RoleName = r.RoleName,
                                                 AvatarUrl = avatarUrlBase + u.Avatar,
                                                 DateOfCreation = u.DateOfCreation
                                             }).FirstOrDefault(),
                                   CommentsResponse = p != null ? new CommentsResponseModel
                                   {
                                       CurrentPage = PaginationHelper.GetCurrentPage(result.TotalCount, requestModel.Skip, requestModel.Take),
                                       IsLastPage = (from c in _context.Comments
                                                     join p2 in _context.Posts on c.PostId equals p2.Id
                                                     join a in _context.Users on c.CreatorId equals a.Id
                                                     where p2.Id == p.Id
                                                     select c).Count() <= requestModel.CommentsTakeAmount,
                                       Comments = (from c in _context.Comments
                                                   join p2 in _context.Posts on c.PostId equals p2.Id
                                                   join a in _context.Users on c.CreatorId equals a.Id
                                                   where p2.Id == p.Id
                                                   orderby c.DateOfCreation descending
                                                   select new GetCommentResponseModel
                                                   {
                                                       Id = c.Id,
                                                       Content = c.Content,
                                                       AuthorName = a.UserName,
                                                       DateOfCreation = c.DateOfCreation
                                                   })
                                                   .Take(requestModel.CommentsTakeAmount)
                                                   .ToList()
                                   } : null
                               })
                               .ToListAsync();


                result.Message = $"Total Post {result.TotalCount}";
            }
            else if (result.TotalCount > 0 && result.TotalCount - requestModel.Skip <= 0)
            {
                result.IsSuccess = false;
                result.Message = Constants.POST_NO_DATA;
            }


            return result;
        }

        public async Task<CommentsResponseModel> GetPostCommentsAsync(GetPostCommentsRequestModel requestModel)
        {
            var commentsQueryable = _context.Comments.Where(p => p.PostId == requestModel.PostId).AsQueryable();
            var totalCount = await commentsQueryable.CountAsync();

            var result = new CommentsResponseModel
            {
                IsSuccess = true,
                Message = Constants.POST_NO_DATA
            };

            if (totalCount - requestModel.Skip > 0)
            {
                var skip = requestModel.Skip;
                var paginationResult = PaginationHelper.CalculatePages(totalCount, skip, requestModel.Take);
                result.CurrentPage = paginationResult.CurrentPage;
                result.IsLastPage = paginationResult.IsLastPage;

                result.Comments = await (from c in commentsQueryable
                                         join u in _context.Users on c.CreatorId equals u.Id
                                         select new { User = u, Comment = c })
                                         .Skip(skip)
                                         .Take(requestModel.Take)
                                         .OrderByDescending(p => p.Comment.DateOfCreation)
                                         .Select(p => new GetCommentResponseModel
                                         {
                                             Id = p.Comment.Id,
                                             Content = p.Comment.Content,
                                             DateOfCreation = p.Comment.DateOfCreation,
                                             AuthorName = p.User.DisplayName
                                         })
                                         .ToListAsync();

                result.Message = $"Total Comments {totalCount}";
                result.IsSuccess = true;
            }
            else
            {
                result.IsSuccess = false;
                result.Message = Constants.POST_NO_DATA;
            }

            return result;
        }

        public async Task<DeletePostResponseModel> DeletePostAsync(DeletePostRequestModel requestModel)
        {
            var result = new DeletePostResponseModel();

            var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == requestModel.Id);

            if (post != null)
            {
                _context.Posts.Remove(post);
                await _context.SaveChangesAsync();
                result.IsSuccess = true;
                result.Message = $"{Constants.POST_DELETED}";
            }
            else
            {
                result.IsSuccess = false;
                result.Message = $"{Constants.POST_NOT_FOUND}";
            }

            return result;
        }

        public async Task<UpdatePostResponseModel> UpdatePostAsync(UpdatePostRequestModel requestModel)
        {
            var result = new UpdatePostResponseModel();
            var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == requestModel.Id);
            if (post != null && !string.IsNullOrWhiteSpace(requestModel.Content) && !string.IsNullOrWhiteSpace(requestModel.Title))
            {
                post.Title = requestModel.Title;
                post.Content = requestModel.Content;

                await _context.SaveChangesAsync();

                result.IsSuccess = true;
                result.Message = $"{Constants.POST_UPDATED}";
            }
            else
            {
                result.IsSuccess = false;
                result.Message = $"{Constants.POST_NOT_FOUND}";
            }

            return result;
        }

        public async Task<GetPostByIdResponseModel> GetPostByIdAsync(GetPostByIdRequestModel requestModel)
        {
            var result = new GetPostByIdResponseModel();

            result = await _context.Posts
                .Where(p => p.Id == requestModel.Id)
                .Select(p => new GetPostByIdResponseModel
                {
                    Id = p.Id,
                    Title = p.Title,
                    Content = p.Content
                }).SingleOrDefaultAsync();

            if (result == null)
            {
                var error = new GetPostByIdResponseModel();
                error.Message = $"{Constants.POST_NOT_FOUND}";
                return error;
            }

            result.IsSuccess = true;
            result.Message = "Successfully retreived Post";
            return result;
        }

        public async Task<TogglePostLikeResponseModel> TogglePostLikeAsync(GetPostToggleLikeRequestExtendedModel requestModel)
        {
            var result = new TogglePostLikeResponseModel();
            var IsUserValid = await _context.Users.AnyAsync(p => p.Id == requestModel.UserId);
            if (!IsUserValid)
            {
                result.IsSuccess = false;
                result.Message = $"{Constants.NO_SUCH_USER}";
                return result;
            }

            //TODO Use FindAsync();
            var userPostLike = await _context.UserPostLikes
                                             .Where(p => p.UserId == requestModel.UserId &&
                                                    p.PostId == requestModel.PostId)
                                             .SingleOrDefaultAsync();
            bool wasLiked = false;

            var shouldSaveChanges = false;
            var postQueryable = _context.Posts.Where(p => p.Id == requestModel.PostId).AsQueryable();
            if (requestModel.ShouldLike)
            {
                if (userPostLike != null)
                {
                    result.IsSuccess = false;
                    result.Message = $"{Constants.POST_ALREADY_LIKED}";
                    return result;
                }
                else
                {
                    var newUserPostLike = new UserPostLike
                    {
                        UserId = requestModel.UserId,
                        PostId = requestModel.PostId
                    };

                    var post = await postQueryable.FirstOrDefaultAsync(p => p.Id == requestModel.PostId);
                    if (post == null)
                    {
                        result.IsSuccess = false;
                        result.Message = Constants.POST_NOT_FOUND;
                        return result;
                    }
                    post.Rating++;
                    result.PostRating = post.Rating;

                    _context.UserPostLikes.Add(newUserPostLike);
                    wasLiked = true;
                    shouldSaveChanges = true;
                }
            }
            else
            {
                if (userPostLike == null)
                {
                    result.IsSuccess = false;
                    result.Message = $"{Constants.POST_NOT_LIKED}";
                    return result;
                }
                else
                {
                    var post = await postQueryable.SingleOrDefaultAsync();
                    if (post == null)
                    {
                        result.Message = "No such post";
                    }
                    else if (post.Rating > 0)
                    {
                        post.Rating--;
                        result.PostRating = post.Rating;
                        _context.UserPostLikes.Remove(userPostLike);
                        shouldSaveChanges = true;
                    }
                }
            }

            if (shouldSaveChanges)
            {
                await _context.SaveChangesAsync();
            }

            result.IsSuccess = true;
            result.IsLikedByUser = wasLiked;
            var likedPrefix = wasLiked ? "" : "dis";
            result.Message = $"Successfully {likedPrefix}liked";

            return result;
        }

        public async Task<CreatePostCommentResponseModel> CreatePostCommentAsync(CreatePostCommentExtendedRequestModel requestModel)
        {
            var today = DateTime.Now;

            var result = new CreatePostCommentResponseModel();

            if (string.IsNullOrWhiteSpace(requestModel.Content))
            {
                result.IsSuccess = false;
                result.Message = Constants.COMMENT_NO_CONTENT;
                return result;
            }

            var isUserExistent = await _context.Users.AnyAsync(p => p.Id == requestModel.UserId);
            if (!isUserExistent)
            {
                result.IsSuccess = false;
                result.Message = Constants.NO_SUCH_USER;
                return result;
            }

            var isPostExistent = await _context.Posts.AnyAsync(p => p.Id == requestModel.PostId);
            if (!isPostExistent)
            {
                result.IsSuccess = false;
                result.Message = Constants.POST_NOT_FOUND;
                return result;
            }

            var comment = new Comment
            {
                CreatorId = requestModel.UserId,
                Content = requestModel.Content,
                DateOfCreation = today,
                PostId = requestModel.PostId
            };

            _context.Comments.Add(comment);

            await _context.SaveChangesAsync();

            result.IsSuccess = true;
            result.Message = "Comment added successfully";

            return result;
        }
    }
}
