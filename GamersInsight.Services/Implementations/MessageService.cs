﻿using GamersInsight.Data;
using GamersInsight.DTOs.Helpers;
using GamersInsight.DTOs.RequestModels.Message;
using GamersInsight.DTOs.ResponseModels.Message;
using GamersInsight.Services.Interfaces;
using GamersInsight.Services.Utilities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GamersInsight.Services
{
    public class MessageService : IMessageService
    {
        private readonly GamersInsightContext _context;

        public MessageService(GamersInsightContext context)
        {
            _context = context;
        }

        public async Task<SendMessageResponseModel> SendMessageAsync(SendMessageExtendedRequestModel requestModel)
        {
            var result = new SendMessageResponseModel();

            var receiver = await _context.Users.FirstOrDefaultAsync(p => p.Id == requestModel.ReceiverId);
            if (receiver == null)
            {
                result.IsSuccess = false;
                result.Message = Constants.NO_SUCH_USER;
                return result;
            }

            var message = new Message
            {
                SenderId = requestModel.SenderId,
                MessageContent = requestModel.Content,
                ReceiverId = requestModel.ReceiverId,
                TimeOfCreation = DateTime.Now
            };

            _context.Messages.Add(message);
            await _context.SaveChangesAsync();

            result.IsSuccess = true;
            result.Message = Constants.MESSAGE_SENT;

            return result;
        }

        public async Task<ReadMessagesResponseModel> ReadMessageAsync(ReadMessagesPaginatedExtendedRequestModel requestModel)
        {
            var result = new ReadMessagesResponseModel();

            var toBeInConstant1 = "dbo.api_GetUserMessagesCount";

            var currentUserIdParam = new SqlParameter("CurrentUserId", requestModel.CurrentUserId);
            var otherParticipantIdParam = new SqlParameter("OtherParticipantId", requestModel.OtherParticipantId);

            var totalCountParam = new SqlParameter
            {
                ParameterName = "TotalCount",
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Output
            };

            await _context.Database.ExecuteSqlRawAsync($"EXEC {toBeInConstant1} @CurrentUserId, @OtherParticipantId, @TotalCount output", new[] { currentUserIdParam, otherParticipantIdParam, totalCountParam });
            var totalCount = totalCountParam.Value;

            result.TotalCount = (int)totalCount;

            if (result.TotalCount > 0)
            {
                var paginationResult = PaginationHelper.CalculatePages(result.TotalCount, requestModel.Skip, requestModel.Take);
                result.CurrentPage = paginationResult.CurrentPage;
                result.IsLastPage = paginationResult.IsLastPage;

                var skip = requestModel.Skip;
                if (requestModel.ShouldGetLastPage)
                {
                    skip = PaginationHelper.FindLastPageSkip(paginationResult.NumberOfPages, requestModel.Take);
                    result.CurrentPage = paginationResult.NumberOfPages;
                }

                var toBeInConstant2 = "dbo.api_GetUserMessagesPaginated";

                result.Data = await _context
                                      .MessageResponseModels.FromSqlRaw($"EXEC {toBeInConstant2} @CurrentUserId = {requestModel.CurrentUserId}, @OtherParticipantId = ${requestModel.OtherParticipantId}, @Skip = {skip}, @Take = {requestModel.Take};")
                                      .AsNoTracking()
                                      .ToListAsync();

                
                result.IsSuccess = true;
            }

            return result;
        }

    }
}
