﻿using GamersInsight.Data;
using GamersInsight.DTOs.ResponseModels.Home;
using GamersInsight.DTOs.ResponseModels.Threads;
using GamersInsight.Services.Interfaces;
using GamersInsight.Services.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GamersInsight.Services
{
    public class HomeService : IHomeService
    {
        private readonly GamersInsightContext _context;
        private readonly IMemoryCache _memoryCache;

        public HomeService(GamersInsightContext context, IMemoryCache memoryCache)
        {
            _context = context;
            _memoryCache = memoryCache;
        }

        public async Task<GetHomeInitialPageStatisticsResponseModel> GetInitialPageStatisticsAsync()
        {
            var result = new GetHomeInitialPageStatisticsResponseModel
            {
                Message = Constants.THREAD_NO_DATA
            };

            result.TotalPosts = await _context.Posts.CountAsync();

            result.TotalThreads = await _context.Threads.CountAsync();

            result.TotalUsers = await _context.Users.Where(p => p.IsActivated && !p.IsBlocked).CountAsync();

            result.Data = await (from t in _context.Threads
                                 join p in _context.Posts on t.Id equals p.ThreadId
                                 orderby p.DateOfCreation descending
                                 select new LatestThreadResponseModel
                                 {
                                     Id = t.Id,
                                     Name = t.ThreadName,
                                     LastActivePost = (from p2 in _context.Posts
                                                       join t2 in _context.Threads on p2.ThreadId equals t2.Id
                                                       orderby p2.DateOfCreation descending
                                                       where p2.ThreadId == t.Id
                                                       select new LastActivePostResponseModel
                                                       {
                                                           PostId = p2.Id,
                                                           Title = p2.Title,
                                                           LastActivityTime = p2.DateOfCreation
                                                       }).FirstOrDefault()
                                 })
                                .Take(Constants.GET_THREADS_STATISTICS_PAGE_SIZE)
                                .ToListAsync();

            if (result.Data != null && result.Data.Any())
            {
                result.IsSuccess = true;
                result.Message = $"{Constants.THREAD_TOTAL_COUNT}";
            }

            return result;
        }

        public async Task<GetHomeInitialPageStatisticsResponseModel> GetInitialPageStatisticsCachedAsync(string stats)
        {
            var cacheKey = stats;
            var result = new GetHomeInitialPageStatisticsResponseModel();

            if (!_memoryCache.TryGetValue(cacheKey, out result))
            {
                result = await GetInitialPageStatisticsAsync();

                var cacheExpirationOptions = new MemoryCacheEntryOptions
                {
                    AbsoluteExpiration = DateTime.Now.AddHours(1),
                    Priority = CacheItemPriority.Normal,
                    SlidingExpiration = TimeSpan.FromMinutes(5)
                };
                _memoryCache.Set(cacheKey, result, cacheExpirationOptions);
            }
            return result;
        }
    }
}
