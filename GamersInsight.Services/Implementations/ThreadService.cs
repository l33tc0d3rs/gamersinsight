﻿using GamersInsight.Data;
using GamersInsight.DTOs.RequestModels.Threads;
using GamersInsight.DTOs.ResponseModels.Threads;
using GamersInsight.DTOs.Helpers;
using GamersInsight.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System;
using GamersInsight.Services.Utilities;
using GamersInsight.DTOs.ResponseModels.Home;
using Microsoft.AspNetCore.Authorization;

namespace GamersInsight.Services
{
    public class ThreadService : IThreadService
    {
        private readonly GamersInsightContext _context;

        public ThreadService(GamersInsightContext context)
        {
            _context = context;
        }

        public async Task<CreateThreadResponseModel> CreateAsync(CreateThreadExtendedRequestModel requestModel)
        {
            var result = new CreateThreadResponseModel();

            var exists = await _context.Threads.Select(t => new
            {
                CategoryId = _context.Categories.Any(i => i.Id == requestModel.CategoryId),
                CreatorId = _context.Users.Any(i => i.Id == requestModel.CurrentUserId)
            })
                .FirstOrDefaultAsync();

            if (string.IsNullOrEmpty(requestModel.ThreadName) || !exists.CategoryId || !exists.CreatorId)
            {
                result.IsSuccess = false;
                result.Message = Constants.THREAD_NO_PARAMETERS;
                return result;
            }

            var thread = new Thread
            {
                ThreadName = requestModel.ThreadName,
                CategoryId = requestModel.CategoryId,
                CreatorId = requestModel.CurrentUserId,
                DateOfCreation = DateTime.Now
            };

            _context.Threads.Add(thread);

            await _context.SaveChangesAsync();

            result.IsSuccess = true;
            result.Message = $"{Constants.THREAD_CREATED}";

            return result;
        }

        public async Task<GetThreadsResponseModel> GetThreadsAsync(GetPaginatedThreadsRequestModel requestModel)
        {
            var threadsQueryable = (from t in _context.Threads
                                    join u in _context.Users on t.CreatorId equals u.Id
                                    where t.CategoryId == requestModel.CategoryId
                                    orderby t.DateOfCreation descending
                                    select new
                                    {
                                        Thread = t,
                                        User = u
                                    }).AsQueryable();

            var result = new GetThreadsResponseModel
            {
                TotalCount = await threadsQueryable.CountAsync()
            };

            if (result.TotalCount - requestModel.Skip > 0)
            {
                var paginationResult = PaginationHelper.CalculatePages(result.TotalCount, requestModel.Skip, requestModel.Take);
                result.CurrentPage = paginationResult.CurrentPage;
                result.IsLastPage = paginationResult.IsLastPage;

                var skip = requestModel.Skip;
                if (requestModel.ShouldGetLastPage)
                {
                    skip = PaginationHelper.FindLastPageSkip(paginationResult.NumberOfPages, requestModel.Take);
                    result.CurrentPage = paginationResult.NumberOfPages;
                }

                result.Data = await threadsQueryable
                                              .Skip(skip)
                                              .Take(requestModel.Take)                                             
                                              .Select(th => new GetThreadResponseModel
                                              {
                                                  Id = th.Thread.Id,
                                                  Name = th.Thread.ThreadName,
                                                  Author = th.User.UserName,
                                                  PostsCount = _context.Posts.Count(c => c.ThreadId == th.Thread.Id),
                                                  DateOfCreation = th.Thread.DateOfCreation,
                                                  LastActivePost = (from p2 in _context.Posts
                                                                    join t2 in _context.Threads on p2.ThreadId equals t2.Id
                                                                    orderby p2.DateOfCreation descending
                                                                    where p2.ThreadId == th.Thread.Id
                                                                    select new LastActivePostResponseModel
                                                                    {
                                                                        PostId = p2.Id,
                                                                        Title = p2.Title,
                                                                        LastActivityTime = p2.DateOfCreation
                                                                    }).FirstOrDefault()
                                              })
                                              .ToListAsync();
                result.IsSuccess = true;
                result.Message = $"{Constants.THREAD_TOTAL_COUNT}";
            }
            else
            {
                result.Message = Constants.THREAD_NO_DATA;
            }

            return result;
        }

        public async Task<GetHomeInitialPageStatisticsResponseModel> GetThreadsStatisticsAsync()
        {
            var result = new GetHomeInitialPageStatisticsResponseModel
            {
                Message = Constants.THREAD_NO_DATA
            };

            result.Data = await (from t in _context.Threads
                                 join p in _context.Posts on t.Id equals p.ThreadId
                                 orderby p.DateOfCreation descending
                                 select new LatestThreadResponseModel
                                 {
                                     Id = t.Id,
                                     Name = t.ThreadName,
                                     LastActivePost = (from p2 in _context.Posts
                                                       join t2 in _context.Threads on p2.ThreadId equals t2.Id
                                                       orderby p2.DateOfCreation descending
                                                       where p2.ThreadId == t.Id
                                                       select new LastActivePostResponseModel
                                                       {
                                                           PostId = p2.Id,
                                                           Title = p2.Title,
                                                           LastActivityTime = p2.DateOfCreation
                                                       }).FirstOrDefault()
                                 })
                                .Take(Constants.GET_THREADS_STATISTICS_PAGE_SIZE)
                                .ToListAsync();

            if (result.Data != null && result.Data.Any())
            {
                result.IsSuccess = true;
                result.Message = $"{Constants.THREAD_TOTAL_COUNT}";
            }

            return result;
        }

        [Authorize(Roles = UserRoleConstants.ADMIN)]
        public async Task<DeleteThreadResponseModel> DeleteThreadAsync(DeleteThreadRequestModel requestModel)
        {
            var result = new DeleteThreadResponseModel();

            var thread = await _context.Threads.FirstOrDefaultAsync(p => p.Id == requestModel.Id);

            if (thread != null)
            {
                _context.Threads.Remove(thread);
                await _context.SaveChangesAsync();
                result.IsSuccess = true;
                result.Message = $"{Constants.THREAD_DELETED}";
            }
            else
            {
                result.IsSuccess = false;
                result.Message = $"{Constants.THREAD_NOT_FOUND}";
            }

            return result;
        }

        public async Task<UpdateThreadResponseModel> UpdateThreadAsync(UpdateThreadRequestModel requestModel)
        {
            var result = new UpdateThreadResponseModel();

            var thread = await _context.Threads.FirstOrDefaultAsync(p => p.Id == requestModel.Id);

            if (thread != null)
            {
                if (!string.IsNullOrEmpty(requestModel.ThreadName) && requestModel.ThreadName != thread.ThreadName)
                {
                    thread.ThreadName = requestModel.ThreadName;
                }

                await _context.SaveChangesAsync();
                result.IsSuccess = true;
                result.Message = $"{Constants.THREAD_UPDATED}";
            }
            else
            {
                result.IsSuccess = false;
                result.Message = $"{Constants.THREAD_NOT_FOUND}";
            }

            return result;
        }

        public async Task<GetThreadByIdResponseModel> GetThreadByIdAsync(GetThreadByIdRequestModel requestModel)
        {
            var result = await _context.Threads
                                       .Where(t => t.Id == requestModel.Id)
                                       .Select(t => new GetThreadByIdResponseModel
                                       {
                                           Id = t.Id,
                                           Name = t.ThreadName
                                       })
                                       .FirstOrDefaultAsync();
            if (result != null)
            {
                result.Message = "Found Thread";
                result.IsSuccess = true;
                return result;
            }
            else
            {
                var error = new GetThreadByIdResponseModel();
                error.Message = Constants.THREAD_NOT_FOUND;
                error.IsSuccess = false;
                return error;
            }
        }
    }
}
