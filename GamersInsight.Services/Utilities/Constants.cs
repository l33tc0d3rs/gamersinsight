﻿//TODO -- Fix

namespace GamersInsight.Services.Utilities
{
    public class Constants
    {
        public const string USER_ALREADY_EXISTS = "User or Customer Already Exists!";
        public const string EMAIL_ALREADY_EXISTS = "Email Already Registered to a User!";
        public const string CITY_NOT_SUPPORTED = "City not supported.";
        public const string USER_REGISTERED = "You have been registered!";
        public const string PASSWORD_IS_INCORRECT = "Password is incorrect!";
        public const string USER_LOGGED = "You have logged successfully.";
        public const string NO_SUCH_USER = "No such user!";
        public const string USER_USERNAME_ERROR = "Username must be between 2 and 20 symbols long";
        public const string USER_DISPLAYNAME_ERROR = "Username must be between 2 and 20 symbols long";
        public const string USER_EMAIL_ERROR = "Invalid Email";
        public const string USER_PASSWORD = "Password must be at least 8 symbols and should contain capital letter, digit and special symbol (+, -, *, &, ^, …)";
        public const string PASSWORD_NO_MATCH = "Repeated password does not match.";
        public const string USER_UPDATED = "User updated successfully!";
        public const string USER_SETTING_RETRIVED = "User Settings retreived successfully!";
        public const string USER_BLOCKED = "User Blocked!";
        public const string USER_UNBLOCKED = "User Unblocked!";

        public const int PAGINATION_SIZE = 10;

        public const string EMAIL_REGEX = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$";
        public const string AdminRoleName = "Admin";
        public const string EmployeeRoleName = "Employee";
        public const string CustomerRoleName = "Customer";

        public const string CATEGORY_NAME_DESCRIPTION_EMPTY = "Please provide Category Name and Description";
        public const string CATEGORY_EXIST = "Category with that name already exist";
        public const string CATEGORY_CREATED = "Category created.";
        public const string CATEGORY_NO_DATA = "No data to display";
        public const string CATEGORY_TOTAL_COUNT = "Total Categories: ";
        public const string CATEGORY_DELETED = "Category deleted.";
        public const string CATEGORY_NOT_FOUND = "Category does not exist";
        public const string CATEGORY_UPDATED = "Category updated.";

        public const string POST_NO_CONTENT = "Please provide Content";
        public const string POST_CREATED = "Post was created.";
        public const string POST_NO_DATA = "No data to display";
        public const string POST_TOTAL_COUNT = "Total Post: ";
        public const string POST_DELETED = "Post deleted.";
        public const string POST_NOT_FOUND = "Post does not exist";
        public const string POST_UPDATED = "Post was updated.";
        public const string POST_ALREADY_LIKED = "Post already liked by this user.";
        public const string POST_NOT_LIKED = "Post is not liked";

        public const string THREAD_NO_PARAMETERS = "Please provide proper parameters!";
        public const string THREAD_CREATED = "Thread created.";
        public const string THREAD_NO_DATA = "No data to display";
        public const string THREAD_TOTAL_COUNT = "Total Threads: ";
        public const string THREAD_DELETED = "Thread deleted.";
        public const string THREAD_NOT_FOUND = "Thread does not exist";
        public const string THREAD_UPDATED = "Thread updated.";
        public const string ThreadGridCategoryIdInit = "ThreadGridCategoryIdInit";

        public const string COMMENT_NO_CONTENT = "Comment cannot be empty";

        public const string MESSAGE_SENT = "Message sent";

        public const int GET_THREADS_STATISTICS_PAGE_SIZE = 5;

        public const string DOMAIN_CONSTANT_HASH = "/User/Verify?token=";
    }
}
