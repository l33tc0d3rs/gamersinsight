﻿namespace GamersInsight.Services.Utilities
{
    public static class UserRoleConstants
    {
        public const string ADMIN = "Admin";
        public const string MODERATOR = "Moderator";
        public const string USER = "User";
        public const string NOTVERIFIED = "NotVerified";
    }
}
