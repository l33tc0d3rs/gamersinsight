﻿using GamersInsight.DTOs.RequestModels;
using System.Threading.Tasks;

namespace GamersInsight.Services.Interfaces
{
    public interface IMailService
    {
         Task SendEmailAsync(MailRequestModel mailRequest, string BaseUrl);
    }
}
