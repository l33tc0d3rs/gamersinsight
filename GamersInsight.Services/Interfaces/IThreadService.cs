﻿using GamersInsight.DTOs.RequestModels.Threads;
using GamersInsight.DTOs.ResponseModels.Home;
using GamersInsight.DTOs.ResponseModels.Threads;

using System.Threading.Tasks;


namespace GamersInsight.Services.Interfaces
{
    public interface IThreadService
    {
        Task<CreateThreadResponseModel> CreateAsync(CreateThreadExtendedRequestModel requestModel);
        Task<GetThreadByIdResponseModel> GetThreadByIdAsync(GetThreadByIdRequestModel requestModel);
        Task<GetThreadsResponseModel> GetThreadsAsync(GetPaginatedThreadsRequestModel requestModel);
        Task<DeleteThreadResponseModel> DeleteThreadAsync(DeleteThreadRequestModel requestModel);
        Task<UpdateThreadResponseModel> UpdateThreadAsync(UpdateThreadRequestModel requestModel);
        Task<GetHomeInitialPageStatisticsResponseModel> GetThreadsStatisticsAsync();
    }
}
