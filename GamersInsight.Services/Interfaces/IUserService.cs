﻿using GamersInsight.DTOs.RequestModels;
using GamersInsight.DTOs.RequestModels.User;
using GamersInsight.DTOs.ResponseModels.User;
using GamersInsight.Models.RequestModels;
using GamersInsight.Models.ResponseModels;
using System.Threading.Tasks;

namespace GamersInsight.Services.Interfaces
{
    public interface IUserService
    {
        Task<GetLoggedUserInfoResponseModel> GetLoggedUserInfoAsync(GetLoggedUserInfoRequestModel requestModel);
        Task<RegisterUserResponseModel> RegisterUser(RegisterUserRequestExtendedModel requestModel);
        Task<UpdateUserResponseModel> UpdateUserAsync(UpdateUserExtendedRequestModel requestModel);
        Task<UpdateAvatarResponseModel> UpdateAvatarAsync(UpdateAvatarExtendedRequestModel requestModel);
        Task<GetUserSettingsResponseModel> GetUserSettingsAsync(GetUserSettingsRequestModel requestModel);
        Task<GetUserShortInfoResponseModel> GetUserShortInfoAsync(GetUserSettingsRequestModel requestModel);
        Task<GetManagementResponseModel> GetUsersAsync(GetManagementRequestModel requestModel);
        Task<BlockUserResponseModel> BlockUserAsync(BlockUserRequestModel requestModel);
        Task<UnblockUserResponseModel> UnblockUserAsync(UnblockUserRequestModel requestModel);
        Task<string> ConfirmMail(string token);
    }
}
