﻿using GamersInsight.DTOs.RequestModels.Post;
using GamersInsight.DTOs.ResponseModels.Comment;
using GamersInsight.DTOs.ResponseModels.Post;
using System.Threading.Tasks;

namespace GamersInsight.Services.Interfaces
{
    public interface IPostService
    {
        Task<CreatePostResponseModel> CreateAsync(CreatePostExtendedRequestModel requestModel);
        Task<GetPostsResponseModel> GetPostsAsync(GetPagintedPostsRequestExtendedModel requestModel);
        Task<DeletePostResponseModel> DeletePostAsync(DeletePostRequestModel requestModel);
        Task<UpdatePostResponseModel> UpdatePostAsync(UpdatePostRequestModel requestModel);
        Task<GetPostByIdResponseModel> GetPostByIdAsync(GetPostByIdRequestModel requestModel);
        Task<TogglePostLikeResponseModel> TogglePostLikeAsync(GetPostToggleLikeRequestExtendedModel requestModel);
        Task<CommentsResponseModel> GetPostCommentsAsync(GetPostCommentsRequestModel requestModel);
        Task<CreatePostCommentResponseModel> CreatePostCommentAsync(CreatePostCommentExtendedRequestModel requestModel);
    }
}
