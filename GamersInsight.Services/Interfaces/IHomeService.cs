﻿using GamersInsight.DTOs.ResponseModels.Home;

using System.Threading.Tasks;


namespace GamersInsight.Services.Interfaces
{
    public interface IHomeService
    {
        Task<GetHomeInitialPageStatisticsResponseModel> GetInitialPageStatisticsAsync();
        Task<GetHomeInitialPageStatisticsResponseModel> GetInitialPageStatisticsCachedAsync(string stats);
    }
}
