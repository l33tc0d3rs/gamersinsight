﻿using GamersInsight.DTOs.RequestModels.Conversation;
using GamersInsight.DTOs.ResponseModels.Conversation;
using System.Threading.Tasks;

namespace GamersInsight.Services.Interfaces
{
    public interface IConversationService
    {
        Task<PaginatedConversationResponseModel> ConversationsAsync(GetConversationsExtendedRequestModel requestModel);
    }
}
