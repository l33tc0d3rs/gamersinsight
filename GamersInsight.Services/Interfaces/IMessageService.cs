﻿using GamersInsight.DTOs.RequestModels.Message;
using GamersInsight.DTOs.ResponseModels.Message;
using System.Threading.Tasks;

namespace GamersInsight.Services.Interfaces
{
    public interface IMessageService
    {
        Task<SendMessageResponseModel> SendMessageAsync(SendMessageExtendedRequestModel requestModel);
        Task<ReadMessagesResponseModel> ReadMessageAsync(ReadMessagesPaginatedExtendedRequestModel requestModel);
    }
}
