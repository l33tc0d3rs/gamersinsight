﻿using GamersInsight.DTOs.RequestModels;
using GamersInsight.DTOs.ResponseModels;
using System.Threading.Tasks;

namespace GamersInsight.Services.Interfaces
{
    public interface ICategoryService
    {
        Task<CreateCategoryResponseModel> CreateAsync(CreateCategoryRequestModel requestModel);
        Task<GetCategoriesResponseModel> GetCategoriesAsync(GetExtendedCategoriesModel requestModel);
        Task<DeleteCategoryResponseModel> DeleteCategoryAsync(DeleteCategoryRequestModel requestModel);
        Task<UpdateCategoryResponseModel> UpdateCateogryAsync(UpdateCategoryRequestModel requestModel);
        Task<GetCategoryByIdResponseModel> GetCategoryByIdAsync(GetCategoryByIdRequestModel requestModel);
    }
}
