﻿using System;

namespace GamersInsight.Data
{
    public class MessageResponseModel
    {
        public long SenderId { get; set; }
        public long ReceiverId { get; set; }
        public string SenderDisplayName { get; set; }
        public string ReceiverDisplayName { get; set; }
        public string Message { get; set; }
        public DateTime TimeOfCreation { get; set; }
    }
}
