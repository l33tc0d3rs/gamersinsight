﻿using Microsoft.EntityFrameworkCore;

namespace GamersInsight.Data
{
    public class GamersInsightContext : DbContext
    {
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<Thread> Threads { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<UserPostLike> UserPostLikes { get; set; }

        public virtual DbSet<MessageResponseModel> MessageResponseModels { get; set; }
        public virtual DbSet<MessageResponseCount> MessageResponseCount { get; set; }

        public GamersInsightContext(DbContextOptions options) : base(options)
        {
        }

        public GamersInsightContext()
        {
        }

        private readonly string _connectionString = "Server=localhost;Database=GamersInsight_DB;Trusted_Connection=True;MultipleActiveResultSets=true";

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            if (!builder.IsConfigured)
            {
                builder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Message>()
               .HasOne(p => p.Sender)
               .WithMany(x => x.MessagesSent)               
               .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<Message>()
              .HasOne(p => p.Receiver)
              .WithMany(x => x.MessagesRecived)
              .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<Thread>()
              .HasOne(p => p.Creator)
              .WithMany(p => p.Threads)
              .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<Post>()
              .HasOne(p => p.Creator)
              .WithMany(p => p.Posts)
              .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<User>()
              .HasOne(u => u.Role)
              .WithMany(r => r.Users)
              .HasForeignKey(u => u.RoleId)
              .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<UserPostLike>()
                .HasKey(c => new { c.UserId, c.PostId });

            builder.Entity<MessageResponseModel>().HasNoKey().ToView(null);

            builder.Entity<MessageResponseCount>().HasNoKey().ToView(null);

            base.OnModelCreating(builder);
        }     
    }
}
