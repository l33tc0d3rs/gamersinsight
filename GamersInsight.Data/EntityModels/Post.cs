﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GamersInsight.Data
{
    public class Post
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        [ForeignKey("Thread")]
        public Guid ThreadId { get; set; }
        public Thread Тhread { get; set; }
        [Required]
        [ForeignKey("User")]
        public long CreatorId { get; set; }
        public User Creator { get; set; }
        [Required]
        public DateTime DateOfCreation { get; set; }
        public long Rating { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
