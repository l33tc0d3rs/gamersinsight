﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GamersInsight.Data
{
    public class Message
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [ForeignKey("User")]
        public long SenderId { get; set; }
        public User Sender { get; set; }
        [Required]
        [ForeignKey("User")]
        public long ReceiverId { get; set; }
        public User Receiver { get; set; }

        public string MessageContent { get; set; }
        public DateTime TimeOfCreation { get; set; }
    }
}
