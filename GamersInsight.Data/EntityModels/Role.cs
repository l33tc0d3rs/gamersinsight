﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GamersInsight.Data
{
    public class Role
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string RoleName { get; set; }
        [Required]
        public string Alies { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
