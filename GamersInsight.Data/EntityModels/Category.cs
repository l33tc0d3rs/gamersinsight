﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GamersInsight.Data
{
    public class Category
    {
        [Key]
        public long Id { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public bool NSFW { get; set; } // TODO: change to IsNsfw 
        public virtual ICollection<Thread> Threads { get; set; }
    }
}
