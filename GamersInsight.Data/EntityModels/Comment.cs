﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GamersInsight.Data
{
    public class Comment
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        [ForeignKey("Post")]
        public Guid PostId { get; set; }
        public Post Post { get; set; }
        [Required]
        [ForeignKey("User")]
        public long CreatorId { get; set; }
        public User Creator { get; set; }
        [Required]
        public DateTime DateOfCreation { get; set; }
        public long Rating { get; set; } // TODO - REMOVE THIS!!!
    }
}
