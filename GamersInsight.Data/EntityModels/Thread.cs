﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GamersInsight.Data
{
    public class Thread
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string ThreadName { get; set; }
        [Required]
        [ForeignKey("Category")]
        public long CategoryId { get; set; }
        public Category Category { get; set; }
        [Required]
        [ForeignKey("User")]
        public long CreatorId { get; set; }
        public User Creator { get; set; }
        [Required]
        public DateTime DateOfCreation { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}
