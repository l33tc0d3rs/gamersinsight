﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GamersInsight.Data
{
    public class UserPostLike
    {
        [Required]
        [ForeignKey("User")]
        public long UserId { get; set; }
        [Required]
        [ForeignKey("Post")]
        public Guid PostId { get; set; }
    }
}
