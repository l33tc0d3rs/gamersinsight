﻿using GamersInsight.Data;
using GamersInsight.Services.UnitTests.Helpers;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GamersInsight.Services.UnitTests
{
    [TestClass]
    public class HomeServiceTests
    {
        [TestMethod]
        public async Task UserService_ConversationsAsync_returnTrue()
        {
            var mockContext = MockDb.MockDbContext;
            var memoryCache = new Mock<IMemoryCache>();

            var service = new HomeService(mockContext.Object, memoryCache.Object);

            var result = await service.GetInitialPageStatisticsAsync();

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task UserService_ConversationsAsync_returnFalse_NoData()
        {
            var mockDbContext = new Mock<GamersInsightContext>();

            var roles = new List<Role>()
                 {
                    new Role {Id = 1, RoleName = "User" , Alies = "Commoner"},
                    new Role {Id = 2, RoleName = "Moderator", Alies = "Knight"},
                    new Role {Id = 3, RoleName = "Admin", Alies = "King"}
                };
            var categories = new List<Category>()
                 {
                    new Category {Id = 1, CategoryName = "RPG", Description = "For the role players", NSFW = false},
                    new Category {Id = 2, CategoryName = "Racing", Description = "How fast can you go", NSFW = false},
                    new Category {Id = 3, CategoryName = "Mobile", Description = "Why does this exist even", NSFW = false},
                    new Category {Id = 4, CategoryName = "Sandbox", Description = "Show your creativity", NSFW = false},
                };
            var users = new List<User>()
                {
                    new User {Id = 1, UserName = "Test1", Password = "$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq",
                              Email = "test1@mail.com", DisplayName = "TestUser1", Avatar = "/images/test.jpg",
                              DateOfCreation = DateTime.Now.AddMonths(-2),
                              DateOfBirth = DateTime.Now.AddYears(-19), RoleId = 1, IsActivated = true, IsBlocked = false, Rating = 0},
                    new User {Id = 1, UserName = "Test2", Password = "$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq",
                              Email = "test2@mail.com", DisplayName = "TestUser2", Avatar = "/images/test.jpg",
                              DateOfCreation = DateTime.Now.AddMonths(-2),
                              DateOfBirth = DateTime.Now.AddYears(-15), RoleId = 1, IsActivated = true, IsBlocked = false, Rating = 0},
                    new User {Id = 1, UserName = "Test3", Password = "$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq",
                              Email = "test3@mail.com", DisplayName = "TestUser3", Avatar = "/images/test.jpg",
                              DateOfCreation = DateTime.Now.AddMonths(-2),
                              DateOfBirth = DateTime.Now.AddYears(-17), RoleId = 1, IsActivated = true, IsBlocked = false, Rating = 0},
                    new User {Id = 1, UserName = "Test4", Password = "$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq",
                              Email = "test4@mail.com", DisplayName = "TestUser4", Avatar = "/images/test.jpg",
                              DateOfCreation = DateTime.Now.AddMonths(-2),
                              DateOfBirth = DateTime.Now.AddYears(-23), RoleId = 1, IsActivated = true, IsBlocked = false, Rating = 0}
                };
            var threads = new List<Data.Thread>();
            var posts = new List<Post>();
            var postLikes = new List<UserPostLike>();

            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockCategories = categories.AsQueryable().BuildMockDbSet();
            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockThreads = threads.AsQueryable().BuildMockDbSet();
            var mockPosts = posts.AsQueryable().BuildMockDbSet();
            var mockPostLikes = postLikes.AsQueryable().BuildMockDbSet();

            mockDbContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockDbContext.Setup(db => db.Categories).Returns(mockCategories.Object);
            mockDbContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockDbContext.Setup(db => db.Threads).Returns(mockThreads.Object);
            mockDbContext.Setup(db => db.Posts).Returns(mockPosts.Object);
            mockDbContext.Setup(db => db.UserPostLikes).Returns(mockPostLikes.Object);
            var memoryCache = new Mock<IMemoryCache>();

            var service = new HomeService(mockDbContext.Object, memoryCache.Object);

            var result = await service.GetInitialPageStatisticsAsync();

            Assert.AreEqual(false, result.IsSuccess);
        }
    }
}
