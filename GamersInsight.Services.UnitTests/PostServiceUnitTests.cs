﻿using GamersInsight.DTOs.RequestModels.Post;
using GamersInsight.Services.UnitTests.Helpers;
using GamersInsight.Services.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace GamersInsight.Services.UnitTests
{
    [TestClass]
    public class PostServiceUnitTests
    {
        [TestMethod]
        public async Task PostService_Create_ShouldReturnFalse_WrongThread()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new PostService(mockContext.Object);

            var request = new CreatePostExtendedRequestModel()
            {
                ThreadId = new Guid("9a3ed572-3bc5-4918-a033-82934a2017ec"),
                Content = "Testing Service",
                Title = "Test Title",
                UserId = 1
            };

            var result = await service.CreateAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
        }

        [TestMethod]
        public async Task PostService_Create_ShouldReturnFalse_UserNotExist()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new PostService(mockContext.Object);

            var request = new CreatePostExtendedRequestModel()
            {
                ThreadId = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec"),
                Content = "Testing Service",
                Title = "Test Title",
                UserId = 9
            };

            var result = await service.CreateAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.NO_SUCH_USER, result.Message);
        }

        [TestMethod]
        public async Task PostService_Create_ShouldReturnFalse_NoContent()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new PostService(mockContext.Object);

            var request = new CreatePostExtendedRequestModel()
            {
                ThreadId = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec"),
                Content = null,
                Title = "Test Title",
                UserId = 1
            };

            var result = await service.CreateAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.POST_NO_CONTENT, result.Message);
        }

        [TestMethod]
        public async Task PostService_Create_ShouldReturnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new PostService(mockContext.Object);

            var request = new CreatePostExtendedRequestModel()
            {
                ThreadId = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec"),
                Content = "Test Content",
                Title = "Test Title",
                UserId = 1
            };

            var result = await service.CreateAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        // GET TESTS START // 

        [TestMethod]
        public async Task PostService__ShouldReturnFalse_NoContent()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new PostService(mockContext.Object);

            var request = new GetPagintedPostsRequestExtendedModel()
            {
                ThreadId = new Guid("1a5ed572-3bc5-4918-a03b-82934a2017ec"),
                ShouldGetLastPage = false,
                Skip = 0,
                Take = 5,
                CommentsTakeAmount = 3,
                UserId = 1
            };

            var result = await service.GetPostsAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.THREAD_NOT_FOUND, result.Message);
        }

        [TestMethod]
        public async Task PostService_GetPostsAsync_ShouldReturnFalse_()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new PostService(mockContext.Object);

            var request = new GetPagintedPostsRequestExtendedModel()
            {
                ThreadId = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec"),
                ShouldGetLastPage = false,
                Skip = 30,
                Take = 5,
                CommentsTakeAmount = 3,
                UserId = 1
            };

            var result = await service.GetPostsAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.POST_NO_DATA, result.Message);
        }

        [TestMethod]
        public async Task PostService_GetPostsAsync_ShouldReturnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new PostService(mockContext.Object);

            var request = new GetPagintedPostsRequestExtendedModel()
            {
                ThreadId = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec"),
                ShouldGetLastPage = false,
                Skip = -10,
                Take = 5,
                CommentsTakeAmount = 3,
                UserId = 1
            };

            var result = await service.GetPostsAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
            Assert.AreEqual(2, result.TotalCount);
        }

        [TestMethod]
        public async Task PostService_GetPostsAsync_ShouldReturnTrue_LastPage()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new PostService(mockContext.Object);

            var request = new GetPagintedPostsRequestExtendedModel()
            {
                ThreadId = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec"),
                ShouldGetLastPage = true,
                Skip = 0,
                Take = 5,
                CommentsTakeAmount = 3,
                UserId = 1
            };

            var result = await service.GetPostsAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
            Assert.AreEqual(2, result.TotalCount);
        }

        [TestMethod]
        public async Task PostService_GetPostsAsync_ShouldReturnTrue_Ascending()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new PostService(mockContext.Object);

            var request = new GetPagintedPostsRequestExtendedModel()
            {
                ThreadId = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec"),
                ShouldGetLastPage = false,
                OrderByDateAscending = false,
                Skip = 0,
                Take = 5,
                CommentsTakeAmount = 3,
                UserId = 1
            };

            var result = await service.GetPostsAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
            Assert.AreEqual(2, result.TotalCount);
        }

        [TestMethod]
        public async Task PostService_GetPostsAsync_ShouldReturnTrue_Search()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new PostService(mockContext.Object);

            var request = new GetPagintedPostsRequestExtendedModel()
            {
                ThreadId = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec"),
                ShouldGetLastPage = false,
                OrderByDateAscending = false,
                ContentSearchTerm = "SearchWork?",
                Skip = 0,
                Take = 5,
                CommentsTakeAmount = 3,
                UserId = 1
            };

            var result = await service.GetPostsAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
            Assert.AreEqual(1, result.TotalCount);
        }

        [TestMethod]
        public async Task PostService_GetPostsByIdAsync_ShouldReturnTrue()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new GetPostByIdRequestModel()
            {
                Id = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4")
            };

            var result = await service.GetPostByIdAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task PostService_GetPostsByIdAsync_ShouldReturnFalse_PostNotFound()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new GetPostByIdRequestModel()
            {
                Id = new Guid("c79894aa-917d-4a97-9ffc-2290d791b0c4"),
                
            };

            var result = await service.GetPostByIdAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
        }

        // DELETE TESTS START //
        [TestMethod]
        public async Task PostService_DeletePostsAsync_ShouldReturnTrue_PostExist()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new DeletePostRequestModel()
            {
                Id = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4")
            };

            var result = await service.DeletePostAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task PostService_DeletePostsAsync_ShouldReturnFalse_PostDontExist()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new DeletePostRequestModel()
            {
                Id = new Guid("c89894aa-997d-4a97-9ffc-2290d791b0c4")
            };

            var result = await service.DeletePostAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
        }

        // UPDATE TESTS START //

        [TestMethod]
        public async Task PostService_UpdatePostsAsync_ShouldReturnFalse_PostDontExist()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new UpdatePostRequestModel()
            {
                Id = new Guid("c89894aa-997d-4a97-9ffc-2290d791b0c4")
            };

            var result = await service.UpdatePostAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
        }

        [TestMethod]
        public async Task PostService_UpdatePostsAsync_ShouldReturnTrue_PostUpdated()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new UpdatePostRequestModel()
            {
                Id = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"),
                Content = "edited",
                Title = "NewTitle"
            };
            
            var result = await service.UpdatePostAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        // CREATE POST TESTS START // 
        [TestMethod]
        public async Task PostService_CreatePostsAsync_ShouldReturnTrue_PostCreated()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new CreatePostCommentExtendedRequestModel()
            {
                Content = "Random0",
                PostId = new Guid("1e4ee427-3299-44a6-8394-e09129196c1d"),
                UserId = 4
            };

            var result = await service.CreatePostCommentAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task PostService_CreatePostsAsync_ShouldReturnFalse_NoSuchUser()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new CreatePostCommentExtendedRequestModel()
            {
                Content = "Random0",
                PostId = new Guid("c89894aa-997d-4a97-9ffc-2290d791b0c4"),
                UserId = 9
            };

            var result = await service.CreatePostCommentAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.NO_SUCH_USER, result.Message);
        }

        [TestMethod]
        public async Task PostService_CreatePostsAsync_ShouldReturnFalse_PostNotExist()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new CreatePostCommentExtendedRequestModel()
            {
                Content = "Random0",
                PostId = new Guid("c83894aa-997d-4a97-9ffc-2290d791b0c4"),
                UserId = 1
            };

            var result = await service.CreatePostCommentAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.POST_NOT_FOUND, result.Message);
        }

        [TestMethod]
        public async Task PostService_CreatePostsAsync_ShouldReturnFalse_NoContent()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new CreatePostCommentExtendedRequestModel()
            {
                Content = "",
                PostId = new Guid("c89894aa-997d-4a97-9ffc-2290d791b0c4"),
                UserId = 1
            };

            var result = await service.CreatePostCommentAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.COMMENT_NO_CONTENT, result.Message);
        }

        [TestMethod]
        public async Task PostService_CreatePostsAsync_ShouldReturnTrue()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new CreatePostCommentExtendedRequestModel()
            {
                Content = "TEST1",
                PostId = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"),
                UserId = 1
            };

            var result = await service.CreatePostCommentAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        // GET TESTS START //

        [TestMethod]
        public async Task PostService_GetPostCommentsAsync_ShouldReturnTrue()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new GetPostCommentsRequestModel()
            {
                PostId = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"),
                Skip = 0,
                Take = 3
            };

            var result = await service.GetPostCommentsAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task PostService_GetPostCommentsAsync_ShouldReturnFalse_NoContent()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new GetPostCommentsRequestModel()
            {
                PostId = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"),
                Skip = 4,
                Take = 3
            };

            var result = await service.GetPostCommentsAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.POST_NO_DATA, result.Message);
        }

        // TogglePostLikeAsync TESTS START // 

        [TestMethod]
        public async Task PostService_TogglePostLikeAsync_ShouldReturnTrue()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new GetPostToggleLikeRequestExtendedModel()
            {
                PostId = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"),
                ShouldLike = false,
                UserId = 1
            };

            var result = await service.TogglePostLikeAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task PostService_TogglePostLikeAsync_ShouldReturnFalse_NoSuchUser()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new GetPostToggleLikeRequestExtendedModel()
            {
                PostId = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"),
                ShouldLike = false,
                UserId = 9
            };

            var result = await service.TogglePostLikeAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.NO_SUCH_USER, result.Message);
        }

        [TestMethod]
        public async Task PostService_TogglePostLikeAsync_ShouldReturnTrue_ShouldLikeTrue()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new GetPostToggleLikeRequestExtendedModel()
            {
                PostId = new Guid("57f18194-fea0-460b-bcbf-4fb4021c0f06"),
                ShouldLike = true,
                UserId = 3
            };

            var result = await service.TogglePostLikeAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task PostService_TogglePostLikeAsync_ShouldReturnTrue_ShouldLikeFalse()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new GetPostToggleLikeRequestExtendedModel()
            {
                PostId = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"),
                ShouldLike = false,
                UserId = 3
            };

            var result = await service.TogglePostLikeAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task PostService_TogglePostLikeAsync_ShouldReturnFalse_ShouldLikeFalse()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new GetPostToggleLikeRequestExtendedModel()
            {
                PostId = new Guid("57f18194-fea0-460b-bcbf-4fb4021c0f06"),
                ShouldLike = false,
                UserId = 1
            };

            var result = await service.TogglePostLikeAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
        }

        [TestMethod]
        public async Task PostService_TogglePostLikeAsync_ShouldReturnTrue_NoSuchPost()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new GetPostToggleLikeRequestExtendedModel()
            {
                PostId = new Guid("3e57e5aa-49f0-4e95-8715-c98f1ec67467"),
                ShouldLike = false,
                UserId = 1
            };

            var result = await service.TogglePostLikeAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task PostService_TogglePostLikeAsync_ShouldReturnFalse_AlreadyLiked()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new GetPostToggleLikeRequestExtendedModel()
            {
                PostId = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"),
                ShouldLike = true,
                UserId = 1
            };

            var result = await service.TogglePostLikeAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.POST_ALREADY_LIKED, result.Message);
        }

        [TestMethod]
        public async Task PostService_TogglePostLikeAsync_ShouldReturnFalse_PostDoesNotExist()
        {
            var mockContext = MockDb.MockDbContext;
            var service = new PostService(mockContext.Object);

            var request = new GetPostToggleLikeRequestExtendedModel()
            {
                PostId = new Guid("3e57e5aa-49f0-4e95-8715-c98f1ec67467"),
                ShouldLike = true,
                UserId = 2
            };

            var result = await service.TogglePostLikeAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
        }
    }
}
