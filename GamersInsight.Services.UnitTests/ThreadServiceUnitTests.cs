﻿using GamersInsight.Data;
using GamersInsight.DTOs.RequestModels.Threads;
using GamersInsight.Services.UnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GamersInsight.Services.UnitTests
{
    [TestClass]
    public class ThreadServiceUnitTests
    {
        [TestMethod]
        public async Task ThreadService_Create_ShouldReturnFalse_BadParametersAsync()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);

            var thread = new CreateThreadExtendedRequestModel()
            {
                ThreadName = "",
                CategoryId = 1,
                CurrentUserId = 1
            };

            var result = await service.CreateAsync(thread);

            Assert.AreEqual(false, result.IsSuccess);
        }

        [TestMethod]
        public async Task ThreadService_Create_ShouldReturnFalse_InvalidCategory()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);

            var thread = new CreateThreadExtendedRequestModel()
            {
                ThreadName = "Test1",
                CategoryId = 9,
                CurrentUserId = 1
            };

            var result = await service.CreateAsync(thread);

            Assert.AreEqual(false, result.IsSuccess);
        }

        [TestMethod]
        public async Task ThreadService_Create_ShouldReturnFalse_InvalidCreator()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);

            var thread = new CreateThreadExtendedRequestModel()
            {
                ThreadName = "Test1",
                CategoryId = 1,
                CurrentUserId = 9
            };

            var result = await service.CreateAsync(thread);

            Assert.AreEqual(false, result.IsSuccess);
        }

        [TestMethod]
        public async Task ThreadService_Create_ShouldReturnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);

            var thread = new CreateThreadExtendedRequestModel()
            {
                ThreadName = "Test1",
                CategoryId = 1,
                CurrentUserId = 1
            };

            var result = await service.CreateAsync(thread);

            Assert.AreEqual(true, result.IsSuccess);
        }


        // GET THREADS START //

        [TestMethod]
        public async Task ThreadService_GetThreadsAsync_ShouldReturnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);

            var thread = new GetPaginatedThreadsRequestModel()
            {
                CategoryId = 1,
                ShouldGetLastPage = false,
                Skip = 0,
                Take = 5
            };

            var result = await service.GetThreadsAsync(thread);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task ThreadService_GetThreadsAsync_ShouldReturnTrue_LastPage()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);

            var thread = new GetPaginatedThreadsRequestModel()
            {
                CategoryId = 1,
                ShouldGetLastPage = true,
                Skip = 0,
                Take = 5
            };

            var result = await service.GetThreadsAsync(thread);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task ThreadService_GetThreadsAsync_ShouldReturnFalse_NoMoreElements()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);

            var thread = new GetPaginatedThreadsRequestModel()
            {
                CategoryId = 1,
                ShouldGetLastPage = true,
                Skip = 8,
                Take = 5
            };

            var result = await service.GetThreadsAsync(thread);

            Assert.AreEqual(false, result.IsSuccess);
        }

        // DELETE TESTS START // 

        [TestMethod]
        public async Task ThreadService_DeleteThreadAsync_ShouldReturnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);

            var thread = new DeleteThreadRequestModel()
            {
                Id = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec")                
            };

            var result = await service.DeleteThreadAsync(thread);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task ThreadService_DeleteThreadAsync_ShouldReturnFalse_WrongId()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);

            var thread = new DeleteThreadRequestModel()
            {
                Id = new Guid("9a5ed572-3bc5-4918-a03b-82954a2017ec")
            };

            var result = await service.DeleteThreadAsync(thread);

            Assert.AreEqual(false, result.IsSuccess);
        }

        // UPDATE TESTS START //

        [TestMethod]
        public async Task ThreadService_UpdateThreadAsync_ShouldReturnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);

            var thread = new UpdateThreadRequestModel()
            {
                Id = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec"),
                ThreadName = "New Name"
            };

            var result = await service.UpdateThreadAsync(thread);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task ThreadService_UpdateThreadAsync_ShouldReturnFalse_IdNotFound()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);

            var thread = new UpdateThreadRequestModel()
            {
                Id = new Guid("9a5ed572-3bc5-4918-a03b-82954a2017ec"),
                ThreadName = "Updated Name"
            };

            var result = await service.UpdateThreadAsync(thread);

            Assert.AreEqual(false, result.IsSuccess);
        }

        // GET BY ID START //

        [TestMethod]
        public async Task ThreadService_GetThreadByIdAsync_ShouldReturnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);

            var thread = new GetThreadByIdRequestModel()
            {
                Id = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec")
            };

            var result = await service.GetThreadByIdAsync(thread);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task ThreadService_GetThreadByIdAsync_ShouldReturnFalse_IdNotFound()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);

            var thread = new GetThreadByIdRequestModel()
            {
                Id = new Guid("9a5ed572-3bc5-4918-a03b-8293402017ec")
            };

            var result = await service.GetThreadByIdAsync(thread);

            Assert.AreEqual(false, result.IsSuccess);
        }

        // COUNT START // 
        [TestMethod]
        public async Task ThreadService_GetThreadsStatisticsAsync_ShouldReturnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ThreadService(mockContext.Object);


            var result = await service.GetThreadsStatisticsAsync();

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task ThreadService_GetThreadsStatisticsAsync_ShouldReturnFalse_NoThreads()
        {
            var mockDbContext = new Mock<GamersInsightContext>();

            var roles = new List<Role>()
                 {
                    new Role {Id = 1, RoleName = "User" , Alies = "Commoner"},
                    new Role {Id = 2, RoleName = "Moderator", Alies = "Knight"},
                    new Role {Id = 3, RoleName = "Admin", Alies = "King"}
                };
            var categories = new List<Category>()
                 {
                    new Category {Id = 1, CategoryName = "RPG", Description = "For the role players", NSFW = false},
                    new Category {Id = 2, CategoryName = "Racing", Description = "How fast can you go", NSFW = false},
                    new Category {Id = 3, CategoryName = "Mobile", Description = "Why does this exist even", NSFW = false},
                    new Category {Id = 4, CategoryName = "Sandbox", Description = "Show your creativity", NSFW = false},
                };
            var users = new List<User>()
                {
                    new User {Id = 1, UserName = "Test1", Password = "$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq",
                              Email = "test1@mail.com", DisplayName = "TestUser1", Avatar = "/images/test.jpg",
                              DateOfCreation = DateTime.Now.AddMonths(-2),
                              DateOfBirth = DateTime.Now.AddYears(-19), RoleId = 1, IsActivated = true, IsBlocked = false, Rating = 0},
                    new User {Id = 1, UserName = "Test2", Password = "$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq",
                              Email = "test2@mail.com", DisplayName = "TestUser2", Avatar = "/images/test.jpg",
                              DateOfCreation = DateTime.Now.AddMonths(-2),
                              DateOfBirth = DateTime.Now.AddYears(-15), RoleId = 1, IsActivated = true, IsBlocked = false, Rating = 0},
                    new User {Id = 1, UserName = "Test3", Password = "$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq",
                              Email = "test3@mail.com", DisplayName = "TestUser3", Avatar = "/images/test.jpg",
                              DateOfCreation = DateTime.Now.AddMonths(-2),
                              DateOfBirth = DateTime.Now.AddYears(-17), RoleId = 1, IsActivated = true, IsBlocked = false, Rating = 0},
                    new User {Id = 1, UserName = "Test4", Password = "$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq",
                              Email = "test4@mail.com", DisplayName = "TestUser4", Avatar = "/images/test.jpg",
                              DateOfCreation = DateTime.Now.AddMonths(-2),
                              DateOfBirth = DateTime.Now.AddYears(-23), RoleId = 1, IsActivated = true, IsBlocked = false, Rating = 0}
                };
            var threads = new List<Data.Thread>();
            var posts = new List<Post>();
            var postLikes = new List<UserPostLike>();

            var service = new ThreadService(mockDbContext.Object);

            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockCategories = categories.AsQueryable().BuildMockDbSet();
            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockThreads = threads.AsQueryable().BuildMockDbSet();
            var mockPosts = posts.AsQueryable().BuildMockDbSet();
            var mockPostLikes = postLikes.AsQueryable().BuildMockDbSet();

            mockDbContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockDbContext.Setup(db => db.Categories).Returns(mockCategories.Object);
            mockDbContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockDbContext.Setup(db => db.Threads).Returns(mockThreads.Object);
            mockDbContext.Setup(db => db.Posts).Returns(mockPosts.Object);
            mockDbContext.Setup(db => db.UserPostLikes).Returns(mockPostLikes.Object);


            var result = await service.GetThreadsStatisticsAsync();

            Assert.AreEqual(false, result.IsSuccess);
        }

    }
}
