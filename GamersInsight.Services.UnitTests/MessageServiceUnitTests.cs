﻿using GamersInsight.DTOs.RequestModels.Message;
using GamersInsight.Services.UnitTests.Helpers;
using GamersInsight.Services.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace GamersInsight.Services.UnitTests
{
    [TestClass]
    public class MessageServiceUnitTests
    {
        [TestMethod]
        public async Task UserService_SendMessageAsync_returnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new MessageService(mockContext.Object);

            var request = new SendMessageExtendedRequestModel()
            {
                SenderId = 1,
                ReceiverId = 2,
                Content = "TestMessage 1 to 2"
            };

            var result = await service.SendMessageAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
            Assert.AreEqual(Constants.MESSAGE_SENT, result.Message);
        }

        [TestMethod]
        public async Task UserService_SendMessageAsync_returnFalse_noUserFound()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new MessageService(mockContext.Object);

            var request = new SendMessageExtendedRequestModel()
            {
                SenderId = 1,
                ReceiverId = 5,
                Content = "TestMessage 1 to 2"
            };

            var result = await service.SendMessageAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.NO_SUCH_USER, result.Message);
        }
    }
}
