using GamersInsight.DTOs.RequestModels;
using GamersInsight.Services.UnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace GamersInsight.Services.UnitTests
{
    [TestClass]
    public class CategoryServiceUnitTests
    {
        // CREATE TESTS START //
        [TestMethod]
        public async Task CategoryService_Create_ShouldReturnFalse_BadParametersAsync()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new CategoryService(mockContext.Object);

            var request = new CreateCategoryRequestModel()
            {
                CategoryName = null,
                Description = "TestDescription1",
                NSFW = false
            };

            var request2 = new CreateCategoryRequestModel()
            {
                CategoryName = "Test1",
                Description = null,
                NSFW = false
            };

            var result = await service.CreateAsync(request);
            var result2 = await service.CreateAsync(request2);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(false, result.IsSuccess);
        }

        [TestMethod]
        public async Task CategoryService_Create_ShouldReturnFalse_CategoryExists()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new CategoryService(mockContext.Object);

            var request = new CreateCategoryRequestModel()
            {
                CategoryName = "RPG",
                Description = "For the role players",
                NSFW = false
            };

            var result = await service.CreateAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
        }

        [TestMethod]
        public async Task CategoryService_Create_ReturnTrue_ParamCorrect()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new CategoryService(mockContext.Object);

            var request = new CreateCategoryRequestModel()
            {
                CategoryName = "Test1",
                Description = "Test1",
                NSFW = false
            };

            var result = await service.CreateAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }
        // GET TESTS START // 

        [TestMethod]
        public async Task CategoryService_GetCategoriesAsync_ReturnTrue_ParamCorrect()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new CategoryService(mockContext.Object);

            var request = new GetExtendedCategoriesModel()
            {
                IsNsfw = false,
                ShouldGetLastPage = false,
                Skip = 0,
                Take = 4
            };

            var result = await service.GetCategoriesAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
            Assert.AreEqual(4, result.TotalCount);
        }

        [TestMethod]
        public async Task CategoryService_GetCategoriesAsync_ReturnFalse_ParamCorrect()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new CategoryService(mockContext.Object);

            var request = new GetExtendedCategoriesModel()
            {
                IsNsfw = true,
                ShouldGetLastPage = false,
                Skip = 4,
                Take = 4
            };

            var result = await service.GetCategoriesAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
        }

        [TestMethod]
        public async Task CategoryService_GetCategoriesAsync_ReturnTrue_LastPageCorrect()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new CategoryService(mockContext.Object);

            var request = new GetExtendedCategoriesModel()
            {
                IsNsfw = true,
                ShouldGetLastPage = true,
                Skip = 0,
                Take = 4
            };

            var result = await service.GetCategoriesAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        // DELETE TEST START //

        [TestMethod]
        public async Task CategoryService_DeleteCategoryAsync_ReturnTrue_CategoryExists()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new CategoryService(mockContext.Object);

            var request = new DeleteCategoryRequestModel()
            {
                Id = 1
            };

            var result = await service.DeleteCategoryAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task CategoryService_DeleteCategoryAsync_ReturnFalse_CategoryExists()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new CategoryService(mockContext.Object);

            var request = new DeleteCategoryRequestModel()
            {
                Id = 9
            };

            var result = await service.DeleteCategoryAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
        }

        // UPDATE TESTS START // 

        [TestMethod]
        public async Task CategoryService_UpdateCategoryAsync_ReturnTrue_CategoryExists()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new CategoryService(mockContext.Object);

            var request = new UpdateCategoryRequestModel()
            {
                Id = 1,
                CategoryName = "Test1 Update",
                Description = "Updated",
                IsNsfw = true
            };

            var result = await service.UpdateCateogryAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task CategoryService_UpdateCategoryAsync_ReturnFalse_CategoryNotExist()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new CategoryService(mockContext.Object);

            var request = new UpdateCategoryRequestModel()
            {
                Id = 9,
                CategoryName = "Test1 Update",
                Description = "Updated",
                IsNsfw = true
            };

            var result = await service.UpdateCateogryAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
        }

        // GET BY ID START // 

        [TestMethod]
        public async Task CategoryService_GetCategoryByIdAsync_ReturnTrue_CategoryExists()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new CategoryService(mockContext.Object);

            var request = new GetCategoryByIdRequestModel()
            {
                Id = 1
            };

            var result = await service.GetCategoryByIdAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task CategoryService_GetCategoryByIdAsync_ReturnFalse_CategoryNotExist()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new CategoryService(mockContext.Object);

            var request = new GetCategoryByIdRequestModel()
            {
                Id = 9
            };

            var result = await service.GetCategoryByIdAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
        }
    }
}
