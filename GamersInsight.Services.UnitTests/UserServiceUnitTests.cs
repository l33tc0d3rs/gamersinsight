﻿using GamersInsight.Data;
using GamersInsight.DTOs.RequestModels;
using GamersInsight.DTOs.RequestModels.User;
using GamersInsight.Models.RequestModels;
using GamersInsight.Services.Implementations;
using GamersInsight.Services.Interfaces;
using GamersInsight.Services.UnitTests.Helpers;
using GamersInsight.Services.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace GamersInsight.Services.UnitTests
{
    [TestClass]
    public class UserServiceUnitTests
    {
        [TestMethod]
        public async Task UserService_RegisterUser_ShouldReturnFalse_UsernameRange()
        {
            var mockDbContext = new Mock<GamersInsightContext>();
            var mockEnv = new Mock<IWebHostEnvironment>();
            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockDbContext.Object, mockEnv.Object, mockMail.Object);

            var request = new RegisterUserRequestExtendedModel()
            {
                Username = "o",
                Password = "12345",
                DisplayName = "TestUser1",
                DateOfBirth = DateTime.Now.AddYears(-18),
                Email = "Testuser@testmail.com"
            };

            var result = await service.RegisterUser(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.USER_USERNAME_ERROR, result.Message);
        }

        [TestMethod]
        public async Task UserService_RegisterUser_ShouldReturnFalse_UsernameRangeLong()
        {
            var mockDbContext = new Mock<GamersInsightContext>();
            var mockEnv = new Mock<IWebHostEnvironment>();
            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockDbContext.Object, mockEnv.Object, mockMail.Object);

            var request = new RegisterUserRequestExtendedModel()
            {
                Username = "veryverylongtestnameforusermaybeitisenough",
                Password = "12345",
                DisplayName = "TestUser1",
                DateOfBirth = DateTime.Now.AddYears(-18),
                Email = "Testuser@testmail.com"
            };

            var result = await service.RegisterUser(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.USER_USERNAME_ERROR, result.Message);
        }

        [TestMethod]
        public async Task UserService_RegisterUser_ShouldReturnFalse_DisplayNameShort()
        {
            var mockDbContext = new Mock<GamersInsightContext>();
            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockDbContext.Object, mockEnv.Object, mockMail.Object);

            var request = new RegisterUserRequestExtendedModel()
            {
                Username = "TestUser",
                Password = "12345",
                DisplayName = "T",
                DateOfBirth = DateTime.Now.AddYears(-18),
                Email = "Testuser@testmail.com"
            };

            var result = await service.RegisterUser(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.USER_DISPLAYNAME_ERROR, result.Message);
        }

        [TestMethod]
        public async Task UserService_RegisterUser_ShouldReturnFalse_DisplayNameLong()
        {
            var mockDbContext = new Mock<GamersInsightContext>();
            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockDbContext.Object, mockEnv.Object, mockMail.Object);

            var request = new RegisterUserRequestExtendedModel()
            {
                Username = "TestUser",
                Password = "12345",
                DisplayName = "TooLongDisplayNameShouldBeShorter",
                DateOfBirth = DateTime.Now.AddYears(-18),
                Email = "Testuser@testmail.com"
            };

            var result = await service.RegisterUser(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.USER_DISPLAYNAME_ERROR, result.Message);
        }

        [TestMethod]
        public async Task UserService_RegisterUser_ShouldReturnFalse_EmailInvaid()
        {
            var mockDbContext = new Mock<GamersInsightContext>();
            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockDbContext.Object, mockEnv.Object, mockMail.Object);

            var request = new RegisterUserRequestExtendedModel()
            {
                Username = "TestUser",
                Password = "12345",
                DisplayName = "Testing",
                DateOfBirth = DateTime.Now.AddYears(-18),
                Email = "invalidmail"
            };

            var result = await service.RegisterUser(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.USER_EMAIL_ERROR, result.Message);
        }

        [TestMethod]
        public async Task UserService_RegisterUser_ShouldReturnFalse_PasswordWeak()
        {
            var mockDbContext = new Mock<GamersInsightContext>();
            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockDbContext.Object, mockEnv.Object, mockMail.Object);

            var request = new RegisterUserRequestExtendedModel()
            {
                Username = "TestUser",
                Password = "smallonly",
                DisplayName = "TestName",
                DateOfBirth = DateTime.Now.AddYears(-18),
                Email = "Testuser@testmail.com"
            };

            var result = await service.RegisterUser(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.USER_PASSWORD, result.Message);
        }

        [TestMethod]
        public async Task UserService_RegisterUser_ShouldReturnFalse_UserExists()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new RegisterUserRequestExtendedModel()
            {
                Username = "Test1",
                Password = "TestPas5w0rd",
                DisplayName = "TestUser",
                DateOfBirth = DateTime.Now.AddYears(-18),
                Email = "Testuser@testmail.com"
            };

            var result = await service.RegisterUser(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.USER_ALREADY_EXISTS, result.Message);
        }

        [TestMethod]
        public async Task UserService_RegisterUser_ShouldReturnFalse_EmailExists()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new RegisterUserRequestExtendedModel()
            {
                Username = "TestUSR1",
                Password = "TestPas5w0rd",
                DisplayName = "TestUser",
                DateOfBirth = DateTime.Now.AddYears(-18),
                Email = "test1@mail.com"
            };

            var result = await service.RegisterUser(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.EMAIL_ALREADY_EXISTS, result.Message);
        }

        [TestMethod]
        public async Task UserService_RegisterUser_ShouldReturnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new RegisterUserRequestExtendedModel()
            {
                Username = "TestUSR1",
                Password = "TestPas5w0rd",
                DisplayName = "TestUser",
                DateOfBirth = DateTime.Now.AddYears(-18),
                Email = "test199@mail.com"
            };

            var result = await service.RegisterUser(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        // LOGIN TESTS START //

        [TestMethod]
        public async Task UserService_GetLoggedUserInfoAsync_ShouldReturnFalse_NoUser()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new GetLoggedUserInfoRequestModel()
            {
                Username = "Username",
                Password = "12345"
            };

            var result = await service.GetLoggedUserInfoAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.NO_SUCH_USER, result.Message);
        }

        [TestMethod]
        public async Task UserService_GetLoggedUserInfoAsync_False_PasswordWrong()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new GetLoggedUserInfoRequestModel()
            {
                Username = "Test1",
                Password = "99999999"
            };

            var result = await service.GetLoggedUserInfoAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.PASSWORD_IS_INCORRECT, result.Message);
        }

        [TestMethod]
        public async Task UserService_GetLoggedUserInfoAsync_True()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new GetLoggedUserInfoRequestModel()
            {
                Username = "Test1",
                Password = "12345"
            };

            var result = await service.GetLoggedUserInfoAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        // UPDATE TESTS START //

        [TestMethod]
        public async Task UserService_UpdateUserAsync_returnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new UpdateUserExtendedRequestModel()
            {
                CurrentUserId = 1,
                CurrentPassword = "12345",
                DisplayName = "UpdatedName",
                Email = "testMail1@mail.com",
                NewPassword = "54321",
                RepeatPassword = "54321"
            };

            var result = await service.UpdateUserAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task UserService_UpdateUserAsync_returnFalse_PasswordDontMatch()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new UpdateUserExtendedRequestModel()
            {
                CurrentUserId = 1,
                CurrentPassword = "12345",
                DisplayName = "UpdatedName",
                Email = "testMail1@mail.com",
                NewPassword = "54321",
                RepeatPassword = "4321"
            };

            var result = await service.UpdateUserAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.PASSWORD_NO_MATCH, result.Message);
        }

        [TestMethod]
        public async Task UserService_UpdateUserAsync_returnFalse_WrongUser()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new UpdateUserExtendedRequestModel()
            {
                CurrentUserId = 6,
                CurrentPassword = "12345",
                DisplayName = "UpdatedName",
                Email = "testMail1@mail.com",
                NewPassword = "54321",
                RepeatPassword = "54321"
            };

            var result = await service.UpdateUserAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.NO_SUCH_USER, result.Message);
        }

        [TestMethod]
        public async Task UserService_UpdateUserAsync_returnFalse_WrongPassword()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new UpdateUserExtendedRequestModel()
            {
                CurrentUserId = 1,
                CurrentPassword = "123333",
                DisplayName = "UpdatedName",
                Email = "testMail1@mail.com",
                NewPassword = "54321",
                RepeatPassword = "54321"
            };

            var result = await service.UpdateUserAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.PASSWORD_IS_INCORRECT, result.Message);
        }

        [TestMethod]
        public async Task UserService_UpdateAvatarAsync_returnFalse_NoSuchUser()
        {
            var mockContext = MockDb.MockDbContext;
            var stream = new Mock<System.IO.Stream>();

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new UpdateAvatarExtendedRequestModel()
            {
                CurrentUserId = 9,
                Image = new FormFile(stream.Object, 5, 5, "asd", "img.jpg")
            };

            var result = await service.UpdateAvatarAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.NO_SUCH_USER, result.Message);
        }

        [TestMethod]
        public async Task UserService_GetUserSettingsAsync_returnFalse_NoSuchUser()
        {
            var mockContext = MockDb.MockDbContext;
            var stream = new Mock<System.IO.Stream>();

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new GetUserSettingsRequestModel()
            {
                CurrentUserId = 9
            };

            var result = await service.GetUserSettingsAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.NO_SUCH_USER, result.Message);
        }

        [TestMethod]
        public async Task UserService_GetUserSettingsAsync_returnTrue()
        {
            var mockContext = MockDb.MockDbContext;
            var stream = new Mock<System.IO.Stream>();

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new GetUserSettingsRequestModel()
            {
                CurrentUserId = 1
            };

            var result = await service.GetUserSettingsAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
            Assert.AreEqual(Constants.USER_SETTING_RETRIVED, result.Message);
        }

        [TestMethod]
        public async Task UserService_GetUserByFilter_returnFalse_NoSuchUser()
        {
            var mockContext = MockDb.MockDbContext;
            var stream = new Mock<System.IO.Stream>();

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new GetFilteredUsersPaginatedRequestModel()
            {
                FilterInput = "asdf",
                ShouldGetLastPage = false,
                Skip = 0,
                Take = 5
            };

            var result = await service.GetUserByFilter(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.NO_SUCH_USER, result.Message);
        }

        [TestMethod]
        public async Task UserService_GetUserByFilter_returnTrue()
        {
            var mockContext = MockDb.MockDbContext;
            var stream = new Mock<System.IO.Stream>();

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new GetFilteredUsersPaginatedRequestModel()
            {
                FilterInput = "Test",
                ShouldGetLastPage = true,
                Skip = 0,
                Take = 5
            };

            var result = await service.GetUserByFilter(request);

            Assert.AreEqual(true, result.IsSuccess);
            Assert.AreEqual(4, result.TotalCount);
        }

        //  BLOCK UNBLOCK TESTS // 

        [TestMethod]
        public async Task UserService_BlockUser_returnFalse_NoSuchUser()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new BlockUserRequestModel()
            {
                ManagedUserId = 9
            };

            var result = await service.BlockUserAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.NO_SUCH_USER, result.Message);
        }

        [TestMethod]
        public async Task UserService_BlockUser_returnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new BlockUserRequestModel()
            {
                ManagedUserId = 1
            };

            var result = await service.BlockUserAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
            Assert.AreEqual(Constants.USER_BLOCKED, result.Message);
        }

        [TestMethod]
        public async Task UserService_UnBlockUser_returnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new UnblockUserRequestModel()
            {
                ManagedUserId = 1
            };

            var result = await service.UnblockUserAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
            Assert.AreEqual(Constants.USER_UNBLOCKED, result.Message);
        }

        [TestMethod]
        public async Task UserService_UnBlockUser_returnFalse_NoSuchUser()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new UnblockUserRequestModel()
            {
                ManagedUserId = 9
            };

            var result = await service.UnblockUserAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.NO_SUCH_USER, result.Message);
        }

        // GET TESTS START //

        [TestMethod]
        public async Task UserService_GetUsersAsync_returnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new GetManagementRequestModel()
            {
                ShouldGetLastPage = true,
                Skip = 0,
                Take = 5
            };

            var result = await service.GetUsersAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public async Task UserService_GetUsersAsync_returnFalse()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object); ;

            var request = new GetManagementRequestModel()
            {
                ShouldGetLastPage = false,
                Skip = 10,
                Take = 5
            };

            var result = await service.GetUsersAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.POST_NO_DATA, result.Message);
        }

        [TestMethod]
        public async Task UserService_GetUserShortInfoAsync_returnFalse()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new GetUserSettingsRequestModel()
            {
                CurrentUserId = 9
            };

            var result = await service.GetUserShortInfoAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.NO_SUCH_USER, result.Message);
        }

        [TestMethod]
        public async Task UserService_GetUserShortInfoAsync_returnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var mockEnv = new Mock<IWebHostEnvironment>();

            var mockMail = new Mock<IMailService>();

            var service = new UserService(mockContext.Object, mockEnv.Object, mockMail.Object);

            var request = new GetUserSettingsRequestModel()
            {
                CurrentUserId = 1
            };

            var result = await service.GetUserShortInfoAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }
    }
}
