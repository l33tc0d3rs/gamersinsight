﻿using GamersInsight.Data;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GamersInsight.Services.UnitTests.Helpers
{
    public static class MockDb
    {
        public static List<Role> Roles
        {
            get
            {
                return new List<Role>()
                {
                    new Role {Id = 1, RoleName = "User" , Alies = "Commoner"},
                    new Role {Id = 2, RoleName = "Moderator", Alies = "Knight"},
                    new Role {Id = 3, RoleName = "Admin", Alies = "King"}
                };
            }
        }

        public static List<Category> Categories
        {
            get
            {
                return new List<Category>()
                {
                    new Category {Id = 1, CategoryName = "RPG", Description = "For the role players", NSFW = false},
                    new Category {Id = 2, CategoryName = "Racing", Description = "How fast can you go", NSFW = false},
                    new Category {Id = 3, CategoryName = "Mobile", Description = "Why does this exist even", NSFW = false},
                    new Category {Id = 4, CategoryName = "Sandbox", Description = "Show your creativity", NSFW = false},
                };
            }
        }

        public static List<User> Users
        {
            get
            {
                return new List<User>()
                {
                    new User {Id = 1, UserName = "Test1", Password = "$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq",
                              Email = "test1@mail.com", DisplayName = "TestUser1", Avatar = "/images/test.jpg",
                              DateOfCreation = DateTime.Now.AddMonths(-2),
                              DateOfBirth = DateTime.Now.AddYears(-19), RoleId = 1, IsActivated = true, IsBlocked = false, Rating = 0},
                    new User {Id = 2, UserName = "Test2", Password = "$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq",
                              Email = "test2@mail.com", DisplayName = "TestUser2", Avatar = "/images/test.jpg",
                              DateOfCreation = DateTime.Now.AddMonths(-2),
                              DateOfBirth = DateTime.Now.AddYears(-15), RoleId = 1, IsActivated = true, IsBlocked = false, Rating = 0},
                    new User {Id = 3, UserName = "Test3", Password = "$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq",
                              Email = "test3@mail.com", DisplayName = "TestUser3", Avatar = "/images/test.jpg",
                              DateOfCreation = DateTime.Now.AddMonths(-2),
                              DateOfBirth = DateTime.Now.AddYears(-17), RoleId = 1, IsActivated = true, IsBlocked = false, Rating = 0},
                    new User {Id = 4, UserName = "Test4", Password = "$2a$11$VHy.cyVo0NbyquzkAfm24OV3lahVcwmSv2dxM2f.I9wz3j1jQtcDq",
                              Email = "test4@mail.com", DisplayName = "TestUser4", Avatar = "/images/test.jpg",
                              DateOfCreation = DateTime.Now.AddMonths(-2),
                              DateOfBirth = DateTime.Now.AddYears(-23), RoleId = 1, IsActivated = true, IsBlocked = false, Rating = 0}
                };
            }
        }

        public static List<Thread> Threads
        {
            get
            {
                return new List<Thread>()
                {
                    new Thread { Id = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec"), ThreadName = "New Games", CategoryId = 1, CreatorId = 1, DateOfCreation = DateTime.Now.AddDays(-1) },
                    new Thread { Id = new Guid("3554bc35-af7f-4d9e-b17f-426720c111f7"), ThreadName = "Old Games", CategoryId = 1, CreatorId = 2, DateOfCreation = DateTime.Now.AddDays(-2) },
                    new Thread { Id = new Guid("de6fda2a-b6ed-4d43-9211-8212230a6177"), ThreadName = "Fast Tree", CategoryId = 2, CreatorId = 3, DateOfCreation = DateTime.Now.AddDays(-3) },
                    new Thread { Id = new Guid("55f8d69a-6772-402b-b9e2-706f73d9f2cf"), ThreadName = "The hacks", CategoryId = 2, CreatorId = 4, DateOfCreation = DateTime.Now.AddDays(-4) },
                    new Thread { Id = new Guid("3ef77fbd-23b1-4cda-a450-7f972ad3d749"), ThreadName = "Now Plays", CategoryId = 3, CreatorId = 1, DateOfCreation = DateTime.Now.AddDays(-5) },
                    new Thread { Id = new Guid("1884ac1f-6aab-4aab-bd5c-d4c5aada0120"), ThreadName = "Jumps out", CategoryId = 3, CreatorId = 2, DateOfCreation = DateTime.Now.AddDays(-6) },
                    new Thread { Id = new Guid("5b3db90b-e94f-42b7-b558-07537c9a73c3"), ThreadName = "Outdated!", CategoryId = 4, CreatorId = 3, DateOfCreation = DateTime.Now.AddDays(-7) },
                    new Thread { Id = new Guid("03004644-409e-4cac-8c8b-6591d721dbdd"), ThreadName = "End Game!", CategoryId = 4, CreatorId = 4, DateOfCreation = DateTime.Now.AddDays(-8) }
                };
            }
        }

        public static List<Post> Posts
        {
            get
            {
                return new List<Post>()
                {
                    new Post { Id = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"), Title = "Post One", Content = "SearchWork?",
                               ThreadId = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec"), CreatorId = 1, DateOfCreation = DateTime.Now.AddHours(-5), Rating = 4 },
                    new Post { Id = new Guid("1e4ee427-3299-44a6-8394-e09129196c1d"), Title = "Second Two", Content = "23",
                               ThreadId = new Guid("9a5ed572-3bc5-4918-a03b-82934a2017ec"), CreatorId = 2, DateOfCreation = DateTime.Now.AddHours(-6), Rating = 0 },
                    new Post { Id = new Guid("57f18194-fea0-460b-bcbf-4fb4021c0f06"), Title = "Third Tree", Content = "34",
                               ThreadId = new Guid("3554bc35-af7f-4d9e-b17f-426720c111f7"), CreatorId = 3, DateOfCreation = DateTime.Now.AddHours(-7), Rating = 0 },
                    new Post { Id = new Guid("496362c9-f551-4653-be87-37d38225be00"), Title = "Fourth One", Content = "45",
                               ThreadId = new Guid("3554bc35-af7f-4d9e-b17f-426720c111f7"), CreatorId = 4, DateOfCreation = DateTime.Now.AddHours(-8), Rating = 0 },
                    new Post { Id = new Guid("f1f0d72a-332c-46cf-b248-e9cd2eb4229a"), Title = "Fifth 5", Content = "56",
                               ThreadId = new Guid("de6fda2a-b6ed-4d43-9211-8212230a6177"), CreatorId = 4, DateOfCreation = DateTime.Now.AddHours(-9), Rating = 0 },
                    new Post { Id = new Guid("f80ae531-be6a-4243-8090-9fe85432bc90"), Title = "Sixt 6", Content = "67",
                               ThreadId = new Guid("de6fda2a-b6ed-4d43-9211-8212230a6177"), CreatorId = 3, DateOfCreation = DateTime.Now.AddHours(-10), Rating = 0 },
                    new Post { Id = new Guid("972948f6-3647-4aa1-a383-98508b225e1f"), Title = "Seventh 7", Content = "78",
                               ThreadId = new Guid("55f8d69a-6772-402b-b9e2-706f73d9f2cf"), CreatorId = 2, DateOfCreation = DateTime.Now.AddHours(-11), Rating = 0 },
                    new Post { Id = new Guid("eb2abe7b-2d60-42d6-a3e0-8ee7243ef888"), Title = "Eight 8", Content = "89",
                               ThreadId = new Guid("55f8d69a-6772-402b-b9e2-706f73d9f2cf"), CreatorId = 1, DateOfCreation = DateTime.Now.AddHours(-12), Rating = 0 },
                    new Post { Id = new Guid("2f9e681b-0199-4952-ba43-611fd6218d10"), Title = "test10", Content = "0120",
                               ThreadId = new Guid("3ef77fbd-23b1-4cda-a450-7f972ad3d749"), CreatorId = 1, DateOfCreation = DateTime.Now.AddHours(-5), Rating = 0 },
                    new Post { Id = new Guid("fc99180c-d5cb-448d-9f83-8bd70037c144"), Title = "test11", Content = "0230",
                               ThreadId = new Guid("3ef77fbd-23b1-4cda-a450-7f972ad3d749"), CreatorId = 2, DateOfCreation = DateTime.Now.AddHours(-6), Rating = 0 },
                    new Post { Id = new Guid("ab268ffd-6c2c-4a31-8946-758a9d6873ff"), Title = "test12", Content = "0340",
                               ThreadId = new Guid("1884ac1f-6aab-4aab-bd5c-d4c5aada0120"), CreatorId = 3, DateOfCreation = DateTime.Now.AddHours(-7), Rating = 0 },
                    new Post { Id = new Guid("d2d5211b-1d46-4cfa-8135-04fbbbe364dd"), Title = "test13", Content = "0450",
                               ThreadId = new Guid("1884ac1f-6aab-4aab-bd5c-d4c5aada0120"), CreatorId = 4, DateOfCreation = DateTime.Now.AddHours(-8), Rating = 0 },
                    new Post { Id = new Guid("72516b69-0bc0-49f0-a5da-d708ff31e688"), Title = "test14", Content = "0560",
                               ThreadId = new Guid("5b3db90b-e94f-42b7-b558-07537c9a73c3"), CreatorId = 1, DateOfCreation = DateTime.Now.AddHours(-9), Rating = 0 },
                    new Post { Id = new Guid("562fa09a-28ed-48d3-b9e8-ea1f9c57f419"), Title = "test15", Content = "0670",
                               ThreadId = new Guid("5b3db90b-e94f-42b7-b558-07537c9a73c3"), CreatorId = 2, DateOfCreation = DateTime.Now.AddHours(-10), Rating = 0 },
                    new Post { Id = new Guid("b91cb614-9aee-4aab-9f15-40085f3fae6f"), Title = "test16", Content = "0780",
                               ThreadId = new Guid("03004644-409e-4cac-8c8b-6591d721dbdd"), CreatorId = 3, DateOfCreation = DateTime.Now.AddHours(-11), Rating = 0 },
                    new Post { Id = new Guid("0174b66c-4e68-433d-956c-744e1619388f"), Title = "test17", Content = "0890",
                               ThreadId = new Guid("03004644-409e-4cac-8c8b-6591d721dbdd"), CreatorId = 4, DateOfCreation = DateTime.Now.AddHours(-12), Rating = 0 }
                };
            }
        }

        public static List<Comment> Comments
        {
            get
            {
                return new List<Comment>
                {
                    new Comment { Id = new Guid("11d841aa-ac0f-41f2-8215-90f32fcf75bb"), Content = "Random0", PostId = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"), CreatorId = 4, DateOfCreation = DateTime.Now.AddMinutes(-1), Rating = 0 },
                    new Comment { Id = new Guid("34f011ec-efd4-4522-b065-f33d65c04a55"), Content = "Random2", PostId = new Guid("1e4ee427-3299-44a6-8394-e09129196c1d"), CreatorId = 3, DateOfCreation = DateTime.Now.AddMinutes(-2), Rating = 0 },
                    new Comment { Id = new Guid("fa4dafa9-d8a7-4096-90d4-9172faa89338"), Content = "Random3", PostId = new Guid("57f18194-fea0-460b-bcbf-4fb4021c0f06"), CreatorId = 2, DateOfCreation = DateTime.Now.AddMinutes(-3), Rating = 0 },
                    new Comment { Id = new Guid("607e5044-d03e-417d-a733-274c684d539e"), Content = "Random4", PostId = new Guid("496362c9-f551-4653-be87-37d38225be00"), CreatorId = 1, DateOfCreation = DateTime.Now.AddMinutes(-4), Rating = 0 },
                    new Comment { Id = new Guid("c53bcf2e-edae-4470-b7ec-0c2641afc7d0"), Content = "Random5", PostId = new Guid("f1f0d72a-332c-46cf-b248-e9cd2eb4229a"), CreatorId = 1, DateOfCreation = DateTime.Now.AddMinutes(-5), Rating = 0 },
                    new Comment { Id = new Guid("0a0f83bb-3f87-416b-ae57-af1dc4150f35"), Content = "Random6", PostId = new Guid("f80ae531-be6a-4243-8090-9fe85432bc90"), CreatorId = 2, DateOfCreation = DateTime.Now.AddMinutes(-6), Rating = 0 },
                    new Comment { Id = new Guid("c6e09b5b-7e70-415a-990f-e6cecaa7a482"), Content = "Random7", PostId = new Guid("972948f6-3647-4aa1-a383-98508b225e1f"), CreatorId = 3, DateOfCreation = DateTime.Now.AddMinutes(-7), Rating = 0 },
                    new Comment { Id = new Guid("ae1e666a-da50-4579-aab0-5c94a6816b20"), Content = "Random8", PostId = new Guid("eb2abe7b-2d60-42d6-a3e0-8ee7243ef888"), CreatorId = 4, DateOfCreation = DateTime.Now.AddMinutes(-8), Rating = 0 },
                    new Comment { Id = new Guid("2ac77d7e-b13b-42e0-a995-d366bec8966a"), Content = "Random9", PostId = new Guid("2f9e681b-0199-4952-ba43-611fd6218d10"), CreatorId = 1, DateOfCreation = DateTime.Now.AddMinutes(-9), Rating = 0 },
                    new Comment { Id = new Guid("f4ec57ff-4096-4ec2-8bd3-c37d574efb62"), Content = "Rando10", PostId = new Guid("fc99180c-d5cb-448d-9f83-8bd70037c144"), CreatorId = 2, DateOfCreation = DateTime.Now.AddMinutes(-10), Rating = 0 },
                    new Comment { Id = new Guid("9698a611-36df-488a-8ca3-786b255095e1"), Content = "Rando11", PostId = new Guid("ab268ffd-6c2c-4a31-8946-758a9d6873ff"), CreatorId = 3, DateOfCreation = DateTime.Now.AddMinutes(-11), Rating = 0 },
                    new Comment { Id = new Guid("d37d98ed-232b-4ad9-89a8-2ee16fd50465"), Content = "Rando12", PostId = new Guid("d2d5211b-1d46-4cfa-8135-04fbbbe364dd"), CreatorId = 4, DateOfCreation = DateTime.Now.AddMinutes(-12), Rating = 0 },
                    new Comment { Id = new Guid("87405a62-f926-4244-a3d5-76b8825ed734"), Content = "Rando13", PostId = new Guid("72516b69-0bc0-49f0-a5da-d708ff31e688"), CreatorId = 4, DateOfCreation = DateTime.Now.AddMinutes(-13), Rating = 0 },
                    new Comment { Id = new Guid("55731186-40ba-4cce-8eed-1855a8dec467"), Content = "Rando14", PostId = new Guid("562fa09a-28ed-48d3-b9e8-ea1f9c57f419"), CreatorId = 3, DateOfCreation = DateTime.Now.AddMinutes(-14), Rating = 0 },
                    new Comment { Id = new Guid("80590b7b-caa0-40b2-982c-332ce2e8571a"), Content = "Rando15", PostId = new Guid("b91cb614-9aee-4aab-9f15-40085f3fae6f"), CreatorId = 2, DateOfCreation = DateTime.Now.AddMinutes(-15), Rating = 0 },
                    new Comment { Id = new Guid("95182a39-b2b9-4f55-a842-ebb5dc1fe752"), Content = "Rando16", PostId = new Guid("0174b66c-4e68-433d-956c-744e1619388f"), CreatorId = 1, DateOfCreation = DateTime.Now.AddMinutes(-16), Rating = 0 }
                };
            }
        }

        public static List<Message> Messages
        {
            get
            {
                return new List<Message>()
                {
                    new Message { Id = new Guid("3e57e5aa-49f0-4e95-8715-c98f1ec67466"), SenderId = 1, ReceiverId = 2, MessageContent = "Hello!", TimeOfCreation = DateTime.Now.AddHours(-1) },
                    new Message { Id = new Guid("3e57e5aa-49f0-4e95-8715-c98f1ec67466"), SenderId = 2, ReceiverId = 1, MessageContent = "Hello!", TimeOfCreation = DateTime.Now.AddHours(-1) },
                    new Message { Id = new Guid("3e57e5aa-49f0-4e95-8715-c98f1ec67466"), SenderId = 1, ReceiverId = 3, MessageContent = "Hello!", TimeOfCreation = DateTime.Now.AddHours(-1) },
                    new Message { Id = new Guid("3e57e5aa-49f0-4e95-8715-c98f1ec67466"), SenderId = 3, ReceiverId = 2, MessageContent = "Hello!", TimeOfCreation = DateTime.Now.AddHours(-1) }
                };
            }
        }

        public static List<UserPostLike> UserPostLikes
        {
            get
            {
                return new List<UserPostLike>()
                {
                    new UserPostLike { PostId = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"), UserId = 1},
                    new UserPostLike { PostId = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"), UserId = 2},
                    new UserPostLike { PostId = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"), UserId = 3},
                    new UserPostLike { PostId = new Guid("c79894aa-997d-4a97-9ffc-2290d791b0c4"), UserId = 4},
                    new UserPostLike { PostId = new Guid("3e57e5aa-49f0-4e95-8715-c98f1ec67467"), UserId = 1}

                };
            }
        }
        public static Mock<GamersInsightContext> MockDbContext
        {
            get
            {
                var mockDbContext = new Mock<GamersInsightContext>();

                var mockRoles = Roles.AsQueryable().BuildMockDbSet();
                var mockCategories = Categories.AsQueryable().BuildMockDbSet();
                var mockUsers = Users.AsQueryable().BuildMockDbSet();
                var mockThreads = Threads.AsQueryable().BuildMockDbSet();
                var mockPosts = Posts.AsQueryable().BuildMockDbSet();
                var mockComments = Comments.AsQueryable().BuildMockDbSet();
                var mockMessages = Messages.AsQueryable().BuildMockDbSet();
                var mockUserPostLikes = UserPostLikes.AsQueryable().BuildMockDbSet();

                mockDbContext.Setup(db => db.Roles).Returns(mockRoles.Object);
                mockDbContext.Setup(db => db.Categories).Returns(mockCategories.Object);
                mockDbContext.Setup(db => db.Users).Returns(mockUsers.Object);
                mockDbContext.Setup(db => db.Threads).Returns(mockThreads.Object);
                mockDbContext.Setup(db => db.Posts).Returns(mockPosts.Object);
                mockDbContext.Setup(db => db.Comments).Returns(mockComments.Object);
                mockDbContext.Setup(db => db.Messages).Returns(mockMessages.Object);
                mockDbContext.Setup(db => db.UserPostLikes).Returns(mockUserPostLikes.Object);

                return mockDbContext;
            }
        }
    }
}