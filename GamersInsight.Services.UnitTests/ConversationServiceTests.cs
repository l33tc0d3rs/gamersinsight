﻿using GamersInsight.DTOs.RequestModels.Conversation;
using GamersInsight.Services.UnitTests.Helpers;
using GamersInsight.Services.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace GamersInsight.Services.UnitTests
{
    [TestClass]
    public class ConversationServiceTests
    {
        [TestMethod]
        public async Task UserService_ConversationsAsync_returnFalse_NoSuchUser()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ConversationService(mockContext.Object);

            var request = new GetConversationsExtendedRequestModel()
            {
                CurrentUserId = 5,
                ShouldGetLastPage = false,
                Skip = 0,
                Take = 5
            };

            var result = await service.ConversationsAsync(request);

            Assert.AreEqual(false, result.IsSuccess);
            Assert.AreEqual(Constants.POST_NO_DATA, result.Message);
        }

        [TestMethod]
        public async Task UserService_ConversationsAsync_returnTrue()
        {
            var mockContext = MockDb.MockDbContext;

            var service = new ConversationService(mockContext.Object);

            var request = new GetConversationsExtendedRequestModel()
            {
                CurrentUserId = 1,
                ShouldGetLastPage = true,
                Skip = 0,
                Take = 5
            };

            var result = await service.ConversationsAsync(request);

            Assert.AreEqual(true, result.IsSuccess);
        }
    }
}
